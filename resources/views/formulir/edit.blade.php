@extends('layouts-matrix.master')

@section('breadcrumb')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title alert">{{ $breadcrumb }}</h4>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="container-fluid">
    @if (count($formulir) == "0")
    <div class="col-md-12 alert alert-danger">PERINGATAN</div>
    @else
        @if ($formulir[0]->status == 0)
        <div class="col-md-12 alert alert-primary">{{ $title }}</div>
        @else
        <div class="col-md-12 alert alert-warning">PEMBERITAHUAN</div>
        @endif
    @endif

    <div class="card">
        <div class="card-body">
            @if (count($formulir) == "0")
            <h3 class="text-center"><strong>Formulir Anda belum dibuat.</strong></h3>
            <br>
            <a href="{{ url('/formulir') }}" class="btn btn-block btn-success">KEMBALI</a>
            @else
                @if ($formulir[0]->status == 0)
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/formulir/edit') }}" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PUT') }}
                    @foreach ($formulir as $e=>$dt)
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="picture-container mb-2">
                                    <div class="picture">
                                        <img src="{{ asset( $dt->foto_diri ) }}" class="picture-src" id="photoPreview" title="">
                                        <input type="file" name="foto_diri" id="gambar" class="">
                                    </div>
                                    <label class="text-center control-label col-form-label">FOTO FORMAL</label>
                                </div>
                                <div class="btn-group btn-block" id="ubahFotoX" role="group" aria-label="Aksi">
                                    <button type="button" class="btn btn-secondary rounded-0" id="kembalikan"><span class="fe fe-rotate-ccw fe-16 mr-1"></span>Kembali</button>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="picture-container mb-2">
                                    <div class="picture">
                                        <img src="{{ asset( $dt->foto_ktm ) }}" class="picture-src2" id="photoPreviewKTM" title="">
                                        <input type="file" name="foto_ktm" id="gambar2" class="">
                                    </div>
                                    <label class="text-center control-label col-form-label">FOTO KTM</label>
                                </div>
                                <div class="btn-group btn-block" id="ubahFotoX2" role="group" aria-label="Aksi">
                                    <button type="button" class="btn btn-secondary rounded-0" id="kembalikan2"><span class="fe fe-rotate-ccw fe-16 mr-1"></span>Kembali</button>
                                </div>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">NIM</label>
                                    <input type="number" class="required form-control" name="nim" value="{{ $dt->nim }}" placeholder="Masukkan NIM Anda">
                                </div>

                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">Jenis Kelamin</label>
                                    <select class="required select2 form-control custom-select" name="jenkel" style="width: 100%; height:36px;">
                                        <option  value="{{ $dt->jenkel }}">{{ $dt->jenkel }}</option>
                                        <option>--Pilih--</option>
                                        <option value="Pria">Pria</option>
                                        <option value="Wanita">Wanita</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">Kelas</label>
                                    <select class="required select2 form-control custom-select" name="kelas" style="width: 100%; height:36px;">
                                        <option value="{{ $dt->kelas }}">{{ $dt->kelas }}</option>
                                        <option>--Pilih--</option>
                                        <option value="1-A">1-A</option>
                                        <option value="1-B">1-B</option>
                                        <option value="1-C">1-C</option>
                                        <option value="1-D">1-D</option>
                                        <option value="1-E">1-E</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">Kementrian 1</label>
                                    <select class="required select2 form-control custom-select" name="kementrian1" style="width: 100%; height:36px;">
                                        <option value="{{ $dt->kementrian1 }}">{{ $dt->kementrian1 }}</option>
                                        <option>--Pilih--</option>
                                        @if (count($hasil) != "0")
                                        @foreach ($hasil as $e=>$hsl)
                                            @if ($hsl->kementrian == 'lugri')
                                                <option value="Kementrian Luar Negeri">
                                                    Kementrian Luar Negeri
                                                </option>
                                            @elseif ($hsl->kementrian == 'sosma')
                                                <option value="Kementrian Sosial Masyarakat">
                                                    Kementrian Sosial Masyarakat
                                                </option>
                                            @elseif ($hsl->kementrian == 'dagri')
                                                <option value="Kementrian Dalam Negeri">
                                                    Kementrian Dalam Negeri
                                                </option>
                                            @elseif ($hsl->kementrian == 'kemhas')
                                                <option value="Kementrian Kemahasiswaan">
                                                    Kementrian Kemahasiswaan
                                                </option>
                                            @elseif ($hsl->kementrian == 'psdm')
                                                <option value="Kementrian Pengembangan Sumber Daya Mahasiswa">
                                                    Kementrian Pengembangan Sumber Daya Mahasiswa
                                                </option>
                                            @elseif ($hsl->kementrian == 'agama')
                                                <option value="Kementrian Agama">
                                                    Kementrian Agama
                                                </option>
                                            @elseif ($hsl->kementrian == 'kominfo')
                                                <option value="Kementrian Komunikasi dan Informasi">
                                                    Kementrian Komunikasi dan Informasi
                                                </option>
                                            @else
                                                Tidak ada yang cocok
                                            @endif
                                        @endforeach
                                        @else
                                        @foreach ($kementrian as $e=>$kementrian1)
                                        <option value="{{ $kementrian1->nama_kementrian }}">{{ $kementrian1->nama_kementrian }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">Nama Lengkap</label>
                                    <input type="text" class="required form-control" name="nama" value="{{ $dt->nama }}" placeholder="Masukkan Nama Lengkap Anda">
                                </div>

                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">PRODI</label>
                                    <select class="required select2 form-control custom-select" name="prodi" style="width: 100%; height:36px;">
                                        <option value="{{ $dt->prodi }}">{{ $dt->prodi }}</option>
                                        <option>--Pilih--</option>
                                        <option value="Akuntansi">Akuntansi</option>
                                        <option value="Manajemen Informatika">Manajemen Informatika</option>
                                        <option value="Teknik Mesin">Teknik Mesin</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">No. WA</label>
                                    <input type="text" class="required form-control" name="no_wa" value="{{ $dt->no_wa }}" placeholder="Masukkan Nomor WhatsApp Anda">
                                </div>

                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">Kementrian 2 (Opsional)</label>
                                    <select class="required select2 form-control custom-select" name="kementrian2" style="width: 100%; height:36px;">
                                        <option value="{{ $dt->kementrian2 }}">{{ $dt->kementrian2 }}</option>
                                        <option>--Pilih--</option>
                                        @foreach ($kementrian as $e=>$kementrian2)
                                        <option value="{{ $kementrian2->nama_kementrian }}">{{ $kementrian2->nama_kementrian }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Riwayat Organisasi</label>
                            <textarea class="required form-control" name="riwayat_org" placeholder="Masukkan Riwayat Organisasi Anda">{{ $dt->riwayat_org }}</textarea>
                        </div>

                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Motivasi</label>
                            <textarea class="required form-control" name="motivasi" placeholder="Masukkan Motivasi Anda">{{ $dt->motivasi }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Kekurangan</label>
                            <textarea class="required form-control" name="kekurangan" placeholder="Masukkan Kekurangan Anda">{{ $dt->kekurangan }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Kelebihan</label>
                            <textarea class="required form-control" name="kelebihan" placeholder="Masukkan Kelebihan Anda">{{ $dt->kelebihan }}</textarea>
                        </div>

                    </div>
                    @endforeach
                    <div class="border-top">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="{{ url('formulir/') }}" class="btn btn-block btn-danger">BATAL</a>
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-block btn-success">SIMPAN</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                @else
                <h3 class="text-center"><strong>Formulir Sudah Diterima</strong></h3>
                <br>
                <a href="{{ url('/formulir') }}" class="btn btn-block btn-success">KEMBALI</a>
                @endif
            @endif
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        $('.picture-src').css("width", "150px");
        $('.picture-src').css("height", "200px");

        $('.picture-src2').css("width", "350px");
        $('.picture-src2').css("height", "200px");

        $('#ubahFotoX').hide();
        if ($('#photoPreview').attr('src') == "{{ asset('assets/images/formulir/no-image.jpeg') }}") {
            $('#hapusFoto').hide();
        } else {
            $('#hapusFoto').show();
        }
        $('#ubahFotoX2').hide();
        if ($('#photoPreviewKTM').attr('src') == "{{ asset('assets/images/formulir/no-image.jpeg') }}") {
            $('#hapusFoto2').hide();
        } else {
            $('#hapusFoto2').show();
        }
        $("#gambar").change(function(){
            readURL(this);
            var filename = $(this).val();
            if ($('#gambar').val() != '') {
                $('#ubahFotoX').show();
                $('.picture-src').css("width", "150px");
                $('.picture-src').css("height", "200px");
            } else {
                $('#photoPreview').attr('src', "{{ asset('assets/images/formulir/no-image.jpeg') }}").fadeIn('slow');
                $('#ubahFotoX').hide();
                $('.picture-src').css("width", "200px");
                $('.picture-src').css("height", "200px");
            }
        });
        $("#gambar2").change(function(){
            readURL2(this);
            var filename = $(this).val();
            if ($('#gambar2').val() != '') {
                $('#ubahFotoX2').show();
                $('.picture-src2').css("width", "350px");
                $('.picture-src2').css("height", "200px");
            } else {
                $('#photoPreviewKTM').attr('src', "{{ asset('assets/images/formulir/no-image.jpeg') }}").fadeIn('slow');
                $('#ubahFotoX2').hide();
                $('.picture-src2').css("width", "200px");
                $('.picture-src2').css("height", "200px");
            }
        });
    });
</script>

@foreach ($formulir as $e=>$dt)
<script>
    $(document).ready(function(){
        $("#kembalikan").click(function(){
            $('#photoPreview').attr('src', "{{ asset($dt->foto_diri) }}").fadeIn('slow');
            $('#gambar').val('');
            $('#ubahFotoX').hide();
            $('.picture-src').css("width", "150px");
            $('.picture-src').css("height", "200px");
        });
        $("#kembalikan2").click(function(){
            $('#photoPreviewKTM').attr('src', "{{ asset($dt->foto_ktm) }}").fadeIn('slow');
            $('#gambar2').val('');
            $('#ubahFotoX2').hide();
            $('.picture-src2').css("width", "350px");
            $('.picture-src2').css("height", "200px");
        });
    });
</script>
@endforeach

@endsection

