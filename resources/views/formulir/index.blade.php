@extends('layouts-matrix.master')

@section('breadcrumb')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title alert">{{ $breadcrumb }}</h4>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="container-fluid">
    @if ($pengaturan[0]->status == 1)
        @if (count($formulir) == "0")
        <div class="col-md-12 alert alert-primary">{{ $title }}</div>
        @else
            @if ($formulir[0]->status == 0)
            <div class="col-md-12 alert alert-primary">{{ $title }}</div>
            @else
            <div class="col-md-12 alert alert-success">Formulir Sudah Diterima</div>
            @endif
        @endif
    @else
        <div class="col-md-12 alert alert-warning">PEMBERITAHUAN</div>
    @endif

    <div class="card">
        <div class="card-body">
            @if (count($formulir) == "0")
                @if ($pengaturan[0]->status == 1)
                <form class="form-horizontal" role="form" method="POST" action="{{ url('formulir') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="picture-container mb-2">
                                    <div class="picture">
                                        <img src="{{ asset('assets/images/formulir/no-image.jpeg') }}" class="picture-src" id="photoPreview" title="">
                                        <input type="file" name="foto_diri" id="gambar" class="">
                                    </div>
                                    <label class="text-center control-label col-form-label">FOTO FORMAL</label>
                                </div>
                                <div class="btn-group btn-block" id="ubahFotoX" role="group" aria-label="Aksi">
                                    <button type="button" class="btn btn-secondary rounded-0" id="kembalikan"><span class="fe fe-rotate-ccw fe-16 mr-1"></span>Kembali</button>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="picture-container mb-2">
                                    <div class="picture">
                                        <img src="{{ asset('assets/images/formulir/no-image.jpeg') }}" class="picture-src2" id="photoPreviewKTM" title="">
                                        <input type="file" name="foto_ktm" id="gambar2" class="">
                                    </div>
                                    <label class="text-center control-label col-form-label">FOTO KTM</label>
                                </div>
                                <div class="btn-group btn-block" id="ubahFotoX2" role="group" aria-label="Aksi">
                                    <button type="button" class="btn btn-secondary rounded-0" id="kembalikan2"><span class="fe fe-rotate-ccw fe-16 mr-1"></span>Kembali</button>
                                </div>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">NIM</label>
                                    <input type="number" class="required form-control" name="nim" placeholder="Masukkan NIM Anda">
                                </div>

                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">Jenis Kelamin</label>
                                    <select class="required select2 form-control custom-select" name="jenkel" style="width: 100%; height:36px;">
                                        <option>--Pilih--</option>
                                        <option value="Pria">Pria</option>
                                        <option value="Wanita">Wanita</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">Kelas</label>
                                    <select class="required select2 form-control custom-select" name="kelas" style="width: 100%; height:36px;">
                                        <option>--Pilih--</option>
                                        <option value="1-A">1-A</option>
                                        <option value="1-B">1-B</option>
                                        <option value="1-C">1-C</option>
                                        <option value="1-D">1-D</option>
                                        <option value="1-E">1-E</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">Kementrian 1</label>
                                    <select class="required select2 form-control custom-select" name="kementrian1" style="width: 100%; height:36px;">
                                        <option>--Pilih--</option>
                                        @if (count($hasil) != "0")
                                        @foreach ($hasil as $e=>$dt)
                                            @if ($dt->kementrian == 'lugri')
                                                <option value="Kementrian Luar Negeri">
                                                    Kementrian Luar Negeri
                                                </option>
                                            @elseif ($dt->kementrian == 'sosma')
                                                <option value="Kementrian Sosial Masyarakat">
                                                    Kementrian Sosial Masyarakat
                                                </option>
                                            @elseif ($dt->kementrian == 'dagri')
                                                <option value="Kementrian Dalam Negeri">
                                                    Kementrian Dalam Negeri
                                                </option>
                                            @elseif ($dt->kementrian == 'kemhas')
                                                <option value="Kementrian Kemahasiswaan">
                                                    Kementrian Kemahasiswaan
                                                </option>
                                            @elseif ($dt->kementrian == 'psdm')
                                                <option value="Kementrian Pengembangan Sumber Daya Mahasiswa">
                                                    Kementrian Pengembangan Sumber Daya Mahasiswa
                                                </option>
                                            @elseif ($dt->kementrian == 'agama')
                                                <option value="Kementrian Agama">
                                                    Kementrian Agama
                                                </option>
                                            @elseif ($dt->kementrian == 'kominfo')
                                                <option value="Kementrian Komunikasi dan Informasi">
                                                    Kementrian Komunikasi dan Informasi
                                                </option>
                                            @else
                                                Tidak ada yang cocok
                                            @endif
                                        @endforeach
                                        @else
                                        @foreach ($kementrian as $e=>$dt)
                                        <option value="{{ $dt->nama_kementrian }}">{{ $dt->nama_kementrian }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">Nama Lengkap</label>
                                    <input type="text" class="required form-control" name="nama" placeholder="Masukkan Nama Lengkap Anda">
                                </div>

                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">PRODI</label>
                                    <select class="required select2 form-control custom-select" name="prodi" style="width: 100%; height:36px;">
                                        <option>--Pilih--</option>
                                        <option value="Akuntansi">Akuntansi</option>
                                        <option value="Manajemen Informatika">Manajemen Informatika</option>
                                        <option value="Teknik Mesin">Teknik Mesin</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">No. WA</label>
                                    <input type="text" class="required form-control" name="no_wa" placeholder="Masukkan Nomor WhatsApp Anda">
                                </div>

                                <div class="form-group">
                                    <label class="text-right control-label col-form-label">Kementrian 2 (Opsional)</label>
                                    <select class="required select2 form-control custom-select" name="kementrian2" style="width: 100%; height:36px;">
                                        <option>--Pilih--</option>
                                        @foreach ($kementrian as $e=>$dt)
                                        <option value="{{ $dt->nama_kementrian }}">{{ $dt->nama_kementrian }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Riwayat Organisasi</label>
                            <textarea class="required form-control" name="riwayat_org" placeholder="Masukkan Riwayat Organisasi Anda"></textarea>
                        </div>

                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Motivasi</label>
                            <textarea class="required form-control" name="motivasi" placeholder="Masukkan Motivasi Anda"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Kekurangan</label>
                            <textarea class="required form-control" name="kekurangan" placeholder="Masukkan Kekurangan Anda"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Kelebihan</label>
                            <textarea class="required form-control" name="kelebihan" placeholder="Masukkan Kelebihan Anda"></textarea>
                        </div>
                    </div>
                    <div class="border-top">
                        <div class="card-body">
                            <button type="submit" class="btn btn-block btn-success">SIMPAN</button>
                        </div>
                    </div>
                </form>
                @else
                <h3 class="text-center"><strong>MAAF, MENU FORMULIR SEDANG DITUTUP.</strong></h3>
                @endif
            @else
            {{-- DETAIL --}}
            @foreach ($formulir as $e=>$dt)
            <div class="card-body">

                <div class="row">
                    <div class="col-md-6">
                        <div class="picture-fr-container mb-2">
                            <div class="picture-fr">
                                <img src="{{ asset( $dt->foto_diri ) }}" class="picture-prev" id="photoPreview">
                            </div>
                            <label class="text-center control-label col-form-label">FOTO FORMAL</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="picture-fr-container mb-2">
                            <div class="picture-fr">
                                <img src="{{ asset( $dt->foto_ktm ) }}" class="picture-prev2" id="photoPreviewKTM">
                            </div>
                            <label class="text-center control-label col-form-label">FOTO KTM</label>
                        </div>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-right control-label col-form-label">NIM</label>
                            <input type="number" class="form-control" value="{{ $dt->nim }}" readonly>
                        </div>

                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Jenis Kelamin</label>
                            <input type="text" class="form-control" value="{{ $dt->jenkel }}" readonly>
                        </div>

                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Kelas</label>
                            <input type="text" class="form-control" value="{{ $dt->kelas }}" readonly>
                        </div>

                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Kementrian 1</label>
                            <input type="text" class="form-control" value="{{ $dt->kementrian1 }}" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" value="{{ $dt->nama }}" readonly>
                        </div>

                        <div class="form-group">
                            <label class="text-right control-label col-form-label">PRODI</label>
                            <input type="text" class="form-control" value="{{ $dt->prodi }}" readonly>
                        </div>

                        <div class="form-group">
                            <label class="text-right control-label col-form-label">No. WA</label>
                            <input type="text" class="form-control" value="{{ $dt->no_wa }}" readonly>
                        </div>

                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Kementrian 2 (Opsional)</label>
                            <input type="text" class="form-control" value="{{ $dt->kementrian2 }}" readonly>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="text-right control-label col-form-label">Riwayat Organisasi</label>
                    <textarea class="form-control" readonly>{{ $dt->riwayat_org }}</textarea>
                </div>

                <div class="form-group">
                    <label class="text-right control-label col-form-label">Motivasi</label>
                    <textarea class="form-control" readonly>{{ $dt->motivasi }}</textarea>
                </div>
                <div class="form-group">
                    <label class="text-right control-label col-form-label">Kekurangan</label>
                    <textarea class="form-control" readonly>{{ $dt->kekurangan }}</textarea>
                </div>
                <div class="form-group">
                    <label class="text-right control-label col-form-label">Kelebihan</label>
                    <textarea class="form-control" readonly>{{ $dt->kelebihan }}</textarea>
                </div>
            </div>
            @if ($dt->status == 0)
            <div class="border-top">
                <div class="card-body">
                    <a href="{{ url('/formulir/edit') }}" class="btn btn-block btn-warning">UBAH</a>
                </div>
            </div>
            @else
            @endif

            @endforeach
            @endif
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        $('.picture-src').css("width", "200px");
        $('.picture-src').css("height", "200px");

        $('.picture-src2').css("width", "200px");
        $('.picture-src2').css("height", "200px");

        $('#ubahFotoX').hide();
        if ($('#photoPreview').attr('src') == "{{ asset('assets/images/formulir/no-image.jpeg') }}") {
            $('#hapusFoto').hide();
        } else {
            $('#hapusFoto').show();
        }
        $('#ubahFotoX2').hide();
        if ($('#photoPreviewKTM').attr('src') == "{{ asset('assets/images/formulir/no-image.jpeg') }}") {
            $('#hapusFoto2').hide();
        } else {
            $('#hapusFoto2').show();
        }
        $("#gambar").change(function(){
            readURL(this);
            var filename = $(this).val();
            if ($('#gambar').val() != '') {
                $('#ubahFotoX').show();
                $('.picture-src').css("width", "150px");
                $('.picture-src').css("height", "200px");
            } else {
                $('#photoPreview').attr('src', "{{ asset('assets/images/formulir/no-image.jpeg') }}").fadeIn('slow');
                $('#ubahFotoX').hide();
                $('.picture-src').css("width", "200px");
                $('.picture-src').css("height", "200px");
            }
        });
        $("#gambar2").change(function(){
            readURL2(this);
            var filename = $(this).val();
            if ($('#gambar2').val() != '') {
                $('#ubahFotoX2').show();
                $('.picture-src2').css("width", "350px");
                $('.picture-src2').css("height", "200px");
            } else {
                $('#photoPreviewKTM').attr('src', "{{ asset('assets/images/formulir/no-image.jpeg') }}").fadeIn('slow');
                $('#ubahFotoX2').hide();
                $('.picture-src2').css("width", "200px");
                $('.picture-src2').css("height", "200px");
            }
        });
    });
</script>

<script>
    $(document).ready(function(){
        $("#kembalikan").click(function(){
            $('#photoPreview').attr('src', "{{ asset('assets/images/formulir/no-image.jpeg') }}").fadeIn('slow');
            $('#gambar').val('');
            $('#ubahFotoX').hide();
            $('.picture-src').css("width", "200px");
            $('.picture-src').css("height", "200px");
        });
        $("#kembalikan2").click(function(){
            $('#photoPreviewKTM').attr('src', "{{ asset('assets/images/formulir/no-image.jpeg') }}").fadeIn('slow');
            $('#gambar2').val('');
            $('#ubahFotoX2').hide();
            $('.picture-src2').css("width", "200px");
            $('.picture-src2').css("height", "200px");
        });
    });
</script>

@endsection

