@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <h4>{{ $title }}</h4>
        <div class="box box-warning">
            <div class="box-header">
                <p>
                    <a href="{{ url('penilaian') }}" class="btn btn-sm btn-flat btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>

                    <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                </p>
            </div>
            <div class="box-body">

                <div class="box-body">
                    <form role="form" method="POST" name="form" action="{{ url('penilaian') }}">
                        @csrf
                        {{ method_field('put') }}
                        @foreach ($data as $e=>$dt)
                        <ul class="list-group mt-4 mb-4">
                            <li class="list-group-item">
                                <strong>{{ $e+1 }}. {{ $dt->soals->soalurn }}</strong><br>
                                <input type="hidden" name="user_id[{{$dt->soals->id }}]" value="{{ $dt->user_id }}">
                                <input type="hidden" name="soal_id[{{ $dt->soals->id }}]" value="{{ $dt->soals->id }}" size="2">
                                <br>
                                <div class="row">
                                    <div class="col-md-10">
                                        <textarea class="form-control" readonly>{{ $dt->jawab }}</textarea>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="number" name="skor[{{ $dt->soals->id }}]" id="skor{{ $dt->soals->id }}" onkeyup="mult{{ $dt->soals->id }}(this.value);" value="0" class="form-control" style="width: 70px" max="20" min="0">
                                        @if ($dt->n1 != null)
                                        <input type="hidden" name="nilai[{{ $dt->soals->id }}]" value="{{$dt->n1}}" class="form-control" style="width: 70px" max="20" min="0">
                                        @else
                                        <input type="hidden" name="nilai[{{ $dt->soals->id }}]" value="0" class="form-control" style="width: 70px" max="20" min="0">
                                        @endif
                                    </div>
                                </div>
                            </li>
                        </ul>
                        @endforeach

                        <br>
                        <button id="kirim" type="submit" class="btn btn-block btn-success">SIMPAN</button>
                    </form>

                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    function mult1(value) {
        var x;
        if (value > 20) {
            x = 20
            document.getElementById('skor1').value = x;
        } if (value < 0) {
            x = 0
            document.getElementById('skor1').value = x;
        }
    }
    function mult2(value) {
        var x;
        if (value > 20) {
            x = 20
            document.getElementById('skor2').value = x;
        } if (value < 0) {
            x = 0
            document.getElementById('skor2').value = x;
        }
    }
    function mult3(value) {
        var x;
        if (value > 20) {
            x = 20
            document.getElementById('skor3').value = x;
        } if (value < 0) {
            x = 0
            document.getElementById('skor3').value = x;
        }
    }
    function mult4(value) {
        var x;
        if (value > 20) {
            x = 20
            document.getElementById('skor4').value = x;
        } if (value < 0) {
            x = 0
            document.getElementById('skor4').value = x;
        }
    }
    function mult5(value) {
        var x;
        if (value > 20) {
            x = 20
            document.getElementById('skor5').value = x;
        } if (value < 0) {
            x = 0
            document.getElementById('skor5').value = x;
        }
    }
</script>

<script type="text/javascript">
    $(document).ready(function(){

        // btn refresh
        $('.btn-refresh').click(function(e){
            e.preventDefault();
            $('.preloader').fadeIn();
            location.reload();
        })

    })
</script>

@endsection
