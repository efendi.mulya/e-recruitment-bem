@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <h4>{{ $title }}</h4>
        <div class="box box-warning">
            <div class="box-header">
                <p>
                    <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                </p>
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-stripped myTable">
                        <thead>
                            <tr>
                                <th>Nama Peserta</th>
                                <th>Nilai Pilihanganda</th>
                                <th>Nilai Uraian</th>
                                <th>Penilaian Asesor</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $e=>$dt)
                            <tr>
                                <td>{{ $dt->namas->name }}</td>
                                <td>{{ $dt->nilaipg }}</td>
                                <td>
                                    @if ($dt->nilaiurn == null)
                                        <label class="label label-warning">MENUNGGU PENILAIAN</label>
                                    @else
                                        {{ $dt->nilaiurn }}
                                    @endif
                                </td>
                                <td>
                                    @if ( $dt->a1 == \Auth::user()->id)
                                    {{ $dt->total1 }}
                                    @elseif ($dt->a2 == \Auth::user()->id)
                                    {{ $dt->total2 }}
                                    @else
                                    <label class="label label-warning">MENUNGGU PENILAIAN</label>
                                    @endif
                                </td>
                                <td>
                                    <div style="width:80px">
                                        @if ($dt->nilaiurn == null)
                                            @if ($dt->a1 == \Auth::user()->id)
                                            <label class="label label-success">SUDAH ANDA DINILAI</label>
                                            @elseif ($dt->a2 == \Auth::user()->id)
                                            <label class="label label-success">SUDAH ANDA DINILAI</label>
                                            @else
                                            <a href="{{ url('penilaian/'.$dt->user_id) }}"
                                                class="btn btn-warning btn-xs btn-edit" id="edit"><i class="fa fa-pencil-square-o"></i>
                                            </a>
                                            @endif
                                        @else
                                            <label class="label label-success">SUDAH DINILAI</label>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){

        // btn refresh
        $('.btn-refresh').click(function(e){
            e.preventDefault();
            $('.preloader').fadeIn();
            location.reload();
        })

    })
</script>

@endsection
