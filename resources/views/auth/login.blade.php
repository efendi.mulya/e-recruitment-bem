<!DOCTYPE html>
<html lang="en">

    <head>
        <title>E-Recruitment Login</title><meta charset="UTF-8" />
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('matrix/assets/images/favicon.png') }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="{{ asset('matrix/css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('matrix/css/bootstrap-responsive.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('matrix/css/matrix-login.css') }}" />
        <link href="{{ asset('matrix/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    </head>
    <body>
        <div id="loginbox">

            <form id="loginform" class="form-vertical" method="POST" action="{{ route('login') }}">
                @csrf
				 <div class="control-group normal_text"> <h3><img src="{{ asset('matrix/img/logo.png') }}" alt="Logo" /></h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span>
                            <input  id="email" type="email" class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email" />
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span>
                            <input id="password" type="password" class="@error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password" />
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-info" id="to-recover">Buat Akun</a></span>
                    <!-- <span class="pull-left"><a href="register.html" class="btn btn-info" id="to-recover">Buat Akun 2</a></span> -->
                    <span class="pull-right">
                        {{-- <a type="submit" href="index.html" class="btn btn-success"> Masuk</a> --}}
                        <button type="submit" class="btn btn-success">{{ __('Masuk') }}</button>
                    </span>
                </div>
            </form>

            <form id="recoverform" method="POST" action="{{ route('register') }}" class="form-vertical">
                @csrf
				<div class="control-group normal_text"> <h3><img src="{{ asset('matrix/img/logo.png') }}" alt="Logo" /></h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lb"><i class="icon-info-sign"> </i></span>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="Name" />
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email" />
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password" />
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lr"><i class="icon-lock"></i></span>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password" />
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-info" id="to-login">&laquo; Kembali</a></span>
                    <span class="pull-right">
                        {{-- <a href="#" class="btn btn-success">Daftar</a> --}}
                        <button type="submit" class="btn btn-success">{{ __('Daftar') }}</button>
                    </span>
                </div>
            </form>

        </div>

        <script src="{{ asset('matrix/js/jquery.min.js') }}"></script>
        <script src="{{ asset('matrix/js/matrix.login.js') }}"></script>
    </body>

</html>
