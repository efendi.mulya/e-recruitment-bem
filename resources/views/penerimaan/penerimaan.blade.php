@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <h4>{{ $title }}</h4>
        <div class="box box-warning">
            <div class="box-header">
                <p>
                    @foreach ($data as $e=>$dt)
                    <a href="{{ url('penerimaan') }}" class="btn btn-sm btn-flat btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>

                    <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>

                    @if ($dt->status == "0")
                    <button class="btn btn-sm btn-flat btn-success btn-approved"><i class="fa fa-check"></i> Terima</button>
                    @else
                    @endif

                    @endforeach
                </p>
            </div>
            <div class="box-body">
                @foreach ($data as $e=>$dt)
                    @if ($dt->status == 1)
                        <div class="col-md-12 alert alert-success">FORMULIR SUDAH DITERIMA</div>
                    @else
                    @endif
                @endforeach

                <form role="form" method="POST" name="form" action="">
                    <div class="box-body">
                    @foreach ($data as $e=>$dt)

                        <div class="row">
                            <div class="col-md-6">
                                <div class="picture-fr-container mb-2">
                                    <div class="picture-fr">
                                        <img src="{{ asset( $dt->foto_diri ) }}" class="picture-prev" id="photoPreview">
                                    </div>
                                    <label class="text-center control-label col-form-label">FOTO FORMAL</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="picture-fr-container mb-2">
                                    <div class="picture-fr">
                                        <img src="{{ asset( $dt->foto_ktm ) }}" class="picture-prev2" id="photoPreviewKTM">
                                    </div>
                                    <label class="text-center control-label col-form-label">FOTO KTM</label>
                                </div>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>NIM</label>
                                    <input type="number" class="form-control" value="{{ $dt->nim }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <input type="text" class="form-control" value="{{ $dt->jenkel }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label>Kelas</label>
                                    <input type="text" class="form-control" value="{{ $dt->kelas }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label>Kementrian 1</label>
                                    <input type="text" class="form-control" value="{{ $dt->kementrian1 }}" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Lengkap</label>
                                    <input type="text" class="form-control" value="{{ $dt->nama }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label>PRODI</label>
                                    <input type="text" class="form-control" value="{{ $dt->prodi }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label>No. WA</label>
                                    <input type="text" class="form-control" value="{{ $dt->no_wa }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label>Kementrian 2 (Opsional)</label>
                                    <input type="text" class="form-control" value="{{ $dt->kementrian2 }}" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Riwayat Organisasi</label>
                            <textarea class="form-control" readonly>{{ $dt->riwayat_org }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Motivasi</label>
                            <textarea class="form-control" readonly>{{ $dt->motivasi }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Kekurangan</label>
                            <textarea class="form-control" readonly>{{ $dt->kekurangan }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Kelebihan</label>
                            <textarea class="form-control" readonly>{{ $dt->kelebihan }}</textarea>
                        </div>

                    @endforeach
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

{{-- modal --}}
<div class="modal fade" id="modal-approved" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
    <div class="modal-dialog modal-default modal-dialog-centered modal-" role="document">
      <div class="modal-content bg-gradient-danger">

        <div class="modal-header">
          {{-- <h6 class="modal-title" id="modal-title-notification">Your attention is required</h6> --}}
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>

        <div class="modal-body">

          <div class="py-3 text-center">
            <i class="ni ni-bell-55 ni-3x"></i>
            <h4 class="heading mt-4">Apakah kamu yakin ingin menerima formulir ini?</h4>
          </div>

        </div>

        <div class="modal-footer">
          <form action="{{ url('penerimaan/'.$dt->user_id) }}" method="post">
            {{ csrf_field() }}
            <p>
            <button type="submit" class="btn btn-success btn-flat btn-sm menu-sidebar">TERIMA</button>
            <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">TUTUP</button>
            </p>
          </form>
        </div>

      </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){

        $('.btn-approved').click(function(){
            $('#modal-approved').modal();
        })

        // btn refresh
        $('.btn-refresh').click(function(e){
            e.preventDefault();
            $('.preloader').fadeIn();
            location.reload();
        })

    })
</script>

<script>
    $(document).ready(function(){
        $('.picture-src').css("width", "200px");
        $('.picture-src').css("height", "200px");

        $('.picture-src2').css("width", "200px");
        $('.picture-src2').css("height", "200px");

        $('#ubahFotoX').hide();
        if ($('#photoPreview').attr('src') == "{{ asset('assets/images/formulir/no-image.jpeg') }}") {
            $('#hapusFoto').hide();
        } else {
            $('#hapusFoto').show();
        }
        $('#ubahFotoX2').hide();
        if ($('#photoPreviewKTM').attr('src') == "{{ asset('assets/images/formulir/no-image.jpeg') }}") {
            $('#hapusFoto2').hide();
        } else {
            $('#hapusFoto2').show();
        }
        $("#gambar").change(function(){
            readURL(this);
            var filename = $(this).val();
            if ($('#gambar').val() != '') {
                $('#ubahFotoX').show();
                $('.picture-src').css("width", "150px");
                $('.picture-src').css("height", "200px");
            } else {
                $('#photoPreview').attr('src', "{{ asset('assets/images/formulir/no-image.jpeg') }}").fadeIn('slow');
                $('#ubahFotoX').hide();
                $('.picture-src').css("width", "200px");
                $('.picture-src').css("height", "200px");
            }
        });
        $("#gambar2").change(function(){
            readURL2(this);
            var filename = $(this).val();
            if ($('#gambar2').val() != '') {
                $('#ubahFotoX2').show();
                $('.picture-src2').css("width", "350px");
                $('.picture-src2').css("height", "200px");
            } else {
                $('#photoPreviewKTM').attr('src', "{{ asset('assets/images/formulir/no-image.jpeg') }}").fadeIn('slow');
                $('#ubahFotoX2').hide();
                $('.picture-src2').css("width", "200px");
                $('.picture-src2').css("height", "200px");
            }
        });
    });
</script>

<script>
    $(document).ready(function(){
        $("#kembalikan").click(function(){
            $('#photoPreview').attr('src', "{{ asset('assets/images/formulir/no-image.jpeg') }}").fadeIn('slow');
            $('#gambar').val('');
            $('#ubahFotoX').hide();
            $('.picture-src').css("width", "200px");
            $('.picture-src').css("height", "200px");
        });
        $("#kembalikan2").click(function(){
            $('#photoPreviewKTM').attr('src', "{{ asset('assets/images/formulir/no-image.jpeg') }}").fadeIn('slow');
            $('#gambar2').val('');
            $('#ubahFotoX2').hide();
            $('.picture-src2').css("width", "200px");
            $('.picture-src2').css("height", "200px");
        });
    });
</script>

@endsection
