@extends('layouts-matrix.master')

@section('breadcrumb')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title alert">{{ $breadcrumb }}</h4>
        </div>
    </div>
</div>
@endsection

@section('content')

<?php
    $usr = \Auth::user()->id;
    $uj = \DB::select("SELECT * FROM m_datauji WHERE user_id = '$usr'");
    $hsl = \DB::select("SELECT * FROM m_datahasil WHERE user_id = '$usr'");
?>

<div class="container-fluid">
    @if (count($uj) == "0" && count($hsl) == "0")
    <div class="col-md-12 alert alert-danger">PERINGATAN</div>
    @elseif (count($uj) != "0" && count($hsl) != "0")
    <div class="col-md-12 alert alert-danger">PERINGATAN</div>
    @elseif (count($uj) != "0" && count($hsl) == "0")
    <div class="col-md-12 alert alert-primary">{{ $title }}</div>
    @endif

    <div class="card">
        <div class="card-body">
            @if (count($uj) == "0" && count($hsl) == "0")
            <h3 class="text-center">Anda belum menjawab pertanyaan yang ada.</h3>
            @elseif (count($uj) != "0" && count($hsl) != "0")
            <h3 class="text-center">Anda sudah mendapatkan rekomendasi kementrian.</h3>
            @elseif (count($uj) != "0" && count($hsl) == "0")
            <form role="form" method="POST" name="form" action="{{ url('spk/finish') }}">
            @csrf

            <h3 class="text-center">Klik tombol di bawah untuk melihat hasil rekomendasi kementrian</h3>
            <ul class="list-group mt-4 mb-4" style="display: none">
            @foreach ($vk as $e=>$dt)
                <li class="list-group-item" >
                    <strong>{{ $e+1 }}. </strong><br>
                    <input type="text" name="user_id[{{ $dt->id }}]" value="{{ \Auth::user()->id }}">
                    <input type="text" name="spk_id[{{ $dt->id }}]" value="{{ $dt->id }}" size="2">
                    <br>
                    {{-- vektor --}}
                    <input type="text" name="vklugri[{{ $dt->id }}]" value="{{ $dt->lugri }}" size="2">
                    <input type="text" name="vksosma[{{ $dt->id }}]" value="{{ $dt->sosma }}" size="2">
                    <input type="text" name="vkdagri[{{ $dt->id }}]" value="{{ $dt->dagri }}" size="2">
                    <input type="text" name="vkkemhas[{{ $dt->id }}]" value="{{ $dt->kemhas }}" size="2">
                    <input type="text" name="vkpsdm[{{ $dt->id }}]" value="{{ $dt->psdm }}" size="2">
                    <input type="text" name="vkagama[{{ $dt->id }}]" value="{{ $dt->agama }}" size="2">
                    <input type="text" name="vkkominfo[{{ $dt->id }}]" value="{{ $dt->kominfo }}" size="2">
                </li>
            @endforeach
            @foreach ($uji as $e=>$dt)
                <li class="list-group-item" >
                    {{-- vektor --}}
                    <input type="text" name="lugri[{{ $dt->spk_id }}]" value="{{ $dt->lugri }}" size="2">
                    <input type="text" name="sosma[{{ $dt->spk_id }}]" value="{{ $dt->sosma }}" size="2">
                    <input type="text" name="dagri[{{ $dt->spk_id }}]" value="{{ $dt->dagri }}" size="2">
                    <input type="text" name="kemhas[{{ $dt->spk_id }}]" value="{{ $dt->kemhas }}" size="2">
                    <input type="text" name="psdm[{{ $dt->spk_id }}]" value="{{ $dt->psdm }}" size="2">
                    <input type="text" name="agama[{{ $dt->spk_id }}]" value="{{ $dt->agama }}" size="2">
                    <input type="text" name="kominfo[{{ $dt->spk_id }}]" value="{{ $dt->kominfo }}" size="2">
                </li>
            @endforeach
            </ul>

            <br>
            <button type="submit" class="btn btn-block btn-success">Lihat Hasil</button>
            </form>
            @endif
        </div>
    </div>

</div>

@endsection

