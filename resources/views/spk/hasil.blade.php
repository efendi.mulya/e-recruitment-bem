@extends('layouts-matrix.master')

@section('breadcrumb')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title alert">{{ $breadcrumb }}</h4>
        </div>
    </div>
</div>
@endsection

@section('content')

<?php
    $usr = \Auth::user()->id;
    $uj = \DB::select("SELECT * FROM m_datauji WHERE user_id = '$usr'");
    $hsl = \DB::select("SELECT * FROM m_datahasil WHERE user_id = '$usr'");
?>

<div class="container-fluid">
    @if (count($uj) == "0" && count($hsl) == "0")
    <div class="col-md-12 alert alert-danger">PERINGATAN</div>
    @elseif (count($uj) != "0" && count($hsl) == "0")
    <div class="col-md-12 alert alert-danger">PERINGATAN</div>
    @elseif (count($uj) != "0" && count($hsl) != "0")
    <form id="form" method="POST" action="{{ action('Spk_controller@delete',$usr) }}">
        @csrf
        {{ method_field('delete') }}
        <input type="hidden" name="_method" value="DELETE">
        <div class="col-md-12 alert alert-primary">Lakukan pemilihan ulang rekomendasi kementrian dengan klik
            <strong>
                <a onclick="document.getElementById('form').submit();" href="#">di sini.</a>
            </strong>
        </div>
    </form>
    @endif

    <div class="card">
        <div class="card-header">
            <h3 class="text-center"><strong>HASIL REKOMENDASI</strong></h3>
        </div>
        <div class="card-body">
            <br>
            @if (count($uj) == "0" && count($hsl) == "0")
            <h3 class="text-center"><strong>Anda belum mendapatkan rekomendasi kementrian.</strong></h3>
            @elseif (count($uj) != "0" && count($hsl) == "0")
            <h3 class="text-center"><strong>Anda belum mendapatkan rekomendasi kementrian.</strong></h3>
            @elseif (count($uj) != "0" && count($hsl) != "0")
            <h4 class="text-center">Kami merekomendasikan anda untuk memilih
                @foreach ($hasil as $e=>$dt)
                <strong>
                    @if ($dt->kementrian == 'lugri')
                        Kementrian Luar Negeri
                    @elseif ($dt->kementrian == 'sosma')
                        Kementrian Sosial Masyarakat
                    @elseif ($dt->kementrian == 'dagri')
                        Kementrian Dalam Negeri
                    @elseif ($dt->kementrian == 'kemhas')
                        Kementrian Kemahasiswaan
                    @elseif ($dt->kementrian == 'psdm')
                        Kementrian Pengembangan Sumber Daya Mahasiswa
                    @elseif ($dt->kementrian == 'agama')
                        Kementrian Agama
                    @elseif ($dt->kementrian == 'kominfo')
                        Kementrian Komunikasi dan Informasi
                    @else
                        Tidak ada yang cocok
                    @endif
                </strong>
                @endforeach
            </h4>
            @endif
            <br>
        </div>
    </div>

</div>

<script type="text/javascript">
    const checkbox = document.querySelector('#customControlAutosizing1');
    const button = document.querySelector('#mulai');

    checkbox.addEventListener('change', function() {
        if(this.checked) {
            button.disabled = false;
        } else {
            button.disabled = true;
        }
    });
</script>

@endsection

