@extends('layouts-matrix.master')

@section('breadcrumb')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title alert">{{ $breadcrumb }}</h4>
            {{-- <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Library</li>
                    </ol>
                </nav>
            </div> --}}
        </div>
    </div>
</div>
@endsection

@section('content')

<?php
    $usr = \Auth::user()->id;
    $uj = \DB::select("SELECT * FROM m_datauji WHERE user_id = '$usr'");
    $hsl = \DB::select("SELECT * FROM m_datahasil WHERE user_id = '$usr'");
?>

<div class="container-fluid">
    @if (count($uj) != "0" && count($hsl) != "0")
    <div class="col-md-12 alert alert-danger">PERINGATAN</div>
    @elseif (count($uj) != "0" && count($hsl) == "0")
    <div class="col-md-12 alert alert-primary">{{ $title }}</div>
    @elseif (count($uj) == "0" && count($hsl) == "0")
    <div class="col-md-12 alert alert-primary">{{ $title }}</div>
    @endif

    <div class="card">
        <div class="card-body">

            @if (count($uj) != "0" && count($hsl) != "0")
                <h3 class="text-center"><strong>Anda sudah mendapatkan rekomendasi kementrian.</strong></h3>
            @elseif (count($uj) != "0" && count($hsl) == "0")
                <div id="greeting">
                    <h3 class="text-center">BACA TERLEBIH DAHULU</h3>
                    <br>
                    <div class="container">
                        <p>1. Bacalah dengan teliti tiap-tiap soal sebelum menjawab.</p>
                        <p>2. Jawab dengan jujur sesuai dengan apa yang ada dalam diri anda.</p>
                        <p>3. Hasil rekomendasi bukan hal yang mutlak, anda bisa menjadikannya acuan atau saran dalam memilih kementrian.</p>
                    </div>
                    <br>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="customControlAutosizing1">
                        <label class="custom-control-label" for="customControlAutosizing1">Saya mengerti dan setuju.</label>
                    </div>
                    <br>
                </div>
                <button id="mulai" class="btn btn-block btn-primary" disabled>MULAI</button>

                <form role="form" method="POST" name="form" action="{{ url('spk/ulang') }}">
                    @csrf
                    {{ method_field('put') }}
                    <ul class="list-group mt-4 mb-4">
                    @foreach ($vk as $e=>$dt)
                        <li class="list-group-item"  id="soal{{ $e+1 }}" style="display: none">
                            <strong>{{ $e+1 }}. {{ $dt->soal_spk }}</strong><br>
                            <input type="hidden" name="user_id[{{ $dt->id }}]" value="{{ \Auth::user()->id }}">
                            <input type="hidden" name="spk_id[{{ $dt->id }}]" value="{{ $dt->id }}" size="2">
                            <br>
                            <ul class='list-group mt-4 mb-4'>

                                @if (strpos($dt->soal_spk, 'siap'))
                                <li class="list-group-item">
                                    <label><input type="radio" id="ss{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="1"> Sangat Siap</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="s{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.75"> Siap</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="cs{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.5"> Cukup Siap</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="ks{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.25"> Kurang Siap</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="ts{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0"> Tidak Siap</label>
                                </li>
                                @elseif (strpos($dt->soal_spk, 'suka'))
                                <li class="list-group-item">
                                    <label><input type="radio" id="ss{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="1"> Sangat Suka</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="s{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.75"> Suka</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="cs{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.5"> Cukup Suka</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="ks{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.25"> Kurang Suka</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="ts{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0"> Tidak Suka</label>
                                </li>
                                @elseif (strpos($dt->soal_spk, 'mampu'))
                                <li class="list-group-item">
                                    <label><input type="radio" id="ss{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="1"> Sangat Mampu</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="s{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.75"> Mampu</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="cs{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.5"> Cukup Mampu</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="ks{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.25"> Kurang Mampu</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="ts{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0"> Tidak Mampu</label>
                                </li>
                                @endif

                            </ul>
                            {{-- vektor --}}
                            <input type="hidden" name="lugri[{{ $dt->id }}]" value="{{ $dt->lugri }}" size="2">
                            <input type="hidden" name="sosma[{{ $dt->id }}]" value="{{ $dt->sosma }}" size="2">
                            <input type="hidden" name="dagri[{{ $dt->id }}]" value="{{ $dt->dagri }}" size="2">
                            <input type="hidden" name="kemhas[{{ $dt->id }}]" value="{{ $dt->kemhas }}" size="2">
                            <input type="hidden" name="psdm[{{ $dt->id }}]" value="{{ $dt->psdm }}" size="2">
                            <input type="hidden" name="agama[{{ $dt->id }}]" value="{{ $dt->agama }}" size="2">
                            <input type="hidden" name="kominfo[{{ $dt->id }}]" value="{{ $dt->kominfo }}" size="2">
                        </li>
                    @endforeach
                    </ul>

                    <br>
                    <button id="tombol" type="submit" class="btn btn-block btn-success" style="display: none">Lihat Hasil</button>
                </form>
            @elseif (count($uj) == "0" && count($hsl) == "0")
                <div id="greeting">
                    <h3 class="text-center">BACA TERLEBIH DAHULU</h3>
                    <br>
                    <div class="container">
                        <p>1. Bacalah dengan teliti tiap-tiap soal sebelum menjawab.</p>
                        <p>2. Jawab dengan jujur sesuai dengan apa yang ada dalam diri anda.</p>
                        <p>3. Hasil rekomendasi bukan hal yang mutlak, anda bisa menjadikannya acuan atau saran dalam memilih kementrian.</p>
                    </div>
                    <br>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="customControlAutosizing1">
                        <label class="custom-control-label" for="customControlAutosizing1">Saya mengerti dan setuju.</label>
                    </div>
                    <br>
                </div>
                <button id="mulai" class="btn btn-block btn-primary" disabled>MULAI</button>

                <form role="form" method="POST" name="form" action="{{ url('spk') }}">
                    @csrf
                    <ul class="list-group mt-4 mb-4">
                    @foreach ($vk as $e=>$dt)
                        <li class="list-group-item"  id="soal{{ $e+1 }}" style="display: none">
                            <strong>{{ $e+1 }}. {{ $dt->soal_spk }}</strong><br>
                            <input type="hidden" name="user_id[{{ $dt->id }}]" value="{{ \Auth::user()->id }}">
                            <input type="hidden" name="spk_id[{{ $dt->id }}]" value="{{ $dt->id }}" size="2">
                            <br>
                            <ul class='list-group mt-4 mb-4'>

                                @if (strpos($dt->soal_spk, 'siap'))
                                <li class="list-group-item">
                                    <label><input type="radio" id="ss{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="1"> Sangat Siap</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="s{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.75"> Siap</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="cs{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.5"> Cukup Siap</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="ks{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.25"> Kurang Siap</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="ts{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0"> Tidak Siap</label>
                                </li>
                                @elseif (strpos($dt->soal_spk, 'suka'))
                                <li class="list-group-item">
                                    <label><input type="radio" id="ss{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="1"> Sangat Suka</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="s{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.75"> Suka</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="cs{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.5"> Cukup Suka</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="ks{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.25"> Kurang Suka</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="ts{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0"> Tidak Suka</label>
                                </li>
                                @elseif (strpos($dt->soal_spk, 'mampu'))
                                <li class="list-group-item">
                                    <label><input type="radio" id="ss{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="1"> Sangat Mampu</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="s{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.75"> Mampu</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="cs{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.5"> Cukup Mampu</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="ks{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0.25"> Kurang Mampu</label>
                                </li>
                                <li class="list-group-item">
                                    <label><input type="radio" id="ts{{ $e+1 }}" name="jawab[{{ $dt->id }}]" value="0"> Tidak Mampu</label>
                                </li>
                                @endif

                            </ul>
                            {{-- vektor --}}
                            <input type="hidden" name="lugri[{{ $dt->id }}]" value="{{ $dt->lugri }}" size="2">
                            <input type="hidden" name="sosma[{{ $dt->id }}]" value="{{ $dt->sosma }}" size="2">
                            <input type="hidden" name="dagri[{{ $dt->id }}]" value="{{ $dt->dagri }}" size="2">
                            <input type="hidden" name="kemhas[{{ $dt->id }}]" value="{{ $dt->kemhas }}" size="2">
                            <input type="hidden" name="psdm[{{ $dt->id }}]" value="{{ $dt->psdm }}" size="2">
                            <input type="hidden" name="agama[{{ $dt->id }}]" value="{{ $dt->agama }}" size="2">
                            <input type="hidden" name="kominfo[{{ $dt->id }}]" value="{{ $dt->kominfo }}" size="2">
                        </li>
                    @endforeach
                    </ul>

                    <br>
                    <button id="tombol" type="submit" class="btn btn-block btn-success" style="display: none">Lihat Hasil</button>
                </form>
            @endif
        </div>
    </div>

</div>

<!--Show Hide js-->
<script type="text/javascript" src="{{ asset('showhide/function1.js') }}"></script>
<script type="text/javascript" src="{{ asset('showhide/function2.js') }}"></script>
<script type="text/javascript" src="{{ asset('showhide/function3.js') }}"></script>

<script type="text/javascript">
    const checkbox = document.querySelector('#customControlAutosizing1');
    const button = document.querySelector('#mulai');

    checkbox.addEventListener('change', function() {
        if(this.checked) {
            button.disabled = false;
        } else {
            button.disabled = true;
        }
    });
</script>

@endsection

