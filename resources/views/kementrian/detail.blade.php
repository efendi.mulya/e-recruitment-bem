@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <h4>{{ $title }}</h4>
        <div class="box box-warning">
            <div class="box-header">
                <p>
                    <a href="{{ url('kementrian') }}" class="btn btn-sm btn-flat btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>

                    <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                </p>
            </div>
            <div class="box-body">

                <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Kementrian</label>
                      <input type="text" class="form-control " name="nama_kementrian" value="{{ $dt->nama_kementrian }}" readonly/>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Tupoksi</label>
                      <textarea class="form-control" name="tupoksi" rows="5" readonly>{{ $dt->tupoksi }}</textarea>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Dibuat</label>
                      <input type="text" class="form-control " name="nama_kementrian" value="{{ $dt->created_at }}" readonly/>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Diperbarui</label>
                      <input type="text" class="form-control " name="nama_kementrian" value="{{ $dt->updated_at }}" readonly/>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        {{-- <th>Pilih Produk</th> --}}
                                        {{-- <th>#</th> --}}
                                        <th>Soal</th>
                                        <th>Jawab</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dt->lines as $ln)
                                    <tr>
                                        {{-- <td>{{ $e+1 }}</td> --}}
                                        <td>{{ $ln->soals->soal_spk }}</td>
                                        <td>{{ $ln->vektor }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                  </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){

        // btn refresh
        $('.btn-refresh').click(function(e){
            e.preventDefault();
            $('.preloader').fadeIn();
            location.reload();
        })

    })
</script>

@endsection
