@extends('layouts.master')

@section('content')

@php
    $data = DB::select('SELECT b.soal_spk,
	SUM(IF(kementrian=11, vektor,0)) AS Kementrian_Luar_Negeri,
	SUM(IF(kementrian=12, vektor,0)) AS Kementrian_Dalam_Negeri,
	SUM(IF(kementrian=13, vektor,0)) AS Kementrian_Sosial_Masyarakat,
	SUM(IF(kementrian=14, vektor,0)) AS Kementrian_Pengembangan_Sumber_Daya_Mahasiswa,
	SUM(IF(kementrian=15, vektor,0)) AS Kementrian_Kemahasiswaan,
	SUM(IF(kementrian=16, vektor,0)) AS Kementrian_Agama
FROM data_latih_line a, m_soalspk b
WHERE b.id = a.soalspk
GROUP BY b.soal_spk;');
@endphp

<div class="row">
    <div class="col-md-12">
        <h4>{{ $title }}</h4>
        <div class="box box-warning">
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-stripped">
                        <thead>
                            <tr>
                                {{-- <th>No</th> --}}
                                <th>Soal</th>
                                <th>Kemenlugri</th>
                                <th>Kemendagri</th>
                                <th>Kemensosma</th>
                                <th>PSDM</th>
                                <th>Kemhas</th>
                                <th>Kemenag</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $e=>$dt)
                            <tr>
                                {{-- <td>{{ $e+1 }}</td> --}}
                                <td>{{ $dt->soal_spk }}</td>
                                <td>{{ $dt->Kementrian_Luar_Negeri }}</td>
                                <td>{{ $dt->Kementrian_Dalam_Negeri }}</td>
                                <td>{{ $dt->Kementrian_Sosial_Masyarakat }}</td>
                                <td>{{ $dt->Kementrian_Pengembangan_Sumber_Daya_Mahasiswa }}</td>
                                <td>{{ $dt->Kementrian_Kemahasiswaan }}</td>
                                <td>{{ $dt->Kementrian_Agama }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){

        // btn refresh
        $('.btn-refresh').click(function(e){
            e.preventDefault();
            $('.preloader').fadeIn();
            location.reload();
        })

    })
</script>

@endsection
