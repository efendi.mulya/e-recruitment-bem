@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <h4>{{ $title }}</h4>
        <div class="box box-warning">
            <div class="box-header">
                <p>
                    <a href="{{ url('kementrian') }}" class="btn btn-sm btn-flat btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>

                    <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                </p>
            </div>
            <div class="box-body">

                @if ($count >= 7 )
                <h3 class="text-center"><strong>Jumlah kementrian sudah mencapai batas maksimal.</strong></h3>
                <br>
                @else
                <form role="form" method="POST" action="{{ url('kementrian/add') }}">
                    @csrf
                    <div class="box-body">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama Kementrian</label>
                            <input type="text" class="form-control " name="nama_kementrian" placeholder="Masukkan Nama Kementrian" />
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Tupoksi</label>
                            <textarea class="form-control" name="tupoksi" rows="5" placeholder="Masukkan Tupoksi Kementrian"></textarea>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            {{-- <th>Pilih Produk</th> --}}
                                            {{-- <th>#</th> --}}
                                            <th>Soal</th>
                                            <th>Jawab</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($soal as $e=>$pd)
                                        <tr>
                                            {{-- <td>{{ $e+1 }}</td> --}}
                                            <td>{{ $pd->soal_spk }}</td>
                                            <td>
                                                <input type="hidden" name="soal[]" value="{{ $pd->id }}">
                                                <select class="form-control" name="vektor[]">
                                                    <option id="vektor{{ $ln->soalspk }}" value="1">Iya</option>
                                                    <option value="0">Tidak</option>
                                                </select>
                                                {{-- <input type="number" value="0" class="form-control" name="qty[]"> --}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        {{-- <div class="form-group">
                            <label for="exampleInputEmail1">Sebentar yah</label>
                            <select class="form-control select2" name="jenispg">
                                <option value="1">Iya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </div> --}}

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                    </div>
                </form>
                @endif

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    function randomNumber(min, max) {
        return Math.random() * (max - min) + min;
    }
    document.getElementById("vektor1").value = randomNumber(0.9, 1);
    document.getElementById("vektor2").value = randomNumber(0.9, 1);
    document.getElementById("vektor4").value = randomNumber(0.9, 1);
    document.getElementById("vektor5").value = randomNumber(0.9, 1);
    document.getElementById("vektor6").value = randomNumber(0.9, 1);
    document.getElementById("vektor7").value = randomNumber(0.9, 1);
    document.getElementById("vektor8").value = randomNumber(0.9, 1);
    document.getElementById("vektor9").value = randomNumber(0.9, 1);
    document.getElementById("vektor10").value = randomNumber(0.9, 1);
    document.getElementById("vektor11").value = randomNumber(0.9, 1);
    document.getElementById("vektor12").value = randomNumber(0.9, 1);
    document.getElementById("vektor13").value = randomNumber(0.9, 1);
    document.getElementById("vektor14").value = randomNumber(0.9, 1);
    document.getElementById("vektor15").value = randomNumber(0.9, 1);
    document.getElementById("vektor16").value = randomNumber(0.9, 1);

</script>

<script type="text/javascript">
    $(document).ready(function(){

        // btn refresh
        $('.btn-refresh').click(function(e){
            e.preventDefault();
            $('.preloader').fadeIn();
            location.reload();
        })

    })
</script>

@endsection
