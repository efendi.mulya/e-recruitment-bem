@extends('layouts-matrix.master')

@section('breadcrumb')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-10 d-flex no-block align-items-center">
            <h4 class="page-title alert">{{ $breadcrumb }}</h4>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="container-fluid">
    {{-- <div class="col-md-12 alert alert-success">Silahkan </div> --}}

    <div class="card">
        <div class="card-body">

            <form role="form" method="POST" action="{{ url('/profil-peserta') }}">
                @csrf
                {{ method_field('PUT') }}
                @foreach ($data as $e=>$dt)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Nama</label>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $dt->name }}" required autocomplete="name" placeholder="Masukkan Nama" />
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Password Baru</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Masukkan Password" />
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Email</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $dt->email }}" required autocomplete="email" placeholder="Masukkan E-Mail" />
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label class="text-right control-label col-form-label">Konfirmasi Password</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Konfirmasi Password" />
                        </div>
                    </div>
                </div>
                @endforeach

                <div class="border-top">
                    <div class="card-body">
                        <button type="submit" class="btn btn-block btn-success">SIMPAN</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

</div>

@endsection

