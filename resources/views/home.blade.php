@if (auth()->user()->level=="Peserta")
    @extends('layouts-matrix.master')

    @section('breadcrumb')
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title alert">{{ $breadcrumb }}</h4>
                </div>
            </div>
        </div>
    @endsection

    @section('content')

    <?php
        $usr = \Auth::user()->id;
        $uj = \DB::select("SELECT * FROM m_datauji WHERE user_id = '$usr'");
        $hsl = \DB::select("SELECT * FROM m_datahasil WHERE user_id = '$usr'");
    ?>

        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-md-flex align-items-center">
                                <div>
                                    <h4 class="card-title">Hai {{ \Auth::user()->name }}</h4>
                                    <h5 class="card-subtitle">Selamat Datang di E-Recruitment BEM PSDKU POLINEMA</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- SPK -->
                <div class="col-md-6 col-lg-6 col-xlg-3">
                    <div class="card card-hover">
                        <div class="box bg-success text-center">
                            @if (count($uj) == "0" && count($hsl) == "0")
                            <a href="{{ url('/spk') }}">
                                <h1 class="font-light text-white"><i class="mdi mdi-chart-bubble"></i></h1>
                            </a>
                            @elseif (count($uj) != "0" && count($hsl) == "0")
                            <a href="{{ url('/spk/finish') }}">
                                <h1 class="font-light text-white"><i class="mdi mdi-chart-bubble"></i></h1>
                            </a>
                            @elseif (count($uj) != "0" && count($hsl) != "0")
                            <a href="{{ url('/spk/hasil') }}">
                                <h1 class="font-light text-white"><i class="mdi mdi-chart-bubble"></i></h1>
                            </a>
                            @endif
                            <h6 class="text-white">SPK</h6>
                        </div>
                    </div>
                </div>
                <!-- Formulir -->
                <div class="col-md-6 col-lg-6 col-xlg-3">
                    <div class="card card-hover">
                        <div class="box bg-danger text-center">
                            <a href="{{ url('/formulir') }}">
                                <h1 class="font-light text-white"><i class="mdi mdi-receipt"></i></h1>
                            </a>
                            <h6 class="text-white">Formulir</h6>
                        </div>
                    </div>
                </div>
                <!-- Ujian -->
                <div class="col-md-6 col-lg-6 col-xlg-3">
                    <div class="card card-hover">
                        <div class="box bg-cyan text-center">
                            <a href="{{ url('/ujian') }}">
                                <h1 class="font-light text-white"><i class="mdi mdi-pencil"></i></h1>
                            </a>
                            <h6 class="text-white">Ujian</h6>
                        </div>
                    </div>
                </div>
                <!-- Nilai -->
                <div class="col-md-6 col-lg-6 col-xlg-3">
                    <div class="card card-hover">
                        <div class="box bg-warning text-center">
                            <a href="{{ url('/nilai') }}">
                                <h1 class="font-light text-white"><i class="mdi mdi-chart-bar"></i></h1>
                            </a>
                            <h6 class="text-white">Nilai</h6>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    @endsection
@endif
@if (auth()->user()->level=="Admin" || auth()->user()->level=="Asesor")
    @extends('layouts.master')
    @section('content')

    <?php
        $menu = \DB::select("SELECT * FROM m_pengaturan WHERE menu = 'ujian'");
        $form = \DB::select("SELECT * FROM m_pengaturan WHERE menu = 'formulir'");
        $lolos = \DB::select("SELECT * FROM m_nilai WHERE nilaipg >= 75 AND nilaiurn >= 75");
        $gagal = \DB::select("SELECT * FROM m_nilai WHERE nilaipg < 75 AND nilaiurn < 75");
    ?>

    <section class="content-header">
        <h1>Dashboard</h1>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <h3 class="card-title">Hai <strong>{{ \Auth::user()->name }}</strong></h3>
                        <h4 class="card-subtitle">Selamat Datang di Menu <strong>{{ \Auth::user()->level }}</strong> E-Recruitment BEM PSDKU POLINEMA</h4>
                        <br>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-pencil"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Menu Ujian</span>
                        @if ($menu[0]->status == 1)
                        <span class="info-box-number">ON</span>
                        @else
                        <span class="info-box-number">OFF</span>
                        @endif
                        @if (auth()->user()->level=="Admin")
                        <form class="pull-right" action="{{ url('pengaturan/ujian') }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            @if ($menu[0]->status == 1)
                            <button type="submit" class="btn btn-sm btn-success">
                                <i class="fa fa-toggle-on"></i>
                            </button>
                            @else
                            <button type="submit" class="btn btn-sm btn-danger">
                                <i class="fa fa-toggle-off"></i>
                            </button>
                            @endif
                        </form>
                        @else
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-maroon"><i class="fa fa-users"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Total Peserta</span>
                        <span class="info-box-number">{{ count($peserta) }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-teal"><i class="fa fa-file-text"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Total Formulir</span>
                        <span class="info-box-number">{{ count($formulir) }}</span>
                        @if (auth()->user()->level=="Admin")
                        <form class="pull-right" action="{{ url('pengaturan/formulir') }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            @if ($form[0]->status == 1)
                            <button type="submit" class="btn btn-sm btn-success">
                                <i class="fa fa-toggle-on"></i>
                            </button>
                            @else
                            <button type="submit" class="btn btn-sm btn-danger">
                                <i class="fa fa-toggle-off"></i>
                            </button>
                            @endif
                        </form>
                        @else
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-purple"><i class="fa fa-bar-chart"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Total Peserta Selesai Ujian</span>
                        <span class="info-box-number">{{ count($nilai) }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Total Peserta Lolos</span>
                        <span class="info-box-number">{{ count($lolos) }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-times"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Total Peserta Tidak Lolos</span>
                        <span class="info-box-number">{{ count($gagal) }}</span>
                    </div>
                </div>
            </div>



        </div>
    </section>

    @endsection
@endif

