@extends('layouts-matrix.master')

<?php
    $usr = \Auth::user()->id;
    $pg = \DB::select("SELECT * FROM m_jawabanpg WHERE user_id = '$usr'");
    $uraian = \DB::select("SELECT * FROM m_jawabanurn WHERE user_id = '$usr'");
?>

@section('breadcrumb')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-8 d-flex no-block align-items-center">
            <h4 class="page-title alert">{{ $breadcrumb }}</h4>
        </div>
        @if ($pengaturan[0]->status == 1)
            @if (count($formulir) == 0)
            @else
                @if ($formulir[0]->status == 1)
                    <div class="col-4 no-block align-items-center">
                        @if (count($pg) == "0" && count($uraian) == "0")
                        <h4 class="page-title alert alert-danger  text-center" id="timer">30 : 00</h4>
                        @elseif (count($pg) != "0" && count($uraian) == "0")
                        <h4 class="page-title alert alert-danger  text-center" id="timer">30 : 00</h4>
                        @else
                        @endif
                    </div>
                @else
                @endif
            @endif
        @else
        @endif
    </div>
</div>
@endsection

@section('content')

<div class="container-fluid">
    @if ($pengaturan[0]->status == 1)
        @if (count($formulir) == 0)
        <div class="col-md-12 alert alert-danger">PERINGATAN</div>
        @else
            @if ($formulir[0]->status == 1)
                @if (count($pg) == "0" && count($uraian) == "0")
                <div class="col-md-12 alert alert-primary">{{ $title }}</div>
                @elseif (count($pg) != "0" && count($uraian) == "0")
                <div class="col-md-12 alert alert-primary">{{ $title }}</div>
                @else
                <div class="col-md-12 alert alert-danger">PERINGATAN</div>
                @endif
            @else
                <div class="col-md-12 alert alert-danger">PERINGATAN</div>
            @endif
        @endif
    @else
        <div class="col-md-12 alert alert-warning">PEMBERITAHUAN</div>
    @endif

    <div class="card">
        <div class="card-body">
        @if ($pengaturan[0]->status == 1)
            @if (count($formulir) == 0)
                <h3 class="text-center"><strong>Anda belum mengirimkan formulir.</strong></h3>
            @else
                @if ($formulir[0]->status == 1)

                    @if (count($pg) == "0" && count($uraian) != "0")
                    <h3 class="text-center"><strong>Anda sudah mengerjakan soal uraian.</strong></h3>
                    <br>
                    <a href="{{ url('ujian/pg') }}" class="btn btn-block btn-success">Menuju Soal Pilihan Ganda</a>
                    @elseif (count($pg) != "0" && count($uraian) != "0")
                    <h3 class="text-center"><strong>Anda sudah mengerjakan semua soal.</strong></h3>
                    <br>
                    <a href="{{ url('nilai/') }}" class="btn btn-block btn-success">Menuju Hasil Ujian</a>
                    @else
                    <form role="form" method="POST" name="form" action="{{ url('ujian/uraian') }}">
                        @csrf
                        @foreach ($data as $e=>$dt)
                        <ul class="list-group mt-4 mb-4">
                            <li class="list-group-item">
                                <strong>{{ $e+1 }}. {{ $dt->soalurn }}</strong><br>
                                <input type="hidden" name="user_id[{{ $dt->id }}]" value="{{ \Auth::user()->id }}">
                                <input type="hidden" name="soal_id[{{ $dt->id }}]" value="{{ $dt->id }}" size="2">
                                <br>
                                <textarea name="jawab[{{ $dt->id }}]" class="form-control" style="height: 150px;"></textarea>
                            </li>
                        </ul>
                        @endforeach

                        <br>
                        <button id="kirim" type="submit" class="btn btn-block btn-success">Kirim</button>
                    </form>
                    @endif

                @else
                    <h3 class="text-center"><strong>Formulir Anda belum diterima.</strong></h3>
                @endif
            @endif
        @else
            <h3 class="text-center"><strong>MAAF, MENU UJIAN SEDANG DITUTUP.</strong></h3>
        @endif

        </div>
    </div>

</div>

<!--Timer Uraian js-->
<script language="javascript" type="text/javascript" src="{{ asset('timer/function-urn.js') }}"></script>

@endsection

