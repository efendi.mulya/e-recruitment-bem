@extends('layouts-matrix.master')

<?php
    $usr = \Auth::user()->id;
    $pg = \DB::select("SELECT * FROM m_jawabanpg WHERE user_id = '$usr'");
    $uraian = \DB::select("SELECT * FROM m_jawabanurn WHERE user_id = '$usr'");
?>

@section('breadcrumb')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-8 d-flex no-block align-items-center">
            <h4 class="page-title alert">{{ $breadcrumb }}</h4>
        </div>
        @if ($pengaturan[0]->status == 1)
            @if (count($formulir) == 0)
            @else
                @if ($formulir[0]->status == 1)
                    <div class="col-4 no-block align-items-center">
                        @if (count($pg) == "0" && count($uraian) == "0")
                        <h4 class="page-title alert alert-danger text-center" id="timer">60 : 00</h4>
                        @elseif (count($pg) == "0" && count($uraian) != "0")
                        <h4 class="page-title alert alert-danger text-center" id="timer">60 : 00</h4>
                        @else
                        @endif
                    </div>
                @else
                @endif
            @endif
        @else
        @endif
    </div>
</div>
@endsection

@section('content')

<div class="container-fluid">
    @if ($pengaturan[0]->status == 1)
        @if (count($formulir) == 0)
        <div class="col-md-12 alert alert-danger">PERINGATAN</div>
        @else
            @if ($formulir[0]->status == 1)
                @if (count($pg) == "0" && count($uraian) == "0")
                <div class="col-md-12 alert alert-primary">{{ $title }}</div>
                @elseif (count($pg) == "0" && count($uraian) != "0")
                <div class="col-md-12 alert alert-primary">{{ $title }}</div>
                @else
                <div class="col-md-12 alert alert-danger">PERINGATAN</div>
                @endif
            @else
            <div class="col-md-12 alert alert-danger">PERINGATAN</div>
            @endif
        @endif
    @else
        <div class="col-md-12 alert alert-warning">PEMBERITAHUAN</div>
    @endif

    <div class="card">
        <div class="card-body">
        @if ($pengaturan[0]->status == 1)
            @if (count($formulir) == 0)
                <h3 class="text-center"><strong>Anda belum mengirimkan formulir.</strong></h3>
            @else
                @if ($formulir[0]->status == 1)

                    @if (count($pg) != "0" && count($uraian) == "0")
                    <h3 class="text-center"><strong>Anda sudah mengerjakan soal pilihan ganda.</strong></h3>
                    <br>
                    <a href="{{ url('ujian/uraian') }}" class="btn btn-block btn-success">Menuju Soal Uraian</a>
                    @elseif (count($pg) != "0" && count($uraian) != "0")
                    <h3 class="text-center"><strong>Anda sudah mengerjakan semua soal.</strong></h3>
                    <br>
                    <a href="{{ url('nilai/') }}" class="btn btn-block btn-success">Menuju Hasil Ujian</a>
                    @else
                    <div class="content">
                        <div class="content__inner">
                        <div class="container overflow-hidden">
                            <div class="multisteps-form">
                                <br>
                                <div class="multisteps-form__progress">
                                    <button class="multisteps-form__progress-btn js-active" type="button" title="Keorganisasian">Keorganisasian</button>
                                    <button class="multisteps-form__progress-btn" type="button" title="Matematika">Matematika</button>
                                    <button class="multisteps-form__progress-btn" type="button" title="Psikotes">Psikotes</button>
                                    <button class="multisteps-form__progress-btn" type="button" title="Kewarganegaraan">Kewarganegaraan</button>
                                    <button class="multisteps-form__progress-btn" type="button" title="Wawasan Kampus">Wawasan Kampus</button>
                                </div>
                                <br>
                                <form class="multisteps-form__form" method="POST" action="{{ url('ujian/pg') }}">
                                    @csrf

                                    <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="slideHorz">
                                    <h3 class="multisteps-form__title text-center">Keorganisasian</h3>
                                    <div class="multisteps-form__content">
                                        @foreach ($data1 as $e=>$dt)

                                        <ul class="list-group mt-4 mb-4">
                                            <li class="list-group-item">
                                                <strong>{{ $e+1 }}. {{ $dt->soalpg }}</strong>
                                                <input type="hidden" name="user_id[{{ $dt->id }}]" value="{{ \Auth::user()->id }}">
                                                <input type="hidden" name="soal_id[{{ $dt->id }}]" value="{{ $dt->id }}">
                                                <input type="hidden" name="kunci[{{ $dt->id }}]" value="{{ $dt->jawabanpg }}">
                                                <br>
                                                <ul class='list-group mt-4 mb-4'>

                                                    <li class="list-group-item" style="display:none">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="null" checked="checked"> null</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->a }}"> {{ $dt->a }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->b }}"> {{ $dt->b }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->c }}"> {{ $dt->c }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->d }}"> {{ $dt->d }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->e }}"> {{ $dt->e }}</label>
                                                    </li>

                                                </ul>
                                            </li>
                                        </ul>
                                        @endforeach
                                        <div class="button-row d-flex mt-4">
                                        <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Lanjut</button>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="slideHorz">
                                    <h3 class="multisteps-form__title text-center">Matematika</h3>
                                    <div class="multisteps-form__content">
                                        @foreach ($data2 as $e=>$dt)

                                        <ul class="list-group mt-4 mb-4">
                                            <li class="list-group-item">
                                                <strong>{{ $e+1 }}. {{ $dt->soalpg }}</strong>
                                                <input type="hidden" name="user_id[{{ $dt->id }}]" value="{{ \Auth::user()->id }}">
                                                <input type="hidden" name="soal_id[{{ $dt->id }}]" value="{{ $dt->id }}">
                                                <input type="hidden" name="kunci[{{ $dt->id }}]" value="{{ $dt->jawabanpg }}">
                                                <br>
                                                <ul class='list-group mt-4 mb-4'>

                                                    <li class="list-group-item" style="display:none">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="null" checked="checked"> null</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->a }}"> {{ $dt->a }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->b }}"> {{ $dt->b }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->c }}"> {{ $dt->c }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->d }}"> {{ $dt->d }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->e }}"> {{ $dt->e }}</label>
                                                    </li>

                                                </ul>
                                            </li>
                                        </ul>
                                        @endforeach
                                        <div class="button-row d-flex mt-4">
                                        <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Kembali</button>
                                        <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Lanjut</button>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="slideHorz">
                                        <h3 class="multisteps-form__title text-center">Psikotes</h3>
                                        <div class="multisteps-form__content">
                                            @foreach ($data3 as $e=>$dt)

                                            <ul class="list-group mt-4 mb-4">
                                                <li class="list-group-item">
                                                    <strong>{{ $e+1 }}. {{ $dt->soalpg }}</strong>
                                                    <input type="hidden" name="user_id[{{ $dt->id }}]" value="{{ \Auth::user()->id }}">
                                                    <input type="hidden" name="soal_id[{{ $dt->id }}]" value="{{ $dt->id }}">
                                                    <input type="hidden" name="kunci[{{ $dt->id }}]" value="{{ $dt->jawabanpg }}">
                                                    <br>
                                                    <ul class='list-group mt-4 mb-4'>

                                                        <li class="list-group-item" style="display:none">
                                                            <label><input type="radio" name="jawab[{{ $dt->id }}]" value="null" checked="checked"> null</label>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->a }}"> {{ $dt->a }}</label>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->b }}"> {{ $dt->b }}</label>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->c }}"> {{ $dt->c }}</label>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->d }}"> {{ $dt->d }}</label>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->e }}"> {{ $dt->e }}</label>
                                                        </li>

                                                    </ul>
                                                </li>
                                            </ul>
                                            @endforeach
                                        <div class="button-row d-flex mt-4">
                                            <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Kembali</button>
                                            <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Lanjut</button>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="slideHorz">
                                    <h3 class="multisteps-form__title text-center">Kewarganegaraan</h3>
                                    <div class="multisteps-form__content">
                                        @foreach ($data4 as $e=>$dt)

                                        <ul class="list-group mt-4 mb-4">
                                            <li class="list-group-item">
                                                <strong>{{ $e+1 }}. {{ $dt->soalpg }}</strong>
                                                <input type="hidden" name="user_id[{{ $dt->id }}]" value="{{ \Auth::user()->id }}">
                                                <input type="hidden" name="soal_id[{{ $dt->id }}]" value="{{ $dt->id }}">
                                                <input type="hidden" name="kunci[{{ $dt->id }}]" value="{{ $dt->jawabanpg }}">
                                                <br>
                                                <ul class='list-group mt-4 mb-4'>

                                                    <li class="list-group-item" style="display:none">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="null" checked="checked"> null</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->a }}"> {{ $dt->a }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->b }}"> {{ $dt->b }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->c }}"> {{ $dt->c }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->d }}"> {{ $dt->d }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->e }}"> {{ $dt->e }}</label>
                                                    </li>

                                                </ul>
                                            </li>
                                        </ul>
                                        @endforeach
                                        <div class="button-row d-flex mt-4 col-12">
                                            <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Kembali</button>
                                            <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Lanjut</button>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="slideHorz">
                                    <h3 class="multisteps-form__title text-center">Wawasan Kampus</h3>
                                    <div class="multisteps-form__content">
                                        @foreach ($data5 as $e=>$dt)

                                        <ul class="list-group mt-4 mb-4">
                                            <li class="list-group-item">
                                                <strong>{{ $e+1 }}. {{ $dt->soalpg }}</strong>
                                                <input type="hidden" name="user_id[{{ $dt->id }}]" value="{{ \Auth::user()->id }}">
                                                <input type="hidden" name="soal_id[{{ $dt->id }}]" value="{{ $dt->id }}">
                                                <input type="hidden" name="kunci[{{ $dt->id }}]" value="{{ $dt->jawabanpg }}">
                                                <br>
                                                <ul class='list-group mt-4 mb-4'>

                                                    <li class="list-group-item" style="display:none">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="null" checked="checked"> null</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->a }}"> {{ $dt->a }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->b }}"> {{ $dt->b }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->c }}"> {{ $dt->c }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->d }}"> {{ $dt->d }}</label>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->e }}"> {{ $dt->e }}</label>
                                                    </li>

                                                </ul>
                                            </li>
                                        </ul>
                                        @endforeach
                                        <div class="button-row d-flex mt-4">
                                        <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Kembali</button>
                                        <button class="btn btn-success ml-auto" id="kirim" type="submit" title="Send">Kirim</button>
                                        </div>
                                    </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                        </div>
                    </div>
                    @endif

                @else
                    <h3 class="text-center"><strong>Formulir Anda belum diterima.</strong></h3>
                @endif
            @endif
        @else
            <h3 class="text-center"><strong>MAAF, MENU UJIAN SEDANG DITUTUP.</strong></h3>
        @endif

        </div>
    </div>

</div>

{{-- <div class="container-fluid">
    <div class="col-md-12 alert alert-primary">{{ $title }}</div>

    <div class="card">
        <div class="card-body">
            <form role="form" method="POST" action="{{ url('ujianpg') }}">
            @csrf

            <div id="">
            @foreach ($data1 as $e=>$dt)

            <ul class="list-group mt-4 mb-4">
                <li class="list-group-item">
                    <strong>{{ $e+1 }}. {{ $dt->soalpg }}</strong>
                    <input type="hidden" name="user_id[{{ $dt->id }}]" value="{{ \Auth::user()->id }}">
                    <input type="hidden" name="soal_id[{{ $dt->id }}]" value="{{ $dt->id }}">
                    <input type="hidden" name="kunci[{{ $dt->id }}]" value="{{ $dt->jawabanpg }}">
                    <br>
                    <ul class='list-group mt-4 mb-4'>

                        <li class="list-group-item" style="display:none">
                            <label><input type="radio" name="jawab[{{ $dt->id }}]" value="null" checked="checked"> null</label>
                        </li>
                        <li class="list-group-item">
                            <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->a }}"> {{ $dt->a }}</label>
                        </li>
                        <li class="list-group-item">
                            <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->b }}"> {{ $dt->b }}</label>
                        </li>
                        <li class="list-group-item">
                            <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->c }}"> {{ $dt->c }}</label>
                        </li>
                        <li class="list-group-item">
                            <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->d }}"> {{ $dt->d }}</label>
                        </li>
                        <li class="list-group-item">
                            <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->e }}"> {{ $dt->e }}</label>
                        </li>

                    </ul>
                </li>
            </ul>
            @endforeach
            </div>

            <div id="">
                @foreach ($data2 as $e=>$dt)

                <ul class="list-group mt-4 mb-4">
                    <li class="list-group-item">
                        <strong>{{ $e+1 }}. {{ $dt->soalpg }}</strong>
                        <input type="hidden" name="user_id[{{ $dt->id }}]" value="{{ \Auth::user()->id }}">
                        <input type="hidden" name="soal_id[{{ $dt->id }}]" value="{{ $dt->id }}">
                        <input type="hidden" name="kunci[{{ $dt->id }}]" value="{{ $dt->jawabanpg }}">
                        <br>
                        <ul class='list-group mt-4 mb-4'>

                            <li class="list-group-item" style="display:none">
                                <label><input type="radio" name="jawab[{{ $dt->id }}]" value="null" checked="checked"> null</label>
                            </li>
                            <li class="list-group-item">
                                <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->a }}"> {{ $dt->a }}</label>
                            </li>
                            <li class="list-group-item">
                                <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->b }}"> {{ $dt->b }}</label>
                            </li>
                            <li class="list-group-item">
                                <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->c }}"> {{ $dt->c }}</label>
                            </li>
                            <li class="list-group-item">
                                <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->d }}"> {{ $dt->d }}</label>
                            </li>
                            <li class="list-group-item">
                                <label><input type="radio" name="jawab[{{ $dt->id }}]" value="{{ $dt->e }}"> {{ $dt->e }}</label>
                            </li>

                        </ul>
                    </li>
                </ul>
                @endforeach
                </div>

            <br>
            <button  type="submit" class="btn btn-block btn-primary">Submit</button>
            </form>
        </div>
    </div>

</div> --}}

<!-- Step Wizard js -->
<script src="{{ asset('stepwizard/function.js') }}"></script>
<!--Timer Pilihan Ganda js-->
<script language="javascript" type="text/javascript" src="{{ asset('timer/function-pg.js') }}"></script>

@endsection

