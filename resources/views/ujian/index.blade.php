@extends('layouts-matrix.master')

<?php
    $usr = \Auth::user()->id;
    $pg = \DB::select("SELECT * FROM m_jawabanpg WHERE user_id = '$usr'");
    $uraian = \DB::select("SELECT * FROM m_jawabanurn WHERE user_id = '$usr'");
?>

@section('breadcrumb')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-10 d-flex no-block align-items-center">
            <h4 class="page-title alert">{{ $breadcrumb }}</h4>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="container-fluid">
    @if ($pengaturan[0]->status == 1)
        @if (count($formulir) == 0)
        <div class="col-md-12 alert alert-danger">PERINGATAN</div>
        @else
            @if ($formulir[0]->status == 1)
                @if (count($pg) == "0" && count($uraian) == "0")
                <div class="col-md-12 alert alert-primary">{{ $title }}</div>
                @else
                <div class="col-md-12 alert alert-danger">PERINGATAN</div>
                @endif
            @else
                <div class="col-md-12 alert alert-danger">PERINGATAN</div>
            @endif
        @endif
    @else
        <div class="col-md-12 alert alert-warning">PEMBERITAHUAN</div>
    @endif

    <div class="card">
        <div class="card-body">
        @if ($pengaturan[0]->status == 1)
            @if (count($formulir) == 0)
                <h3 class="text-center"><strong>Anda belum mengirimkan formulir.</strong></h3>
            @else
                @if ($formulir[0]->status == 1)
                    @if (count($pg) == "0" && count($uraian) != "0")
                    <h3 class="text-center"><strong>Anda sudah mengerjakan soal uraian.</strong></h3>
                    <br>
                    <a href="{{ url('ujian/pg') }}" class="btn btn-block btn-success">Menuju Soal Pilihan Ganda</a>
                    @elseif (count($pg) != "0" && count($uraian) != "0")
                    <h3 class="text-center"><strong>Anda sudah mengerjakan semua soal.</strong></h3>
                    <br>
                    <a href="{{ url('nilai/') }}" class="btn btn-block btn-success">Menuju Hasil Ujian</a>
                    @else
                    <h3 class="text-center" id="greeting">BACA TERLEBIH DAHULU</h3>
                    <p>Waktu Pengerjaan : 90 menit</p>
                    <p>Jumlah Soal      : 30 soal</p>
                    <br>
                    <div class="container">
                        <p>1. Bacalah dengan teliti tiap-tiap soal sebelum menjawab.</p>
                        <p>2. Pengerjaan soal-soal ujian dikerjakan sesuai batasan waktu yang diberikan. Apabila waktu telah habis, anda tidak dapat lagi mengisi / mengoreksi kembali jawaban dari soal-soal yang tersedia. Begitu pula sebaliknya.</p>
                        <p>3. Skor atau nilai hanya akan ditampilkan saja tanpa adanya sertifikasi nilai untuk diunduh.</p>
                    </div>
                    <br>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="customControlAutosizing1">
                        <label class="custom-control-label" for="customControlAutosizing1">Saya mengerti dan siap untuk mengikuti ujian.</label>
                    </div>
                    <br>
                    <button id="button" onclick="location.href='{{ url('ujian/pg') }}';" type="button" class="btn btn-block btn-primary" disabled>Mulai</button>
                    @endif
                @else
                    <h3 class="text-center"><strong>Formulir Anda belum diterima.</strong></h3>
                @endif
            @endif
        @else
            <h3 class="text-center"><strong>MAAF, MENU UJIAN SEDANG DITUTUP.</strong></h3>
        @endif

        </div>
    </div>

</div>

<script type="text/javascript">
    const checkbox = document.querySelector('#customControlAutosizing1');
    const button = document.querySelector('#button');

    checkbox.addEventListener('change', function() {
        if(this.checked) {
            button.disabled = false;
        } else {
            button.disabled = true;
        }
    });
</script>

@endsection

