@extends('layouts-matrix.master')

<?php
    $usr = \Auth::user()->id;
    $nilai = \DB::select("SELECT * FROM m_nilai WHERE user_id = '$usr'");
?>

@section('breadcrumb')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title alert">{{ $breadcrumb }}</h4>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="container-fluid">
    @if (count($nilai) == "0")
    <div class="col-md-12 alert alert-danger">PERINGATAN</div>
    @else
    <div class="col-md-12 alert alert-primary">{{ $title }}</div>
    @endif

    <div class="card">
        <div class="card-body">

            @if (count($nilai) == "0")
            <h3 class="text-center"><strong>Anda belum mengerjakan semua soal ujian.</strong></h3>
            <br>
            <a href="{{ url('ujian/') }}" class="btn btn-block btn-success">Kerjakan Soal</a>
            @else
            <table class="table text-center">
                <thead>
                    <tr>
                        <th scope="col">Nilai Pilihan Ganda</th>
                        <th scope="col">Nilai Uraian</th>
                        <th scope="col">Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        @foreach ($data as $e=>$dt)
                        <td>{{ $dt->nilaipg }}</td>
                        <td>
                            @if ($dt->nilaiurn == null)
                                <label class="label label-warning">Belum Dinilai</label>
                            @else
                                {{ $dt->nilaiurn }}
                            @endif
                        </td>
                        <td>
                            @if ($dt->nilaipg >= 50 && $dt->nilaiurn >= 75)
                                <label class="label label-success">Anda Lolos</label>
                            @elseif ($dt->nilaiurn == null)
                                <label class="label label-warning">Menunggu Penilaian</label>
                            @else
                                <label class="label label-danger">Anda Tidak Lolos</label>
                            @endif
                        </td>
                        @endforeach
                    </tr>
                </tbody>
            </table>
            @endif

        </div>
    </div>

</div>

<!--Timer Uraian js-->
<script language="javascript" type="text/javascript" src="{{ asset('timer/function-urn.js') }}"></script>

@endsection

