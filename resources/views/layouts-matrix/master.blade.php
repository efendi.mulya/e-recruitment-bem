@if (auth()->user()->level=="Peserta")
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('matrix/assets/images/favicon.png') }}">
    <title>{{ $breadcrumb }} {{ $title }}</title>
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('matrix/assets/libs/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('matrix/assets/libs/jquery-minicolors/jquery.minicolors.css') }}">
    <link href="{{ asset('matrix/assets/libs/flot/css/float-chart.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('matrix/dist/css/style.min.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('matrix/assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <!-- Step Wizard CSS -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600&display=swap" rel="stylesheet">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css'>
    <link rel="stylesheet" href="{{ asset('stepwizard/style.css') }}">
    <link rel="stylesheet" href="{{ asset('upload-ava/upload-ava.css') }}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <?php
        $usr = \Auth::user()->id;
        $formulir = \DB::select("SELECT * FROM m_formulir WHERE user_id = $usr");
        $ujian = \DB::select("SELECT * FROM m_pengaturan WHERE menu = 'ujian'");
        $form = \DB::select("SELECT * FROM m_pengaturan WHERE menu = 'formulir'");
        $pg = \DB::select("SELECT * FROM m_jawabanpg WHERE user_id = $usr");
        $urn = \DB::select("SELECT * FROM m_jawabanurn WHERE user_id = $usr");
    ?>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin5">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand" href="{{ url('/home') }}">
                        <!-- Logo icon -->
                        <b class="logo-icon p-l-10">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="{{ asset('matrix/assets/images/logo-icon.png') }}" alt="homepage" class="light-logo" />

                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="logo-text">
                            <!-- dark Logo text -->
                            <img src="{{ asset('matrix/assets/images/logo-text.png') }}" alt="homepage" class="light-logo" />

                        </span>
                        <!-- Logo icon -->
                        <!-- <b class="logo-icon"> -->
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <!-- <img src="../../assets/images/logo-text.png" alt="homepage" class="light-logo" /> -->

                        <!-- </b> -->
                        <!--End Logo icon -->
                    </a>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left mr-auto">
                        <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                        <!-- ============================================================== -->
                        <!-- create new -->
                        <!-- ============================================================== -->
                        {{-- <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                             <span class="d-none d-md-block">Create New <i class="fa fa-angle-down"></i></span>
                             <span class="d-block d-md-none"><i class="fa fa-plus"></i></span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </li> --}}
                        <!-- ============================================================== -->
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        {{-- <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="mdi mdi-bell font-24"></i>
                                <span></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </li> --}}
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="mdi mdi-bell font-24"></i>
                                @if (count($formulir) == 0 && $form[0]->status == 1)
                                <span class="badge badge-warning">1</span>
                                @elseif (count($formulir) == 0 && $form[0]->status == 0)
                                @else
                                    @if ($formulir[0]->status == 1 && $ujian[0]->status == 1 && count($pg) == 0)
                                    <span class="badge badge-warning">1</span>
                                    @elseif ($formulir[0]->status == 1 && $ujian[0]->status == 1 && count($urn) == 0)
                                    <span class="badge badge-warning">1</span>
                                    @else
                                    @endif
                                @endif
                            </a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown" aria-labelledby="2">
                                <ul class="list-style-none">
                                    <li>
                                        <div class="">
                                            <!-- Message -->
                                            @if (count($formulir) == 0 && $form[0]->status == 1)
                                            <a href="{{ url('/formulir') }}" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-danger btn-circle"><i class="mdi mdi-receipt"></i></span>
                                                    <div class="m-l-10">
                                                        <span class="mail-desc">Segera buat formulir pendaftaran!</span>
                                                    </div>
                                                </div>
                                            </a>
                                            @elseif (count($formulir) == 0 && $form[0]->status == 0)
                                            <br>
                                            <div class=" no-block align-items-center p-10">
                                                <div class="m-l-10">
                                                    <p class="text-center">(Tidak ada notifikasi)</p>
                                                </div>
                                            </div>
                                            @elseif ($formulir[0]->status == 1 && $ujian[0]->status == 1 && count($pg) == 0)
                                            <a href="{{ url('/ujian') }}" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-cyan btn-circle"><i class="mdi mdi-pencil"></i></span>
                                                    <div class="m-l-10">
                                                        <span class="mail-desc">Segera kerjakan soal ujian seleksi!</span>
                                                    </div>
                                                </div>
                                            </a>
                                            @elseif ($formulir[0]->status == 1 && $ujian[0]->status == 1 && count($urn) == 0)
                                            <a href="{{ url('/ujian') }}" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-cyan btn-circle"><i class="mdi mdi-pencil"></i></span>
                                                    <div class="m-l-10">
                                                        <span class="mail-desc">Segera kerjakan soal ujian seleksi!</span>
                                                    </div>
                                                </div>
                                            </a>
                                            @else
                                            <br>
                                            <div class=" no-block align-items-center p-10">
                                                <div class="m-l-10">
                                                    <p class="text-center">(Tidak ada notifikasi)</p>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->

                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('matrix/assets/images/users/1.jpg') }}" alt="user" class="rounded-circle" width="31"></a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                <p></p>
                                <a class="dropdown-item" href="{{ url('/profil-peserta') }}"><i class="ti-user m-r-5 m-l-5"></i> {{ \Auth::user()->name }}</a>
                                <a class="dropdown-item" href="{{ url('/keluar') }}"><i class="fa fa-power-off m-r-5 m-l-5"></i> Keluar</a>
                                <div class="dropdown-divider"></div>
                                <div class="p-l-30 p-10"><a href="{{ url('/profil-peserta') }}" class="btn btn-sm btn-success btn-rounded">Edit Profil</a></div>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php
            $uj = \DB::select("SELECT * FROM m_datauji WHERE user_id = '$usr'");
            $hsl = \DB::select("SELECT * FROM m_datahasil WHERE user_id = '$usr'");
        ?>

        <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('/home') }}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
                        <br>
                        @if (count($uj) == "0" && count($hsl) == "0")
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('/spk') }}" aria-expanded="false"><i class="mdi mdi-chart-bubble"></i><span class="hide-menu">SPK</span></a></li>
                        @elseif (count($uj) != "0" && count($hsl) == "0")
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('/spk/finish') }}" aria-expanded="false"><i class="mdi mdi-chart-bubble"></i><span class="hide-menu">SPK</span></a></li>
                        @elseif (count($uj) != "0" && count($hsl) != "0")
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('/spk/hasil') }}" aria-expanded="false"><i class="mdi mdi-chart-bubble"></i><span class="hide-menu">SPK</span></a></li>
                        @endif
                        <br>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('/formulir') }}" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Formulir</span></a></li>
                        <br>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('/ujian') }}" aria-expanded="false"><i class="mdi mdi-pencil"></i><span class="hide-menu">Ujian</span></a></li>
                        <br>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('/nilai') }}" aria-expanded="false"><i class="mdi mdi-chart-bar"></i><span class="hide-menu">Nilai</span></a></li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            @yield('breadcrumb')
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            @yield('content')
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <!-- Bootstrap tether Core JavaScript -->
    <script type="text/javascript" src="{{ asset('matrix/assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('matrix/assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('matrix/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('matrix/assets/extra-libs/sparkline/sparkline.js') }}"></script>
    <!--Wave Effects -->
    <script type="text/javascript" src="{{ asset('matrix/dist/js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script type="text/javascript" src="{{ asset('matrix/dist/js/sidebarmenu.js') }}"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript" src="{{ asset('matrix/dist/js/custom.min.js') }}"></script>
    <!--This page JavaScript -->
    <!-- <script src="../../dist/js/pages/dashboards/dashboard1.js"></script> -->
    <!-- Charts js Files -->
    <script type="text/javascript" src="{{ asset('matrix/assets/libs/flot/excanvas.js') }}"></script>
    <script type="text/javascript" src="{{ asset('matrix/assets/libs/flot/jquery.flot.js') }}"></script>
    <script type="text/javascript" src="{{ asset('matrix/assets/libs/flot/jquery.flot.pie.js') }}"></script>
    <script type="text/javascript" src="{{ asset('matrix/assets/libs/flot/jquery.flot.time.js') }}"></script>
    <script type="text/javascript" src="{{ asset('matrix/assets/libs/flot/jquery.flot.stack.js') }}"></script>
    <script type="text/javascript" src="{{ asset('matrix/assets/libs/flot/jquery.flot.crosshair.js') }}"></script>
    <script type="text/javascript" src="{{ asset('matrix/assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('matrix/dist/js/pages/chart/chart-page-init.js') }}"></script>
    <!-- Form js -->
    <script src="{{ asset('matrix/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('matrix/dist/js/pages/mask/mask.init.js') }}"></script>
    <script src="{{ asset('matrix/assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('matrix/assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('matrix/assets/libs/jquery-asColor/dist/jquery-asColor.min.js') }}"></script>
    <script src="{{ asset('matrix/assets/libs/jquery-asGradient/dist/jquery-asGradient.js') }}"></script>
    <script src="{{ asset('matrix/assets/libs/jquery-asColorPicker/dist/jquery-asColorPicker.min.js') }}"></script>
    <script src="{{ asset('matrix/assets/libs/jquery-minicolors/jquery.minicolors.min.js') }}"></script>
    <script src="{{ asset('matrix/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('matrix/assets/libs/quill/dist/quill.min.js') }}"></script>
    <!-- Upload -->
    <script src="{{ asset('upload-ava/upload-ava.js') }}"></script>

</body>

</html>
@endif
