@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <h4>{{ $title }}</h4>
        <div class="box box-warning">
            <div class="box-header">
                <p>
                    <a href="{{ url('uraian') }}" class="btn btn-sm btn-flat btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>

                    <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                </p>
            </div>
            <div class="box-body">

                @if ($count >= 5 )
                <h3 class="text-center"><strong>Jumlah soal sudah mencapai batas maksimal.</strong></h3>
                <br>
                @else
                <form role="form" method="POST" action="{{ url('uraian/add') }}">
                    @csrf

                    <div class="box-body">

                      <div class="form-group">
                        <label for="exampleInputEmail1">Soal</label>
                        <textarea class="form-control" name="soalurn" rows="5"placeholder="Soal"></textarea>
                      </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                  @endif

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){

        // btn refresh
        $('.btn-refresh').click(function(e){
            e.preventDefault();
            $('.preloader').fadeIn();
            location.reload();
        })

    })
</script>
