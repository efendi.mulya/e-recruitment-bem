@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <h4>{{ $title }}</h4>
        <div class="box box-warning">
            <div class="box-header">
                <p>
                    <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                    @if ($count < 5 )
                    <a href="{{ url('uraian/add') }}" class="btn btn-sm btn-flat btn-primary "><i class="fa fa-plus"></i> Tambah Soal</a>
                    @endif

                </p>
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-stripped myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Soal Uraian</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $e=>$dt)
                            <tr>
                                <td>{{ $e+1 }}</td>
                                <td>{{ $dt->soalurn }}</td>
                                <td>
                                    <div style="width:80px">

                                        <a href="{{ url('uraian/'.$dt->id) }}"
                                            class="btn btn-warning btn-xs btn-edit" id="edit"><i class="fa fa-pencil-square-o"></i></a>


                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){

        // btn refresh
        $('.btn-refresh').click(function(e){
            e.preventDefault();
            $('.preloader').fadeIn();
            location.reload();
        })

    })
</script>

@endsection
