@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <h4>{{ $title }}</h4>
        <div class="box box-warning">
            <div class="box-header">
                <p>
                    <a href="{{ url('pilihanganda') }}" class="btn btn-sm btn-flat btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>

                    <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                </p>
            </div>
            <div class="box-body">

                <form role="form" method="POST" action="{{ url('pilihanganda/'.$dt->id) }}">
                    @csrf
                    {{ method_field('put') }}

                    <div class="box-body">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Pilih Jenis Soal</label>
                            <select class="form-control select2" name="jenispg">
                                <option value="{{ $dt->jenispg }}">{{ $dt->jenispg }} (Digunakan sebelumnya)</option>
                                <option value="Keorganisasian">Keorganisasian</option>
                                <option value="Matematika">Matematika</option>
                                <option value="Psikotes">Psikotes</option>
                                <option value="Kewarganegaraan">Kewarganegaraan</option>
                                <option value="Wawasan Kampus">Wawasan Kampus</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Soal</label>
                            <textarea class="form-control" name="soalpg" rows="5"placeholder="Soal">{{ $dt->soalpg }}</textarea>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="exampleInputEmail1">Kunci Jawaban</label>
                                </div>
                                <div class="col-md-2">
                                    @if ($dt->jawabanpg == $dt->a)
                                    <input type="radio" id="jawabanA" name="check" checked>
                                    @else
                                    <input type="radio" id="jawabanA" name="check">
                                    @endif
                                    <label for="exampleInputEmail1">A</label>
                                </div>
                                <div class="col-md-2">
                                    @if ($dt->jawabanpg == $dt->b)
                                    <input type="radio" id="jawabanB" name="check" checked>
                                    @else
                                    <input type="radio" id="jawabanB" name="check">
                                    @endif
                                    <label for="exampleInputEmail1">B</label>
                                </div>
                                <div class="col-md-2">
                                    @if ($dt->jawabanpg == $dt->c)
                                    <input type="radio" id="jawabanC" name="check" checked>
                                    @else
                                    <input type="radio" id="jawabanC" name="check">
                                    @endif
                                    <label for="exampleInputEmail1">C</label>
                                </div>
                                <div class="col-md-2">
                                    @if ($dt->jawabanpg == $dt->d)
                                    <input type="radio" id="jawabanD" name="check" checked>
                                    @else
                                    <input type="radio" id="jawabanD" name="check">
                                    @endif
                                    <label for="exampleInputEmail1">D</label>
                                </div>
                                <div class="col-md-2">
                                    @if ($dt->jawabanpg == $dt->e)
                                    <input type="radio" id="jawabanE" name="check" checked>
                                    @else
                                    <input type="radio" id="jawabanE" name="check">
                                    @endif
                                    <label for="exampleInputEmail1">E</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Jawaban A</label>
                            <input type="text" name="a" class="form-control" id="A" placeholder="Jawaban A" value="{{ $dt->a }}" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Jawaban B</label>
                            <input type="text" name="b" class="form-control" id="B" placeholder="Jawaban B" value="{{ $dt->b }}">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Jawaban C</label>
                            <input type="text" name="c" class="form-control" id="C" placeholder="Jawaban C" value="{{ $dt->c }}">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Jawaban D</label>
                            <input type="text" name="d" class="form-control" id="D" placeholder="Jawaban D" value="{{ $dt->d }}">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Jawaban E</label>
                            <input type="text" name="e" class="form-control" id="E" placeholder="Jawaban E" value="{{ $dt->e }}">
                        </div>

                        <div class="form-group">
                            {{-- <label for="exampleInputEmail1">Jawaban</label> --}}
                            <input type="hidden" name="jawabanpg" class="form-control" id="kunci" placeholder="Kunci Jawaban" value="{{ $dt->jawabanpg }}" readonly>
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    @if ($dt->jawabanpg == $dt->a)
        document.getElementById('kunci').value = document.getElementById('A').value;
        document.getElementById("A").onkeyup = function(value) {
            var x = document.getElementById('A').value;
            document.getElementById('kunci').value = x;
        };
    @elseif ($dt->jawabanpg == $dt->b)
        document.getElementById('kunci').value = document.getElementById('B').value;
        document.getElementById("B").onkeyup = function(value) {
            var x = document.getElementById('B').value;
            document.getElementById('kunci').value = x;
        };
    @elseif ($dt->jawabanpg == $dt->c)
        document.getElementById('kunci').value = document.getElementById('C').value;
        document.getElementById("C").onkeyup = function(value) {
            var x = document.getElementById('C').value;
            document.getElementById('kunci').value = x;
        };
    @elseif ($dt->jawabanpg == $dt->d)
        document.getElementById('kunci').value = document.getElementById('D').value;
        document.getElementById("D").onkeyup = function(value) {
            var x = document.getElementById('D').value;
            document.getElementById('kunci').value = x;
        };
    @elseif ($dt->jawabanpg == $dt->e)
        document.getElementById('kunci').value = document.getElementById('E').value;
        document.getElementById("E").onkeyup = function(value) {
            var x = document.getElementById('E').value;
            document.getElementById('kunci').value = x;
        };
    @endif
</script>

<script type="text/javascript">

    $('#jawabanA').click(function(e){
        document.getElementById('kunci').value = document.getElementById('A').value;
        document.getElementById("A").onkeyup = function(value) {
            var x = document.getElementById('A').value;
            document.getElementById('kunci').value = x;
        };
    })

    $('#jawabanB').click(function(e){
        document.getElementById('kunci').value = document.getElementById('B').value;
        document.getElementById("B").onkeyup = function(value) {
            var x = document.getElementById('B').value;
            document.getElementById('kunci').value = x;
        };
    })

    $('#jawabanC').click(function(e){
        document.getElementById('kunci').value = document.getElementById('C').value;
        document.getElementById("C").onkeyup = function(value) {
            var x = document.getElementById('C').value;
            document.getElementById('kunci').value = x;
        };
    })

    $('#jawabanD').click(function(e){
        document.getElementById('kunci').value = document.getElementById('D').value;
        document.getElementById("D").onkeyup = function(value) {
            var x = document.getElementById('D').value;
            document.getElementById('kunci').value = x;
        };
    })

    $('#jawabanE').click(function(e){
        document.getElementById('kunci').value = document.getElementById('E').value;
        document.getElementById("E").onkeyup = function(value) {
            var x = document.getElementById('E').value;
            document.getElementById('kunci').value = x;
        };
    })

</script>

<script type="text/javascript">
    $(document).ready(function(){

        // btn refresh
        $('.btn-refresh').click(function(e){
            e.preventDefault();
            $('.preloader').fadeIn();
            location.reload();
        })

    })
</script>

@endsection
