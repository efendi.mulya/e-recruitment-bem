@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <h4>{{ $title }}</h4>
        <div class="box box-warning">
            <div class="box-header">
                <p>
                    <a href="{{ url('pilihanganda') }}" class="btn btn-sm btn-flat btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>

                    <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                </p>
            </div>
            <div class="box-body">

                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Jenis Soal</label>
                        <input type="text" name="jenispg" class="form-control" id="exampleInputEmail1" value="{{ $dt->jenispg }}" readonly>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Soal</label>
                        <textarea class="form-control" name="soalpg" rows="5"placeholder="Soal" readonly>{{ $dt->soalpg }}</textarea>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label for="exampleInputEmail1">Kunci Jawaban</label>
                            </div>
                            <div class="col-md-2">
                                @if ($dt->jawabanpg == $dt->a)
                                <input type="radio" id="jawabanA" name="check" checked>
                                @else
                                <input type="radio" id="jawabanA" name="check" disabled>
                                @endif
                                <label for="exampleInputEmail1">A</label>
                            </div>
                            <div class="col-md-2">
                                @if ($dt->jawabanpg == $dt->b)
                                <input type="radio" id="jawabanB" name="check" checked>
                                @else
                                <input type="radio" id="jawabanB" name="check" disabled>
                                @endif
                                <label for="exampleInputEmail1">B</label>
                            </div>
                            <div class="col-md-2">
                                @if ($dt->jawabanpg == $dt->c)
                                <input type="radio" id="jawabanC" name="check" checked>
                                @else
                                <input type="radio" id="jawabanC" name="check" disabled>
                                @endif
                                <label for="exampleInputEmail1">C</label>
                            </div>
                            <div class="col-md-2">
                                @if ($dt->jawabanpg == $dt->d)
                                <input type="radio" id="jawabanD" name="check" checked>
                                @else
                                <input type="radio" id="jawabanD" name="check" disabled>
                                @endif
                                <label for="exampleInputEmail1">D</label>
                            </div>
                            <div class="col-md-2">
                                @if ($dt->jawabanpg == $dt->e)
                                <input type="radio" id="jawabanE" name="check" checked>
                                @else
                                <input type="radio" id="jawabanE" name="check" disabled>
                                @endif
                                <label for="exampleInputEmail1">E</label>
                            </div>
                        </div>
                    </div>

                    @if ($dt->jawabanpg == $dt->a)
                    <div class="form-group has-success">
                    @else
                    <div class="form-group">
                    @endif
                        <label for="exampleInputEmail1">Jawaban A</label>
                        <input type="text" name="a" class="form-control" id="exampleInputEmail1" value="{{ $dt->a }}" readonly>
                    </div>

                    @if ($dt->jawabanpg == $dt->b)
                    <div class="form-group has-success">
                    @else
                    <div class="form-group">
                    @endif
                        <label for="exampleInputEmail1">Jawaban B</label>
                        <input type="text" name="b" class="form-control" id="exampleInputEmail1" value="{{ $dt->b }}" readonly>
                    </div>

                    @if ($dt->jawabanpg == $dt->c)
                    <div class="form-group has-success">
                    @else
                    <div class="form-group">
                    @endif
                        <label for="exampleInputEmail1">Jawaban C</label>
                        <input type="text" name="c" class="form-control" id="exampleInputEmail1" value="{{ $dt->c }}" readonly>
                    </div>

                    @if ($dt->jawabanpg == $dt->d)
                    <div class="form-group has-success">
                    @else
                    <div class="form-group">
                    @endif
                        <label for="exampleInputEmail1">Jawaban D</label>
                        <input type="text" name="d" class="form-control" id="exampleInputEmail1" value="{{ $dt->d }}" readonly>
                    </div>

                    @if ($dt->jawabanpg == $dt->e)
                    <div class="form-group has-success">
                    @else
                    <div class="form-group">
                    @endif
                        <label for="exampleInputEmail1">Jawaban E</label>
                        <input type="text" name="e" class="form-control" id="exampleInputEmail1" value="{{ $dt->e }}" readonly>
                    </div>

                    {{-- <div class="form-group">
                        <label for="exampleInputEmail1">Jawaban</label>
                        <input type="text" name="jawabanpg" class="form-control" id="exampleInputEmail1" value="{{ $dt->jawabanpg }}" readonly>
                    </div> --}}

                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    @if ($dt->jawabanpg == $dt->a)
        document.getElementById('kunci').value = document.getElementById('A').value;
        document.getElementById("A").onkeyup = function(value) {
            var x = document.getElementById('A').value;
            document.getElementById('kunci').value = x;
        };
    @elseif ($dt->jawabanpg == $dt->b)
        document.getElementById('kunci').value = document.getElementById('B').value;
        document.getElementById("B").onkeyup = function(value) {
            var x = document.getElementById('B').value;
            document.getElementById('kunci').value = x;
        };
    @elseif ($dt->jawabanpg == $dt->c)
        document.getElementById('kunci').value = document.getElementById('C').value;
        document.getElementById("C").onkeyup = function(value) {
            var x = document.getElementById('C').value;
            document.getElementById('kunci').value = x;
        };
    @elseif ($dt->jawabanpg == $dt->d)
        document.getElementById('kunci').value = document.getElementById('D').value;
        document.getElementById("D").onkeyup = function(value) {
            var x = document.getElementById('D').value;
            document.getElementById('kunci').value = x;
        };
    @elseif ($dt->jawabanpg == $dt->e)
        document.getElementById('kunci').value = document.getElementById('E').value;
        document.getElementById("E").onkeyup = function(value) {
            var x = document.getElementById('E').value;
            document.getElementById('kunci').value = x;
        };
    @endif
</script>

<script type="text/javascript">
    $(document).ready(function(){

        // btn refresh
        $('.btn-refresh').click(function(e){
            e.preventDefault();
            $('.preloader').fadeIn();
            location.reload();
        })

    })
</script>

@endsection
