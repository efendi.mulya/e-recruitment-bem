@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <h4>{{ $title }}</h4>
        <div class="box box-warning">
            <div class="box-header">
                <p>
                    <a href="{{ url('pilihanganda') }}" class="btn btn-sm btn-flat btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>

                    <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                </p>
            </div>
            <div class="box-body">

                <form role="form" method="POST" action="{{ url('pilihanganda/add') }}">
                    @csrf

                    <div class="box-body">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Pilih Jenis Soal</label>
                            <select class="form-control select2" name="jenispg">
                                <option value="Keorganisasian">Keorganisasian</option>
                                <option value="Matematika">Matematika</option>
                                <option value="Psikotes">Psikotes</option>
                                <option value="Kewarganegaraan">Kewarganegaraan</option>
                                <option value="Wawasan Kampus">Wawasan Kampus</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Soal</label>
                            <textarea class="form-control" name="soalpg" rows="5"placeholder="Soal"></textarea>
                        </div>

                        {{-- <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="exampleInputEmail1">Kunci Jawaban</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" id="jawabanA" name="r3" class="minimal-red" style="position: absolute; opacity: 0;">
                                    <label>A</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" id="jawabanB" name="r3" class="minimal-red" style="position: absolute; opacity: 0;">
                                    <label>B</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" id="jawabanC" name="r3" class="minimal-red" style="position: absolute; opacity: 0;">
                                    <label>C</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" id="jawabanD" name="r3" class="minimal-red" style="position: absolute; opacity: 0;">
                                    <label>D</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" id="jawabanE" name="r3" class="minimal-red" style="position: absolute; opacity: 0;">
                                    <label>E</label>
                                </div>
                            </div>
                        </div> --}}

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="exampleInputEmail1">Kunci Jawaban</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" id="jawabanA" name="check">
                                    <label for="exampleInputEmail1">A</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" id="jawabanB" name="check">
                                    <label for="exampleInputEmail1">B</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" id="jawabanC" name="check">
                                    <label for="exampleInputEmail1">C</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" id="jawabanD" name="check">
                                    <label for="exampleInputEmail1">D</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" id="jawabanE" name="check">
                                    <label for="exampleInputEmail1">E</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Jawaban A</label>
                            <input type="text" name="a" class="form-control" id="A" placeholder="Jawaban A">
                            {{-- <input type="text" name="a" class="form-control" id="A" onkeyup="keyA(this.value);" placeholder="Jawaban A"> --}}
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Jawaban B</label>
                            <input type="text" name="b" class="form-control" id="B" placeholder="Jawaban B">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Jawaban C</label>
                            <input type="text" name="c" class="form-control" id="C" placeholder="Jawaban C">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Jawaban D</label>
                            <input type="text" name="d" class="form-control" id="D" placeholder="Jawaban D">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Jawaban E</label>
                            <input type="text" name="e" class="form-control" id="E" placeholder="Jawaban E">
                        </div>

                        <div class="form-group">
                            {{-- <label for="exampleInputEmail1">Kunci Jawaban</label> --}}
                            <input type="hidden" name="jawabanpg" class="form-control" id="kunci" placeholder="Kunci Jawaban" readonly>
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">

    $('#jawabanA').click(function(e){
        document.getElementById('kunci').value = document.getElementById('A').value;
        document.getElementById("A").onkeyup = function(value) {
            var x = document.getElementById('A').value;
            document.getElementById('kunci').value = x;
        };
    })

    $('#jawabanB').click(function(e){
        document.getElementById('kunci').value = document.getElementById('B').value;
        document.getElementById("B").onkeyup = function(value) {
            var x = document.getElementById('B').value;
            document.getElementById('kunci').value = x;
        };
    })

    $('#jawabanC').click(function(e){
        document.getElementById('kunci').value = document.getElementById('C').value;
        document.getElementById("C").onkeyup = function(value) {
            var x = document.getElementById('C').value;
            document.getElementById('kunci').value = x;
        };
    })

    $('#jawabanD').click(function(e){
        document.getElementById('kunci').value = document.getElementById('D').value;
        document.getElementById("D").onkeyup = function(value) {
            var x = document.getElementById('D').value;
            document.getElementById('kunci').value = x;
        };
    })

    $('#jawabanE').click(function(e){
        document.getElementById('kunci').value = document.getElementById('E').value;
        document.getElementById("E").onkeyup = function(value) {
            var x = document.getElementById('E').value;
            document.getElementById('kunci').value = x;
        };
    })

</script>

{{-- <script type="text/javascript">
    function keyA(value) {
        var x = document.getElementById('A').value;
        document.getElementById('kunci').value = x;
    }
</script> --}}

<script type="text/javascript">
    $(document).ready(function(){

        // btn refresh
        $('.btn-refresh').click(function(e){
            e.preventDefault();
            $('.preloader').fadeIn();
            location.reload();
        })

    })
</script>

@endsection
