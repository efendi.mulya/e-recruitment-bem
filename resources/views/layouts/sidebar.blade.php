<section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <?php
            $formulir = \DB::select("SELECT * FROM m_formulir WHERE status = 0");
            $u = \Auth::user()->id;
            $cek = \DB::select("SELECT * FROM m_nilai WHERE a1 != $u AND a2 != $u");
        ?>

        <ul class="sidebar-menu" data-widget="tree">

            <li class="menu-sidebar"><a href="{{ url('/home') }}"><span class="fa fa-tachometer"></span> Dashboard</span></a></li>

            @if (auth()->user()->level=="Admin")

                <li class="header">PENGGUNA</li>

                <li class="menu-sidebar"><a href="{{ url('/admin') }}"><i class="fa fa-user"></i><span>Admin</span></a></li>

                <li class="menu-sidebar"><a href="{{ url('/peserta') }}"><i class="fa fa-users"></i><span>Peserta</span></a></li>

                <li class="header">SPK</li>

                <li class="menu-sidebar"><a href="{{ url('/kementrian') }}"><i class="fa fa-commenting"></i><span>Kementrian</span></a></li>

                <li class="menu-sidebar"><a href="{{ url('/soalspk') }}"><i class="fa fa-question-circle"></i><span>Bank Soal SPK</span></a></li>

                <li class="header">FORMULIR</li>

                <li class="menu-sidebar"><a href="{{ url('/penerimaan') }}">
                    <i class="fa fa-file-text"></i>
                    <span>Verifikasi Formulir</span>
                    @if (count($formulir) != 0)
                    <span class="pull-right-container">
                        <small class="label pull-right bg-yellow">{{ count($formulir) }}</small>
                    </span>
                    @else
                    @endif
                </a></li>

                <li class="header">UJIAN</li>

                <li class="menu-sidebar"><a href="{{ url('/pilihanganda') }}"><i class="fa fa-check-square"></i><span>Bank Soal Pilihan Ganda</span></a></li>

                <li class="menu-sidebar"><a href="{{ url('/uraian') }}"><i class="fa fa-pencil-square"></i><span>Bank Soal Uraian</span></a></li>

            @elseif (auth()->user()->level=="Asesor")

                <li class="header">UJIAN</li>

                <li class="menu-sidebar"><a href="{{ url('/penilaian') }}">
                    <i class="fa fa-bar-chart"></i>
                    <span>Penilaian</span>
                    @if (count($cek) != 0)
                        <span class="pull-right-container">
                            <small class="label pull-right bg-yellow">{{ count($cek) }}</small>
                        </span>
                    @else
                    @endif
                </a></li>

            @endif

            <li class="header">LAINNYA</li>

            {{-- @if(\Auth::user()->name == 'Admin')
            <li class="menu-sidebar"><a href="{{ url('/reset-password') }}"><span class="glyphicon glyphicon-log-out"></span> Reset Password</span></a></li>
            @endif --}}

            <li class="menu-sidebar"><a href="{{ url('/keluar') }}"><span class="glyphicon glyphicon-log-out"></span> Keluar</span></a></li>


        </ul>
    </section>
