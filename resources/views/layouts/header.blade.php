<!-- Logo -->
<?php
    $dt = \App\User::where('id',\Auth::user()->id)->first();
    $formulir = \DB::select("SELECT * FROM m_formulir WHERE status = 0");
    $u = \Auth::user()->id;
    $cek = \DB::select("SELECT * FROM m_nilai WHERE a1 != $u AND a2 != $u");
    if (count($cek) == 0) {
        $total = count($formulir);
    } else {
        $total = count($formulir) + count($cek);
    }
?>
<a href="{{ url('/home') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>A</b>LT</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>{{ \Auth::user()->level }}</b></span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        <!-- Tasks: style can be found in dropdown.less -->

        <!-- User Account: style can be found in dropdown.less -->


            @if (auth()->user()->level=="Asesor")
            <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bell-o"></i>
                    @if (count($cek) != 0)
                    <span class="label label-warning">{{ count($cek) }}</span>
                    @else
                    @endif
                </a>
                <ul class="dropdown-menu">
                    <li class="header">Notifikasi</li>
                    <li>
                    <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            @if (count($cek) != 0)
                                {{-- @if (count($formulir) != 0)
                                <li>
                                    <a href="{{ url('penerimaan/') }}">
                                    <i class="fa fa-file-text text-yellow"></i> ({{ count($formulir) }}) Formulir belum diverivikasi
                                    </a>
                                </li>
                                @else
                                @endif --}}
                                @if (count($cek) != 0)
                                <li>
                                    <a href="{{ url('penilaian/') }}">
                                    <i class="fa fa-bar-chart text-yellow"></i> ({{ count($cek) }}) Jawaban belum dinilai
                                    </a>
                                </li>
                                @else
                                @endif
                            @else
                            <li>
                                <p class="text-center">(Tidak ada notifikasi)</p>
                            </li>
                            @endif
                        </ul>

                    </li>
                </ul>
            </li>
            @endif

            @if (auth()->user()->level=="Admin")
            <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bell-o"></i>
                    @if (count($formulir) != 0)
                    <span class="label label-warning">{{ count($formulir) }}</span>
                    @else
                    @endif
                </a>
                <ul class="dropdown-menu">
                    <li class="header">Notifikasi</li>
                    <li>
                    <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            @if (count($formulir) != 0)
                                @if (count($formulir) != 0)
                                <li>
                                    <a href="{{ url('penerimaan/') }}">
                                    <i class="fa fa-file-text text-yellow"></i> ({{ count($formulir) }}) Formulir belum diverivikasi
                                    </a>
                                </li>
                                @else
                                @endif
                            @else
                            <li>
                                <p class="text-center">(Tidak ada notifikasi)</p>
                            </li>
                            @endif
                        </ul>

                    </li>
                </ul>
            </li>
            @endif

            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="{{ asset('matrix/assets/images/users/1.jpg') }}" class="user-image" alt="User Image">
                <span class="hidden-xs">{{\Auth::user()->name}}</span>
                </a>
                <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                    <img src="{{ asset('matrix/assets/images/users/1.jpg') }}" class="img-circle" alt="User Image">

                    <p>
                    {{\Auth::user()->name}}
                    <small>{{ \Auth::user()->level }}</small>
                    </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                    <div class="pull-left">
                    <a href="{{ url('/profil-admin') }}" class="btn btn-default btn-flat menu-sidebar">Edit Profil</a>
                    </div>
                    <div class="pull-right">
                    <a href="{{ url('keluar') }}" class="btn btn-default btn-flat menu-sidebar">Keluar</a>
                    </div>
                </li>
                </ul>
            </li>

        </ul>
    </div>

</nav>
