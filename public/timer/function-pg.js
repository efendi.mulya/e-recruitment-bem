const startingMinutes = 60;
let time = startingMinutes * 60;
// let time = 300;


const timer = document.querySelector('#timer');
const kirim = document.querySelector('#kirim');

let interval = setInterval(updateCountdown, 1000)

function updateCountdown() {
    let minutes = Math.floor(time / 60);
    let seconds = time % 60;

    seconds = seconds < 10 ? '0' + seconds : seconds;
    minutes = minutes < 10 ? '0' + minutes : minutes;

    timer.innerHTML = `${minutes} : ${seconds}`;
    time--;
    if (time == 0){
        clearInterval(interval);
        timer.innerHTML = 'selesai';
        kirim.click();
    }
}
