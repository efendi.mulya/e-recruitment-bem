/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.1.38-MariaDB : Database - oprec-bem
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`oprec-bem` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `oprec-bem`;

/*Table structure for table `data_latih_line` */

DROP TABLE IF EXISTS `data_latih_line`;

CREATE TABLE `data_latih_line` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kementrian` int(10) unsigned NOT NULL,
  `soalspk` int(10) unsigned NOT NULL,
  `vektor` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `data_latih_line_kementrian_foreign` (`kementrian`),
  KEY `data_latih_line_soalspk_foreign` (`soalspk`),
  CONSTRAINT `data_latih_line_kementrian_foreign` FOREIGN KEY (`kementrian`) REFERENCES `m_kementrian` (`id`) ON DELETE CASCADE,
  CONSTRAINT `data_latih_line_soalspk_foreign` FOREIGN KEY (`soalspk`) REFERENCES `m_soalspk` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `data_latih_line` */

insert  into `data_latih_line`(`id`,`kementrian`,`soalspk`,`vektor`,`created_at`,`updated_at`) values 
(1,11,1,0.9647,'2021-04-04 06:28:34','2021-07-08 07:05:11'),
(2,11,2,0.9879,'2021-04-04 06:28:34','2021-07-08 07:05:11'),
(3,11,4,0,'2021-04-04 06:28:34','2021-07-08 07:05:11'),
(4,11,5,0,'2021-04-04 06:28:34','2021-07-08 07:05:11'),
(5,11,6,0.9457,'2021-04-04 06:28:34','2021-07-08 07:05:11'),
(6,11,7,0,'2021-04-04 06:28:34','2021-07-08 07:05:11'),
(7,11,8,0,'2021-04-04 06:28:34','2021-07-08 07:05:11'),
(8,11,9,0,'2021-04-04 06:28:34','2021-07-08 07:05:11'),
(9,11,10,0,'2021-04-04 06:28:34','2021-07-08 07:05:11'),
(10,11,11,0,'2021-04-04 06:28:35','2021-07-08 07:05:11'),
(11,11,12,0,'2021-04-04 06:28:35','2021-07-08 07:05:11'),
(12,11,13,0.9857,'2021-04-04 06:28:35','2021-07-08 07:05:11'),
(13,11,14,0,'2021-04-04 06:28:35','2021-07-08 07:05:11'),
(14,11,15,0,'2021-04-04 06:28:35','2021-07-08 07:05:11'),
(15,11,16,0,'2021-04-04 06:28:35','2021-07-08 07:05:11'),
(16,12,1,0.9743,'2021-04-04 08:08:36','2021-07-08 07:02:55'),
(17,12,2,0,'2021-04-04 08:08:36','2021-07-08 07:02:55'),
(18,12,4,0,'2021-04-04 08:08:36','2021-07-08 07:02:55'),
(19,12,5,0,'2021-04-04 08:08:36','2021-07-08 07:02:55'),
(20,12,6,0.9735,'2021-04-04 08:08:36','2021-07-08 07:02:55'),
(21,12,7,0.9783,'2021-04-04 08:08:36','2021-07-08 07:02:55'),
(22,12,8,0.9863,'2021-04-04 08:08:37','2021-07-08 07:02:55'),
(23,12,9,0.9987,'2021-04-04 08:08:37','2021-07-08 07:02:55'),
(24,12,10,0,'2021-04-04 08:08:37','2021-07-08 07:02:55'),
(25,12,11,0,'2021-04-04 08:08:37','2021-07-08 07:02:55'),
(26,12,12,0,'2021-04-04 08:08:37','2021-07-08 07:02:55'),
(27,12,13,0.9634,'2021-04-04 08:08:37','2021-07-08 07:02:55'),
(28,12,14,0,'2021-04-04 08:08:37','2021-07-08 07:02:55'),
(29,12,15,0,'2021-04-04 08:08:37','2021-07-08 07:02:55'),
(30,12,16,0,'2021-04-04 08:08:37','2021-07-08 07:02:55'),
(31,13,1,0.9768,'2021-04-04 08:31:01','2021-07-08 07:06:33'),
(32,13,2,0,'2021-04-04 08:31:01','2021-07-08 07:06:33'),
(33,13,4,0.9985,'2021-04-04 08:31:01','2021-07-08 07:06:33'),
(34,13,5,0.9874,'2021-04-04 08:31:01','2021-07-08 07:06:33'),
(35,13,6,0.9672,'2021-04-04 08:31:01','2021-07-08 07:06:33'),
(36,13,7,0,'2021-04-04 08:31:01','2021-07-08 07:06:33'),
(37,13,8,0,'2021-04-04 08:31:01','2021-07-08 07:06:33'),
(38,13,9,0,'2021-04-04 08:31:01','2021-07-08 07:06:33'),
(39,13,10,0,'2021-04-04 08:31:02','2021-07-08 07:06:33'),
(40,13,11,0.9912,'2021-04-04 08:31:02','2021-07-08 07:06:33'),
(41,13,12,0,'2021-04-04 08:31:02','2021-07-08 07:06:33'),
(42,13,13,0,'2021-04-04 08:31:02','2021-07-08 07:06:33'),
(43,13,14,0,'2021-04-04 08:31:02','2021-07-08 07:06:33'),
(44,13,15,0,'2021-04-04 08:31:02','2021-07-08 07:06:33'),
(45,13,16,0,'2021-04-04 08:31:02','2021-07-08 07:06:33'),
(46,14,1,0.9463,'2021-04-04 08:59:04','2021-07-08 07:05:50'),
(47,14,2,0,'2021-04-04 08:59:05','2021-07-08 07:05:50'),
(48,14,4,0,'2021-04-04 08:59:05','2021-07-08 07:05:50'),
(49,14,5,0,'2021-04-04 08:59:05','2021-07-08 07:05:50'),
(50,14,6,0.9748,'2021-04-04 08:59:05','2021-07-08 07:05:50'),
(51,14,7,0.9632,'2021-04-04 08:59:05','2021-07-08 07:05:50'),
(52,14,8,0,'2021-04-04 08:59:05','2021-07-08 07:05:50'),
(53,14,9,0,'2021-04-04 08:59:05','2021-07-08 07:05:50'),
(54,14,10,0,'2021-04-04 08:59:05','2021-07-08 07:05:50'),
(55,14,11,0,'2021-04-04 08:59:05','2021-07-08 07:05:50'),
(56,14,12,0,'2021-04-04 08:59:05','2021-07-08 07:05:50'),
(57,14,13,0.9854,'2021-04-04 08:59:05','2021-07-08 07:05:50'),
(58,14,14,0.9875,'2021-04-04 08:59:05','2021-07-08 07:05:50'),
(59,14,15,0.9989,'2021-04-04 08:59:05','2021-07-08 07:05:50'),
(60,14,16,0,'2021-04-04 08:59:05','2021-07-08 07:05:50'),
(61,15,1,0.9526,'2021-04-04 08:59:35','2021-07-08 07:03:52'),
(62,15,2,0,'2021-04-04 08:59:35','2021-07-08 07:03:52'),
(63,15,4,0,'2021-04-04 08:59:35','2021-07-08 07:03:52'),
(64,15,5,0,'2021-04-04 08:59:35','2021-07-08 07:03:52'),
(65,15,6,0.9754,'2021-04-04 08:59:35','2021-07-08 07:03:52'),
(66,15,7,0.9758,'2021-04-04 08:59:35','2021-07-08 07:03:52'),
(67,15,8,0.9978,'2021-04-04 08:59:35','2021-07-08 07:03:52'),
(68,15,9,0.9963,'2021-04-04 08:59:35','2021-07-08 07:03:52'),
(69,15,10,0,'2021-04-04 08:59:35','2021-07-08 07:03:52'),
(70,15,11,0,'2021-04-04 08:59:36','2021-07-08 07:03:52'),
(71,15,12,0.9758,'2021-04-04 08:59:36','2021-07-08 07:03:52'),
(72,15,13,0,'2021-04-04 08:59:36','2021-07-08 07:03:52'),
(73,15,14,0,'2021-04-04 08:59:36','2021-07-08 07:03:52'),
(74,15,15,0,'2021-04-04 08:59:36','2021-07-08 07:03:52'),
(75,15,16,0,'2021-04-04 08:59:36','2021-07-08 07:03:52'),
(76,16,1,0.9243,'2021-04-04 09:00:34','2021-07-08 06:56:15'),
(77,16,2,0,'2021-04-04 09:00:35','2021-07-08 06:56:15'),
(78,16,4,0,'2021-04-04 09:00:35','2021-07-08 06:56:15'),
(79,16,5,0,'2021-04-04 09:00:35','2021-07-08 06:56:15'),
(80,16,6,0.9345,'2021-04-04 09:00:35','2021-07-08 06:56:15'),
(81,16,7,0,'2021-04-04 09:00:35','2021-07-08 06:56:15'),
(82,16,8,0,'2021-04-04 09:00:35','2021-07-08 06:56:15'),
(83,16,9,0,'2021-04-04 09:00:35','2021-07-08 06:56:15'),
(84,16,10,0.9988,'2021-04-04 09:00:35','2021-07-08 06:56:15'),
(85,16,11,0.9888,'2021-04-04 09:00:35','2021-07-08 06:56:15'),
(86,16,12,0,'2021-04-04 09:00:35','2021-07-08 06:56:15'),
(87,16,13,0.9744,'2021-04-04 09:00:35','2021-07-08 06:56:15'),
(88,16,14,0,'2021-04-04 09:00:36','2021-07-08 06:56:15'),
(89,16,15,0,'2021-04-04 09:00:36','2021-07-08 06:56:15'),
(90,16,16,0,'2021-04-04 09:00:36','2021-07-08 06:56:15'),
(91,17,1,0,'2021-04-18 07:27:53','2021-07-08 07:04:24'),
(92,17,2,0,'2021-04-18 07:27:53','2021-07-08 07:04:24'),
(93,17,4,0,'2021-04-18 07:27:53','2021-07-08 07:04:24'),
(94,17,5,0,'2021-04-18 07:27:53','2021-07-08 07:04:24'),
(95,17,6,0.9898,'2021-04-18 07:27:53','2021-07-08 07:04:24'),
(96,17,7,0,'2021-04-18 07:27:53','2021-07-08 07:04:24'),
(97,17,8,0,'2021-04-18 07:27:53','2021-07-08 07:04:24'),
(98,17,9,0,'2021-04-18 07:27:53','2021-07-08 07:04:24'),
(99,17,10,0,'2021-04-18 07:27:53','2021-07-08 07:04:24'),
(100,17,11,0,'2021-04-18 07:27:53','2021-07-08 07:04:24'),
(101,17,12,0,'2021-04-18 07:27:53','2021-07-08 07:04:24'),
(102,17,13,0.9475,'2021-04-18 07:27:53','2021-07-08 07:04:24'),
(103,17,14,0,'2021-04-18 07:27:53','2021-07-08 07:04:24'),
(104,17,15,0,'2021-04-18 07:27:53','2021-07-08 07:04:24'),
(105,17,16,1,'2021-04-18 07:27:53','2021-07-08 07:04:24');

/*Table structure for table `m_datahasil` */

DROP TABLE IF EXISTS `m_datahasil`;

CREATE TABLE `m_datahasil` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `spk_id` int(10) unsigned DEFAULT NULL,
  `lugri` double DEFAULT NULL,
  `sosma` double DEFAULT NULL,
  `dagri` double DEFAULT NULL,
  `kemhas` double DEFAULT NULL,
  `psdm` double DEFAULT NULL,
  `agama` double DEFAULT NULL,
  `kominfo` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `m_datahasil_user_id_foreign` (`user_id`),
  KEY `m_datahasil_spk_id_foreign` (`spk_id`),
  CONSTRAINT `m_datahasil_spk_id_foreign` FOREIGN KEY (`spk_id`) REFERENCES `m_soalspk` (`id`),
  CONSTRAINT `m_datahasil_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `m_datahasil` */

insert  into `m_datahasil`(`id`,`user_id`,`spk_id`,`lugri`,`sosma`,`dagri`,`kemhas`,`psdm`,`agama`,`kominfo`,`created_at`,`updated_at`) values 
(46,3,1,0.21594609,0.22733824,0.22496049,0.20484676,0.19918368999999997,0.18003049000000002,0.25,'2021-07-09 07:31:49','2021-07-09 07:31:49'),
(47,3,2,0.54449641,0.0625,0.0625,0.0625,0.0625,0.0625,0.0625,'2021-07-09 07:31:49','2021-07-09 07:31:49'),
(48,3,4,0.0625,0.5602522500000001,0.0625,0.0625,0.0625,0.0625,0.0625,'2021-07-09 07:31:49','2021-07-09 07:31:49'),
(49,3,5,0,0.9749587600000001,0,0,0,0,0,'2021-07-09 07:31:49','2021-07-09 07:31:49'),
(50,3,6,0.8943484899999999,0.93547584,0.9477022500000001,0.9514051600000001,0.95023504,0.87329025,0.97970404,'2021-07-09 07:31:49','2021-07-09 07:31:49'),
(51,3,7,0.25,0.25,0.22877089,0.22638564,0.21455424,0.25,0.25,'2021-07-09 07:31:49','2021-07-09 07:31:49'),
(52,3,8,0.0625,0.0625,0.5421376899999999,0.55920484,0.0625,0.0625,0.0625,'2021-07-09 07:31:49','2021-07-09 07:31:49'),
(53,3,9,0,0,0.9974016900000001,0.9926136899999999,0,0,0,'2021-07-09 07:31:49','2021-07-09 07:31:49'),
(54,3,10,0.25,0.25,0.25,0.25,0.25,0.24880144,0.25,'2021-07-09 07:31:49','2021-07-09 07:31:49'),
(55,3,11,0.0625,0.54937744,0.0625,0.0625,0.0625,0.54582544,0.0625,'2021-07-09 07:31:49','2021-07-09 07:31:49'),
(56,3,12,0,0,0,0.95218564,0,0,0,'2021-07-09 07:31:49','2021-07-09 07:31:49'),
(57,3,13,0.97160449,0,0.9281395600000001,0,0.9710131600000002,0.9494553600000001,0.89775625,'2021-07-09 07:31:49','2021-07-09 07:31:49'),
(58,3,14,0,0,0,0,0.9751562500000001,0,0,'2021-07-09 07:31:49','2021-07-09 07:31:49'),
(59,3,15,0,0,0,0,0.99780121,0,0,'2021-07-09 07:31:49','2021-07-09 07:31:49'),
(60,3,16,0.0625,0.0625,0.0625,0.0625,0.0625,0.0625,0.5625,'2021-07-09 07:31:49','2021-07-09 07:31:49');

/*Table structure for table `m_datauji` */

DROP TABLE IF EXISTS `m_datauji`;

CREATE TABLE `m_datauji` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `spk_id` int(10) unsigned NOT NULL,
  `jawab` double NOT NULL,
  `lugri` double DEFAULT NULL,
  `sosma` double DEFAULT NULL,
  `dagri` double DEFAULT NULL,
  `kemhas` double DEFAULT NULL,
  `psdm` double DEFAULT NULL,
  `agama` double DEFAULT NULL,
  `kominfo` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `m_datauji_user_id_foreign` (`user_id`),
  KEY `m_datauji_spk_id_foreign` (`spk_id`),
  CONSTRAINT `m_datauji_spk_id_foreign` FOREIGN KEY (`spk_id`) REFERENCES `m_soalspk` (`id`),
  CONSTRAINT `m_datauji_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `m_datauji` */

insert  into `m_datauji`(`id`,`user_id`,`spk_id`,`jawab`,`lugri`,`sosma`,`dagri`,`kemhas`,`psdm`,`agama`,`kominfo`,`created_at`,`updated_at`) values 
(1,3,1,0.5,0.4647,0.4768,0.47430000000000005,0.4526,0.44630000000000003,0.4243,-0.5,'2021-07-09 06:50:27','2021-07-09 07:31:47'),
(2,3,2,0.25,0.7379,-0.25,-0.25,-0.25,-0.25,-0.25,-0.25,'2021-07-09 06:50:27','2021-07-09 07:31:47'),
(3,3,4,0.25,-0.25,0.7485,-0.25,-0.25,-0.25,-0.25,-0.25,'2021-07-09 06:50:27','2021-07-09 07:31:47'),
(4,3,5,0,0,0.9874,0,0,0,0,0,'2021-07-09 06:50:27','2021-07-09 07:31:47'),
(5,3,6,0,0.9457,0.9672,0.9735,0.9754,0.9748,0.9345,0.9898,'2021-07-09 06:50:27','2021-07-09 07:31:47'),
(6,3,7,0.5,-0.5,-0.5,0.47829999999999995,0.4758,0.46319999999999995,-0.5,-0.5,'2021-07-09 06:50:27','2021-07-09 07:31:47'),
(7,3,8,0.25,-0.25,-0.25,0.7363,0.7478,-0.25,-0.25,-0.25,'2021-07-09 06:50:27','2021-07-09 07:31:47'),
(8,3,9,0,0,0,0.9987,0.9963,0,0,0,'2021-07-09 06:50:27','2021-07-09 07:31:47'),
(9,3,10,0.5,-0.5,-0.5,-0.5,-0.5,-0.5,0.4988,-0.5,'2021-07-09 06:50:27','2021-07-09 07:31:47'),
(10,3,11,0.25,-0.25,0.7412,-0.25,-0.25,-0.25,0.7388,-0.25,'2021-07-09 06:50:27','2021-07-09 07:31:47'),
(11,3,12,0,0,0,0,0.9758,0,0,0,'2021-07-09 06:50:27','2021-07-09 07:31:47'),
(12,3,13,0,0.9857,0,0.9634,0,0.9854,0.9744,0.9475,'2021-07-09 06:50:27','2021-07-09 07:31:47'),
(13,3,14,0,0,0,0,0,0.9875,0,0,'2021-07-09 06:50:27','2021-07-09 07:31:47'),
(14,3,15,0,0,0,0,0,0.9989,0,0,'2021-07-09 06:50:27','2021-07-09 07:31:47'),
(15,3,16,0.25,-0.25,-0.25,-0.25,-0.25,-0.25,-0.25,0.75,'2021-07-09 06:50:27','2021-07-09 07:31:47');

/*Table structure for table `m_formulir` */

DROP TABLE IF EXISTS `m_formulir`;

CREATE TABLE `m_formulir` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `nim` int(10) NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenkel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prodi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_wa` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `riwayat_org` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kementrian1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kementrian2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `motivasi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kekurangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelebihan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_ktm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_diri` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `m_formulir_user_id_foreign` (`user_id`),
  CONSTRAINT `m_formulir_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `m_formulir` */

insert  into `m_formulir`(`id`,`user_id`,`nim`,`nama`,`jenkel`,`prodi`,`kelas`,`no_wa`,`riwayat_org`,`kementrian1`,`kementrian2`,`motivasi`,`kekurangan`,`kelebihan`,`foto_ktm`,`foto_diri`,`status`,`created_at`,`updated_at`) values 
(1,3,1931733094,'Muhamad Efendi Mulya','Pria','Manajemen Informatika','1-D','085790510999','Belum punya','Kementrian Kemahasiswaan','Kementrian Pengembangan Sumber Daya Mahasiswa','Saya ingin belajar berorganisasi','contoh','contoh','assets/images/formulir/1931733094_KTM_1624437486.jpg','assets/images/formulir/1931733094_PAS_1624437486.jpg',1,'2021-06-23 08:38:06','2021-06-23 08:38:45'),
(2,6,1931733999,'Adi Waluyo','Pria','Teknik Mesin','1-C','081122334455','contoh','Kementrian Sosial Masyarakat','Kementrian Pengembangan Sumber Daya Mahasiswa','contoh','contoh','contoh','assets/images/formulir/1931733999_KTM_1624954019.jpg','assets/images/formulir/1931733999_PAS_1624954019.jpg',1,'2021-06-29 08:06:59','2021-06-29 08:10:57');

/*Table structure for table `m_jawabanpg` */

DROP TABLE IF EXISTS `m_jawabanpg`;

CREATE TABLE `m_jawabanpg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `soal_id` int(10) unsigned NOT NULL,
  `jawab` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kunci` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skor` int(2) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `m_jawabanpg_user_id_foreign` (`user_id`),
  KEY `m_jawabanpg_soal_id_foreign` (`soal_id`),
  CONSTRAINT `m_jawabanpg_soal_id_foreign` FOREIGN KEY (`soal_id`) REFERENCES `m_pilihanganda` (`id`),
  CONSTRAINT `m_jawabanpg_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `m_jawabanpg` */

insert  into `m_jawabanpg`(`id`,`user_id`,`soal_id`,`jawab`,`kunci`,`skor`,`created_at`,`updated_at`) values 
(1,3,9,'Akan bertanggung jawab','Akan bertanggung jawab',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(2,3,6,'Menerima kritikan tersebut sebagai masukan','Menerima kritikan tersebut sebagai masukan',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(3,3,1,'Dengan senang hati mempertimbangkan pendapat orang tersebut','Tetap beranggapan bahwa perbedaan pendapat adalah hal yang lumrah',0,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(4,3,3,'Menerima aturan panitia dan berusaha mengenal dan memahami teman sekamar','Menerima aturan panitia dan berusaha mengenal dan memahami teman sekamar',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(5,3,16,'Meningkatkan kesadaran mahasiswa terkait fungsi kredit point.','Meningkatkan kesadaran mahasiswa terkait fungsi kredit point.',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(6,3,20,'54, 47','54, 47',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(7,3,27,'10','10',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(8,3,31,'10','10',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(9,3,33,'Rp. 50.000,-','Rp. 50.000,-',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(10,3,22,'101 – 79','101 – 79',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(11,3,36,'Dongeng : Peristiwa','Dongeng : Peristiwa',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(12,3,37,'Lidah : Hidung','Lidah : Hidung',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(13,3,44,'Haus : Air','Haus : Air',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(14,3,43,'Atap : Genteng','Atap : Genteng',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(15,3,39,'Sapi : Susu','Sapi : Susu',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(16,3,50,'Jl. Pegangsaan Timur 56, Jakarta Pusat','Jl. Pegangsaan Timur 56, Jakarta Pusat',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(17,3,57,'16,056','16,056',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(18,3,58,'Memperkuat semangat dan tekat para pemuda untuk bersatu','Memperkuat semangat dan tekat para pemuda untuk bersatu',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(19,3,62,'Falsafah dan dasar negara','Pandangan hidup',0,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(20,3,51,'Radjiman Widyodiningrat','Radjiman Widyodiningrat',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(21,3,75,'Jl. Lingkar Maskumambang, Sukorame Mojoroto Kota Kediri','Jl. Lingkar Maskumambang, Pojok Mojoroto Kota Kediri',0,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(22,3,74,'Jl. Mayor Bismo 27, Kediri','Jl. Mayor Bismo 27, Kediri',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(23,3,70,'Fadlis Sukya S.Kom M.Cs','Fadlis Sukya S.Kom M.Cs',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(24,3,73,'Menjalin hubungan dengan ukm dan menjembatani antara ukm dengan manjemen','Menjalin hubungan dengan ukm dan menjembatani antara ukm dengan manjemen',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(25,3,76,'Dosen pembimbing kemahasiswaan','Dosen pembimbing kemahasiswaan',4,'2021-06-26 07:49:35','2021-06-26 07:49:35'),
(26,6,10,'Dapat memutuskan sendiri dan realistis','Percaya diri dan mandiri',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(27,6,11,'Memberikan kesempatan beraktualisasi diri','Menyediakan stabilitas pekerjaan kepada saya.',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(28,6,7,'Mereka adalah rekan kerja yang menyenangkan','Mereka adalah rekan kerja yang menyenangkan',4,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(29,6,15,'Mengkaji isu-isu dan menjalin hubungan dengan pihak luar/kampus lain.','Mengkaji isu-isu dan menjalin hubungan dengan pihak luar/kampus lain.',4,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(30,6,16,'Mengkaji isu-isu dalam kampus','Meningkatkan kesadaran mahasiswa terkait fungsi kredit point.',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(31,6,20,'22, 15','54, 47',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(32,6,28,'10 buah','12 buah',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(33,6,24,'12','10',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(34,6,23,'10','6',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(35,6,25,'556','256',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(36,6,44,'Bosan : Tidur','Haus : Air',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(37,6,39,'Jentik : Nyamuk','Sapi : Susu',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(38,6,48,'Papan','Kursi tamu',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(39,6,46,'Sakarin','Sakarin',4,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(40,6,42,'Hangat : Listrik','Terang : Matahari',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(41,6,52,'Mufakat dan Demokrasi, Kebangsaan Indonesia,Kesejahteraan Sosial','Mufakat dan Demokrasi, Kebangsaan Indonesia,Kesejahteraan Sosial',4,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(42,6,60,'Piagam Jakarta','Pembukaan UUD 1945',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(43,6,64,'19 Agustus 1945','20 Agustus 1945',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(44,6,54,'Bintang','Kepala banteng',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(45,6,63,'Sosial budaya','Pribadi',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(46,6,66,'Maskuri, S.T., M.T.','Drs. Awan Setiawan, M.MT., M.M.',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(47,6,79,'Agustono Heriadi','Ratna Widyastuti',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(48,6,76,'Dosen Pendukung Keputusan','Dosen pembimbing kemahasiswaan',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(49,6,73,'Menghimpun dana beasiswa mahasiswa','Menjalin hubungan dengan ukm dan menjembatani antara ukm dengan manjemen',0,'2021-06-29 08:12:05','2021-06-29 08:12:05'),
(50,6,69,'Septian wijaya aminullah','Indah wulansari',0,'2021-06-29 08:12:05','2021-06-29 08:12:05');

/*Table structure for table `m_jawabanurn` */

DROP TABLE IF EXISTS `m_jawabanurn`;

CREATE TABLE `m_jawabanurn` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `soal_id` int(10) unsigned NOT NULL,
  `jawab` text COLLATE utf8mb4_unicode_ci,
  `skor` float DEFAULT NULL,
  `a1` int(10) DEFAULT NULL,
  `n1` int(11) DEFAULT NULL,
  `a2` int(10) DEFAULT NULL,
  `n2` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `m_jawabanurn_user_id_foreign` (`user_id`),
  CONSTRAINT `m_jawabanurn_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `m_jawabanurn` */

insert  into `m_jawabanurn`(`id`,`user_id`,`soal_id`,`jawab`,`skor`,`a1`,`n1`,`a2`,`n2`,`created_at`,`updated_at`) values 
(1,3,1,'Wadah atau tempat berkumpulnya orang dengan 3 sistematis, terpimpin, terkendali, terencana, rasional dalam memanfaatkan segala sumber daya baik dengan metode, material, lingkungan dan uang serta sarana dan prasarana, dan lain sebagainya dengan efisien dan efektif untuk bisa mencapai tujuan organisasi.',17.5,4,20,5,15,'2021-06-26 08:00:11','2021-07-05 06:08:24'),
(2,3,2,'1. dengan melakukan komunikasi dan hubungan yang baik dengan para anggota suatu organisasi\r\n2. membentuk program kerja yang kreatif yang mampu memajukan organisasi tersebut\r\n3. Membentuk susunan kepengurusan seperti adanya seksi-seksi/pokja-pokja disetiap bidang. dalam suatu organisasi harus dibagi antar per anggota misalnya : adanya seksi perlengkapan,seksi acara dll\r\n4. Mampu saling bersosialisasi dengan para anggota dan orang lain',17.5,4,20,5,15,'2021-06-26 08:00:11','2021-07-05 06:08:24'),
(3,3,3,'Tergantung..\r\nUntuk dibidang yang benar - benar saya kuasai. Artinya saya punya kemampuan lebih diantara orang lain, saya akan memilih menjadi pemimpin. Begitupun sebaliknya.',15,4,15,5,15,'2021-06-26 08:00:11','2021-07-05 06:08:24'),
(4,3,4,'Menjalankan sistem pembelajaran melalui media kelas online seperti Google Classroom',12.5,4,10,5,15,'2021-06-26 08:00:11','2021-07-05 06:08:24'),
(5,3,5,'Saya kurang bisa memanajemen waktu dalam pengerjaan tugas individu akan tetapi saya lebih tertib apabila melakukan pengerjaan tugas secara kelompok',15,4,15,5,15,'2021-06-26 08:00:11','2021-07-05 06:08:24'),
(6,6,1,'contoh',7.5,5,10,4,5,'2021-06-29 08:12:17','2021-07-05 06:06:35'),
(7,6,2,'contoh',5,5,5,4,5,'2021-06-29 08:12:17','2021-07-05 06:06:35'),
(8,6,3,'contoh',10,5,15,4,5,'2021-06-29 08:12:17','2021-07-05 06:06:35'),
(9,6,4,'contoh',5,5,5,4,5,'2021-06-29 08:12:17','2021-07-05 06:06:35'),
(10,6,5,'contoh',7.5,5,10,4,5,'2021-06-29 08:12:17','2021-07-05 06:06:35');

/*Table structure for table `m_kementrian` */

DROP TABLE IF EXISTS `m_kementrian`;

CREATE TABLE `m_kementrian` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_kementrian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tupoksi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `m_kementrian` */

insert  into `m_kementrian`(`id`,`nama_kementrian`,`tupoksi`,`created_at`,`updated_at`) values 
(11,'Kementrian Luar Negeri','Kementrian luar negeri yang selanjutnya dapat disebut dengan kemenlu bertugas melakukan koordinasi yang baik dengan birokrasi kampus BEM PSDKU Politeknik Negeri Malang di Kota Kediri. Kemenlu berfungsi membangun Kerjasama bilateral atau multilateral dengan lembaga pemerintahan maupun non pemerintahan. Kemenlu perlu menjaga hubungan baik dengan lembaga yang telah dijalin. Selain itu, Kemenlu juga ditugaskan untuk mengoptimalkan kegiatan pengabdian masyarakat sebagai wujud konstribusi BEM PSDKU.','2021-04-04 06:28:34','2021-07-08 07:05:11'),
(12,'Kementrian Dalam Negeri','Kementrian Dalam Negeri atau Kemendagri bertugas mengoptimalkan komunikasi dan koordinasi antar elemen Organisasi Kemahasiswaan Intra (OKI) PSDKU secara keseluruhan. Hal tersebut untuk menjalin hubungan yang sinergis dan kolaboratif antar OKI. Kemendagri juga berperan menjadi wadah advokasi dan pelayanan yang proaktif terhadap aspirasi mahasiswa PSDKU. Kemendagri juga berfungsi untuk mewujudkan kegiatan BEM PSDKU yang konstributif untuk PSDKU Politeknik Negeri Malang di Kota Kediri.','2021-04-04 08:08:36','2021-07-08 07:02:55'),
(13,'Kementrian Sosial Masyarakat','Kementrian sosial masyarakat atau yang selanjutnya disebut kementrian sosma memiliki tugas untuk menjalin hubungan yang harmonis dengan masyarakat umum. Kementrian sosma sebagai kementrian yang meninjak lanjuti isi dari Tri Dharma perguruan tinggi yang ke-tiga yaitu pengabdian kepada masyarakat. Kementrian sosma bertugas membuat sebuah kegiatan untuk mengajak mahasiswa PSDKU untuk peduli dan tanggap terhadap keadaan masyarakat dengan memberikan inovasi yang baru dan bermanfaat kepada masyarakat yang tepat.','2021-04-04 08:31:01','2021-07-08 07:06:33'),
(14,'Kementrian Pengembangan Sumber Daya Mahasiswa','Kementrian pengembangan sumber daya mahasiswa atau PSDM bertugas membangun keterbukaan dalam komunikasi sebagai pondasi meningkatkan solidaritas yang professional diantara mahasiswa. PSDM juga berperan melakukan koordinasi yang intens terhadap lembaga – lembaga dan unit kegiatan mahasiswa dibawah naungan BEM PSDKU. PSDM juga berfungsi untuk mengontrol dan menjadwalkan kegiatan unit atau lembaga dibawah naungan BEM PSDKU.','2021-04-04 08:59:04','2021-07-08 07:05:50'),
(15,'Kementrian Kemahasiswaan','Kementrian kemahasiswaan atau kemhas bertugas mengadakan advokasi terhadap mahasiswa PSDKU Politeknik Negeri Malang di Kota Kediri yang sedang memiliki kendala atau permasalahan. Kemhas juga berperan untuk menumbuhkan sikap keilmiahan mahasiswa PSDKU yang berlandaskan disiplin ilmu dan kebutuhan saat ini. Selain itu, kemhas juga berperan menciptakan iklim kompetitif dan prestatif di bidang keilmiahan bagi seluruh mahasiswa PSDKU. Kemhas berfungsi membantu pengawalan terbentuknya Haluan Dasar Pengembangan Kemahasiswaan.','2021-04-04 08:59:35','2021-07-08 07:03:52'),
(16,'Kementrian Agama','Kementrian agama atau Kemenag bertugas menetapkan penyusunan agenda kegiatan menurut hari besar agama. Kemudian Kemenag akan mengadakan kegiatan menurut hari besar agama. Kemenag juga bertugas mengadakan kegiatan rutin agama secara berkesinambungan','2021-04-04 09:00:34','2021-07-08 06:56:15'),
(17,'Kementrian Komunikasi dan Informasi','Kementrian komunikasi dan informasi atau Kominfo bertugas meningkatkan citra positif BEM PSDKU kepada warga kampus PSDKU dan masyarakat luas. Kominfo bertugas untuk menyampaikan informasi program kerja, kegiatan dan agenda BEM PSDKU kepada warga kampus PSDKU terutama mahasiswa PSDKU. Selain itu, Kominfo berperan meningkatkan pengolahan dan kualitas informasi untuk menunjang kebutuhan mahasiswa di bidang akademik maupun non akademik.','2021-04-18 07:27:53','2021-07-08 07:04:24');

/*Table structure for table `m_nilai` */

DROP TABLE IF EXISTS `m_nilai`;

CREATE TABLE `m_nilai` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `nilaipg` int(11) NOT NULL,
  `nilaiurn` float DEFAULT NULL,
  `a1` int(10) NOT NULL DEFAULT '0',
  `total1` int(11) DEFAULT NULL,
  `a2` int(10) NOT NULL DEFAULT '0',
  `total2` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `m_nilai_user_id_foreign` (`user_id`),
  CONSTRAINT `m_nilai_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `m_nilai` */

insert  into `m_nilai`(`id`,`user_id`,`nilaipg`,`nilaiurn`,`a1`,`total1`,`a2`,`total2`,`created_at`,`updated_at`) values 
(1,3,88,77.5,4,80,5,75,'2021-06-26 07:49:35','2021-07-05 06:08:24'),
(2,6,16,35,5,45,4,25,'2021-06-29 08:12:05','2021-07-05 06:06:35');

/*Table structure for table `m_pengaturan` */

DROP TABLE IF EXISTS `m_pengaturan`;

CREATE TABLE `m_pengaturan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `m_pengaturan` */

insert  into `m_pengaturan`(`id`,`menu`,`status`,`created_at`,`updated_at`) values 
(1,'ujian',1,'2021-03-09 09:14:57','2021-06-23 08:39:30'),
(2,'formulir',1,'2021-03-09 09:14:57','2021-06-29 08:05:15');

/*Table structure for table `m_pilihanganda` */

DROP TABLE IF EXISTS `m_pilihanganda`;

CREATE TABLE `m_pilihanganda` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenispg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `soalpg` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bobotpg` int(11) NOT NULL,
  `a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `b` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `d` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `e` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jawabanpg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `m_pilihanganda` */

insert  into `m_pilihanganda`(`id`,`jenispg`,`soalpg`,`bobotpg`,`a`,`b`,`c`,`d`,`e`,`jawabanpg`,`created_at`,`updated_at`) values 
(1,'Keorganisasian','Dalam suatu rapat pimpinan, ada sejawat yang tidak sependapat dengan usulan saya ....',4,'Dengan senang hati mempertimbangkan pendapat orang tersebut','Bersikeras agar usulan saya yang diterima','Meninggalkan ruang rapat dan tidak kembali lagi','Mempertimbangkan pendapat tersebut dengan melihat siapaorangnya','Tetap beranggapan bahwa perbedaan pendapat adalah hal yang lumrah','Bersikeras agar usulan saya yang diterima','2021-03-09 06:17:42','2021-07-05 06:11:03'),
(3,'Keorganisasian','Saya diutus mengikuti suatu diklat. Oleh panitia penyelenggara saya ditempatkan sekamar dengan orang yang tidak saya kenal yang berasal dari kota lain. Sikap saya adalah…',4,'Mengajukan keberatan tetapi akhirnya menerima aturan panitia','Protes keran dan minta ditempatkan sendiri saja','Mengajukan keberatan dan minta ditempatkan dengan minimal orang yang dikenal','Menerima aturan panitia dan berusaha mengenal dan memahami teman sekamar','Menerima aturan panitia','Menerima aturan panitia dan berusaha mengenal dan memahami teman sekamar','2021-03-26 05:58:58','2021-03-26 05:58:58'),
(4,'Keorganisasian','Terjadi pergantian pimpinan di unit kerja saya. Sikap saya adalah …',4,'Tidak peduli','Berusaha mengenal dan memahami visi dan misi pimpinan baru','Tidak berusaha mendekati pimpinan baru karena takut dicap penjilat','Berusaha memahami dan mengenal pribadi pimpinan baru','Pergantian pimpinan itu sesuatu yang biasa','Berusaha mengenal dan memahami visi dan misi pimpinan baru','2021-03-26 06:01:41','2021-03-26 06:01:41'),
(5,'Keorganisasian','Tentang perubahan yang terjadi dalam organisasi, pendapat saya adalah…',4,'Perubahan adalah sesuatu yang lumrah','Menjaga stabilitas organisasi dengan sedapat mungkin menghindari perubahan','Pegawai tidak boleh terlalu kreatif menciptakan perubahan','Organisasi harus menjadi “learning organization”','Setiap individu dalam organisasi harus terbiasa dengan perubahan','Organisasi harus menjadi “learning organization”','2021-03-26 06:03:24','2021-03-26 06:03:24'),
(6,'Keorganisasian','Dalam rapat staf dan pimpinan, pendapat saya dikritik keras oleh peserta rapat lainnya. Respon saya adalah…',4,'Mencoba sekuat tenaga mempertahankan pendapat saya','Menyerang semua peserta yang mengeritik pendapat saya','Mencoba mempelajari kritikan tersebut dan berbalik mengkritik dengan tajam','Menerima kritikan tersebut sebagai masukan','Diam saja','Menerima kritikan tersebut sebagai masukan','2021-03-26 06:05:38','2021-03-26 06:05:38'),
(7,'Keorganisasian','Menurut anda apa pendapat anda tentang rekan kerja?',4,'Mereka bukan tim kerja yang membanggakan','Saya tidak sependapat dengan mereka','Mereka adalah teman yang baik dan selalu membimbing saya','Mereka adalah rekan kerja yang menyenangkan','Mereka adalah sahabat dan rekan kerja yang professional','Mereka adalah rekan kerja yang menyenangkan','2021-03-26 06:07:15','2021-03-26 06:07:15'),
(8,'Keorganisasian','Menurut saya, bekerja merupakan upaya untuk meraih kesuksesan. Untuk itu upaya saya adalah…',4,'Bekerja dengan cermat dan berusaha menjadi terbaik dan diakui','Berusaha menyingkirkan orang yang saya anggap menghalangi upaya saya','Bekerja sepenuh hati','Bekerja sampai larut malam','Bekerja berdasarkan kontrak kinerja','Bekerja dengan cermat dan berusaha menjadi terbaik dan diakui','2021-03-26 06:08:55','2021-03-26 06:08:55'),
(9,'Keorganisasian','Ketika muncul suatu masalah dalam pekerjaan yang kebetulan ada kaitannya dengan hal-hal yang menjadi kewajiban saya, maka saya…',4,'Mencari kambing hitam penyebab masalah','Akan bertanggung jawab','Membiarkan masalah tetap berlangsung','Melihat dulu apakah saya sebagai sumber masalah','Melihat sejauh mana apakah saya terlibat di dalamnya','Akan bertanggung jawab','2021-03-26 06:10:25','2021-03-26 06:10:25'),
(10,'Keorganisasian','Saya ingin rekan-rekan kerja saya melihat kualitas diri saya yang terbaik sebagai seorang yang…',4,'Inovatif','Disiplin dan penurut','Mampu beradaptasi dan ramah','Dapat memutuskan sendiri dan realistis','Percaya diri dan mandiri','Percaya diri dan mandiri','2021-03-26 06:11:59','2021-03-26 06:11:59'),
(11,'Keorganisasian','Kondisi kerja yang paling saya harapkan adalah kondisi yang...',4,'Menghasilkan tambahan penghasilan bagi saya.','Menyediakan stabilitas pekerjaan kepada saya.','Memberikan kemungkinan promosi jabatan kepada saya.','Memberikan kesempatan beraktualisasi diri','Memotivasi saya untuk bekerja lebih giat','Menyediakan stabilitas pekerjaan kepada saya.','2021-03-26 06:13:34','2021-03-26 06:13:34'),
(12,'Keorganisasian','Berikut ini yang bukan merupakan tupoksi dari kementerian Keagamaan adalah…',4,'Merancang program yang bertujuan untuk menjalin kerukunan anatar umat beragama.','Melakukan koordinasi dengan pihak luar atau instansi yang berkepentigan dalam kegiatan keagamaan','Menjadi wadah mahasiwa untuk menambah wawasan beragama','Meningkatkan kehidupan religious di lingkungan kampus dengan adanya kegitan-kegiatan beragama.','Sebagai advokasi mahasiswa dengan eksternal kampus','Sebagai advokasi mahasiswa dengan eksternal kampus','2021-03-26 06:15:02','2021-03-26 06:15:02'),
(13,'Keorganisasian','Berikut ini yang merupakan tupoksi dari kementerian Kemahasiswaan adalah…',4,'Memfasilitasi ruang kelas dalam kampus.','Tanggap membantu mahasiswa yang mengalami hambatan segi materiil saja.','Sebagai advokasi mahasiswa dengan eksternal kampus.','Meningkatkan kesadaran mahasiswa terkait fungsi kredit point.','Mengkaji isu-isu dalam kampus.','Meningkatkan kesadaran mahasiswa terkait fungsi kredit point.','2021-03-26 06:17:04','2021-03-26 06:17:04'),
(14,'Keorganisasian','Yang bertugas dalam menggali komunikasi dan informasi dari internal maupun eksternal yaitu kementerian…',4,'Kementerian Luar Negeri','Kementerian Agama','Kementerian Kominfo','Kementerian Dalam Negeri','Kementerian Kemahasiswaan','Kementerian Kominfo','2021-03-26 06:18:18','2021-03-26 06:18:18'),
(15,'Keorganisasian','Menurutmu Kementerian Luar Negeri itu seperti apa…',4,'Menjalin Kerjasama dengan internal kampus.','Melakukan pergerakan Bersama OKI PSDKU POLINEMA.','Menjembatani kegiatan mahasiswa dalam bidang keagamaan.','Mengkaji isu-isu dan menjalin hubungan dengan pihak luar/kampus lain.','Menampung dan merealisasikan aspirasi mahasiswa.','Mengkaji isu-isu dan menjalin hubungan dengan pihak luar/kampus lain.','2021-03-26 06:20:38','2021-03-26 06:20:38'),
(16,'Keorganisasian','Berikut ini yang bukan merupakan tupoksi dari kementerian Dalam Negeri adalah…',4,'Melakukan pergerakan bersama OKI PSDKU POLINEMA.','Menjalin kerjasama dengan pihak internal kampus.','Meningkatkan kesadaran mahasiswa terkait fungsi kredit point.','Mengkaji isu-isu dalam kampus','Menampung dan merealisasikan aspirasi mahasiswa yg idealis.','Meningkatkan kesadaran mahasiswa terkait fungsi kredit point.','2021-03-26 06:22:42','2021-03-26 06:22:42'),
(17,'Keorganisasian','Berikut ini yang merupakan tupoksi dari Kementerian Keuangan adalah…',4,'Bergerak dalam bidang materiil.','Menghimpun dana beasiswa mahasiswa.','Tanggap membantu mahasiswa yang mengalami hambatan segi non materiil saja.','Menjalin kerjasama dengan pihak internal kampus.','Melakukan pergerakan bersama OKI PSDKU POLINEMA.','Bergerak dalam bidang materiil.','2021-03-26 06:24:12','2021-03-26 06:24:12'),
(18,'Keorganisasian','Di bawah ini yang merupakan peran fungsi mahasiswa adalah, kecuali…',4,'Agent of Change','Social Control','Moral Force','Social Force','Iron Stock','Social Force','2021-03-26 06:25:29','2021-03-26 06:25:29'),
(19,'Keorganisasian','Pendidikan dan pengajaran, penelitian dan pengembangan, pengabdian kepada masyarakat berupakan bunyi dari?',4,'Sumpah mahasiswa','Tri Dharma mahasiswa','Tri Dharma perguruan tinggi','Fungsi Tri Dharma perguruan tinggi','Peran fungsi mahasiswa','Tri Dharma perguruan tinggi','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(20,'Matematika','72, 69, 56, 60, …, …',4,'74, 54','54, 47','47, 29','22, 15','18, 12','54, 47','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(21,'Matematika','34 … … 64 128 113 124 248',4,'77 dan 88','68 dan 82','68 dan 53','99 dan 77','53 dan 69','68 dan 53','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(22,'Matematika','11 - 99 - 77 - 12 - 100 - 78 - 13 - … - …',4,'79 – 101','78 – 100','100 – 78','101 – 79','77 - 101','101 – 79','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(23,'Matematika','625 - 1296 - 25 - 36 - 5 - …',4,'8','4','6','10','14','6','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(24,'Matematika','2, 4, 6, 8, …, 12',4,'9','10','11','12','13','10','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(25,'Matematika','2, 4, 16, …',4,'356','256','456','556','128','256','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(26,'Matematika','Seorang karyawan perpustakaan harus membawakan lima dus buku ke perpustakaan. Jika dus buku yang ada adalah sebanyak sembilan dus buku, berapa kali ia harus mengantarkan buku-buku tersebut jika ia hanya mampu mengangkat 2 dus buku dalam satu kali antar?',4,'4','5','6','7','8','5','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(27,'Matematika','100 – 4 – 90 – 7 – 80,……..',4,'8','9','10','11','12','10','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(28,'Matematika','Berapa banyak rusuk yang dimiliki kubus?',4,'12 buah','16 buah','32 buah','10 buah','22 buah','12 buah','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(29,'Matematika','1 Dasawarsa + 3 lustrum - 1 windu = ...',4,'23 tahun','20 tahun','17 tahun','13 tahun','10 tahun','17 tahun','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(30,'Matematika','..., 2, 5, 6, 7, 10, 9, 14',4,'6','5','4','3','1','3','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(31,'Matematika','Empat orang membangun jembatan untuk sungai dan selesai dalam 15 hari. Jika jembatan ingin diselesaikan dalam 6 hari, maka berapa orang yang diperlukan untuk menyelesaikannya?',4,'12','10','8','4','6','10','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(32,'Matematika','Waktu di negara S adalah 3 jam lebih cepat dibandingkan dengan negara M. Sebuah pesawat terbang bergerak dari negara S ke negara M pada pukul 5 pagi dan tiba 4 jam kemudian. Pada pukul berapakah pesawat tersebut tiba di kota M?',4,'2 pagi','3 pagi','4 pagi','6 pagi','9 pagi','6 pagi','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(33,'Matematika','Seorang pedagang menjual sepatu seharga Rp. 60.000,- dan memperoleh laba sebesar 20% dari harga belinya. Berapa harga belinya?',4,'Rp. 72.000,-','Rp. 56.000,-','Rp. 50.000,-','Rp. 48.000,-','Rp. 30.000,-','Rp. 50.000,-','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(34,'Matematika','Seorang pedagang menjual sebuah barang dengan harga Rp. 80.000,- dan mempunyai laba 25% dari harga belinya. Berapakah harga beli barang tersebut?',4,'Rp. 120.000,-','Rp. 100.000,-','Rp. 20.000,-','Rp. 96.000,-','Rp. 64.000,-','Rp. 64.000,-','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(35,'Psikotes','SEMINAR : SARJANA',4,'Akademi : Taruna','Rumah sakit : Pasien','Ruang pengdilan : Saksi','Konservator : Seniman','Perpustakaan : Peneliti','Perpustakaan : Peneliti','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(36,'Psikotes','FIKTIF : FAKTA',4,'Dagelan : Sandiwara','Dongeng : Peristiwa','Dugaaan : Rekaan','Data : Estimasi','Rencana : Projeksi','Dongeng : Peristiwa','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(37,'Psikotes','MATA : TELINGA',4,'Jari : Tangan','Lidah : Hidung','Kaki : Paha','Lutut : Siku','Perut : Dada','Lidah : Hidung','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(38,'Psikotes','UANG : PUNDI -PUNDI',4,'Hubungan : Jambangan','Gelas : Nampan','Air : Tempayan','Buku : Percetakan','Rokok : Asbak','Air : Tempayan','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(39,'Psikotes','POHON : BUAH',4,'Domba : Daging','Sapi : Susu','Telur : Ayam','Jentik : Nyamuk','Setam : Sampit','Sapi : Susu','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(40,'Psikotes','SENAPAN : BERBURU',4,'Kapal : Berlabuh','Kereta : Langsir','Pancing : Ikan','Perangkap : Menangkap','Perang : Mengasah','Perangkap : Menangkap','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(41,'Psikotes','BATA : TANAH LIAT',4,'Batu : Pasir','Tembikar : Keramik','Bunga : Buah','Beton : Semen','Kertas : Buku','Beton : Semen','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(42,'Psikotes','PANAS : API',4,'Hujan : Awan','Abu : Arang','Terang : Matahari','Hangat : Listrik','Dingin : Beku','Terang : Matahari','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(43,'Psikotes','KULIT : SISIK',4,'Tegal : Lantai','Rumah : Kamar','Keramik : Mozaik','Dinding : Cat','Atap : Genteng','Atap : Genteng','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(44,'Psikotes','LAPAR : NASI',4,'Haus : Air','Mual : Obat','Mengantuk : Melamun','Bosan : Tidur','Membaca : Gambar','Haus : Air','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(45,'Psikotes','ES : AIR = AIR :…',4,'Mendidih','Membeku','Uap','Cair','Kabut','Uap','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(46,'Psikotes','PEDAS : CABAI = MANIS :…',4,'Gadis','Manisan','Teh botol','Sakarin','Kecap','Sakarin','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(47,'Psikotes','PEDATI : KUDA = Pesawat Terbang :…',4,'Sayap','Baling-Baling','Pramugari','Pilot','Landasan','Baling-Baling','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(48,'Psikotes','BERAS : NASI GORENG = Kayu :…',4,'Pasak','Kursi tamu','Gelondongan','Papan','Tripleks','Kursi tamu','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(49,'Psikotes','LAMPU : GELAP = MAKANAN :…',4,'Kenyang','Penuh','Gizi','Mulas','Lapar','Lapar','2021-03-26 06:26:42','2021-03-26 06:26:42'),
(50,'Kewarganegaraan','Dimana Ir. Soekarno membacakan teks Proklamasi pada tanggal 17 Agustus 1945...',4,'Rumah Ikada','Jl. Pegangsaan Timur 56, Jakarta Pusat','Jl. Pegangsaan Timur 65, Jakarta Pusat','Istana Negara','Rumah Rengasdengklok','Jl. Pegangsaan Timur 56, Jakarta Pusat','2003-09-21 06:17:00','0000-00-00 00:00:00'),
(51,'Kewarganegaraan','Siapa nama Ketua BPUPKI?',4,'Ir. Soekarno','Moh. Hatta','Radjiman Widyodiningrat','Sutan Syahrir','Mohammad Yamin','Radjiman Widyodiningrat','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(52,'Kewarganegaraan','Sebelum disahkan menjadi menjadi pancasila, terdapat 3 tokoh negara yang mengemukakan pendapatnya mengenai dasar Negara Republik Indonesia. Manakah yang termasuk gagasan dari Ir. Soekarno...',4,'Peri Ketuhanan,Peri Kerakyatan, Keadilan Sosial','Persatuan, Kesatuan, Musyawarah','Kesejahteraan Rakyat, Keadilan Sosial, Musyawarah','Mufakat dan Demokrasi, Kebangsaan Indonesia,Kesejahteraan Sosial','Ketuhanan yang maha esa, Mufakat dan Demokrasi, Persatuan','Mufakat dan Demokrasi, Kebangsaan Indonesia,Kesejahteraan Sosial','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(53,'Kewarganegaraan','Padi dan kapas diartikan sebagai kebutuhan dasar berupa sandang dan pangan, bahkan rakyat indonesia tanpa melihat status dan kedudukan tidak adanya kesengjangan antara yang satu dengan yang lainnya. Pada penjelasan diatas terdapat kata artian perwujudan lambang pancasila yang ke ..',4,'Sila ke 3','Sila ke 4','Sila ke 5','Sila ke 2','Sila ke 1','Sila ke 5','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(54,'Kewarganegaraan','Mahasiswa juga mempunyai peran yaitu mengutamakan kepentingan negara untuk masa depan serta menagih hak hak rakyat kepada pemerintahan. Pada penjelasan diatas merupakan perwujudan dari nilai pancasila yang di lambangkan ..',4,'Kepala banteng','Pohon beringin','Padi dan kapas','Bintang','Rantai','Kepala banteng','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(55,'Kewarganegaraan','Seorang pemimpin yang berasal dari masyarakat batak yang pernah melawan belanda dimasa dulunya serta di nobatkan sebagai raja ke 12 bersamaan dengan masuknya belanda di sumatra utara adalah ..',4,'Sisingamangaraja','Ompu Sohahuaon','Amir Hamzah','Adam Malik','I.r Soekarno','Sisingamangaraja','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(56,'Kewarganegaraan','Berapakah jumlah provinsi Di Indonesia?',4,'32','35','34','36','40','34','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(57,'Kewarganegaraan','Indonesia merupakan negara kepulauan yang berarti Indonesia mempunyai banyak pulau, ada berapakah jumlah pulau di Negara Indonesia yang telah dibakukan namanya di PBB hingga Juli 2017?',4,'16,056','16,667','16,754','16,660','16,065','16,056','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(58,'Kewarganegaraan','Pengaruh sumpah pemuda 28 Oktober 1928 bagi perjuangan bangsa Indonesia adalah..',4,'Memperkuat semangat dan tekat para pemuda untuk bersatu','Membangkitkan kesadaran nasional','Belanda bersikap lunak kepada pejuang indonesia','Mempercepat proses kemerdekaan indonesia','Jepang bersikap lunak kepada pejuang indonesia','Memperkuat semangat dan tekat para pemuda untuk bersatu','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(59,'Kewarganegaraan','Dalam kehidupan bernegara, Pancasila berperan sebagai...',4,'Dasar negara','Dasar kenegaraan','Dasar beragama','Dasar ketatanegaraan','Dasar kebangsaan','Dasar negara','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(60,'Kewarganegaraan','Rumusan Pancasila yang resmi terdapat dalam..',4,'Pidato Bung Karno','Proklamasi 17 Agustus 1945','Pembukaan UUD 1945','Piagam Jakarta','Buku Undang-Undang','Pembukaan UUD 1945','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(61,'Kewarganegaraan','Dalam sumber tata hukum di Indonesia, Pancasila dijadikan sebagai …',4,'Sumber dari segala sumber hukum','Hukum tertinggi di Indonesia','Hukum tertulis tertinggi di Indonesia','Setingkat dengan UUD 1945','Hukum tertinggi di dunia','Sumber dari segala sumber hukum','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(62,'Kewarganegaraan','Pancasila bagi bangsa Indonesia merupakan …',4,'Pandangan hidup','Falsafah dan dasar negara','Sumber hukum','Semua benar','Semua salah','Pandangan hidup','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(63,'Kewarganegaraan','Beribadah dan menganut suatu agama atau kepecayaan adalah merupakan asasi …',4,'Pribadi','Perlakuan dan perlindungan','Politik','Sosial budaya','Pendidikan','Pribadi','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(64,'Kewarganegaraan','Secara formal Pancasila disahkan sebagai dasar negara Republik Indonesia pada tanggal …',4,'1 Juni 1945','17 Agustus 1945','18 Agustus 1945','19 Agustus 1945','20 Agustus 1945','20 Agustus 1945','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(65,'Wawasan Kampus','Dandung Novianto, S.T.,M.T merupakan ..... dari, PSDKU Politeknik Negeri Malang di Kota Kediri',4,'Ketua program studi Teknik Mesin','Ketua program studi Akuntansi','Ketua program studi Manajemen Informatika','Koordinator','Dewan Perwakilan Kemahasiswaan','Koordinator','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(66,'Wawasan Kampus','Di bawah ini yang merupakan direktur dari Politeknik Negeri Malang adalah?',4,'Ahmad Dony Mutiara Bahtiar, S.T., M.T.','Dr. Aang Afandi, S.E. M.M.','Drs. Awan Setiawan, M.MT., M.M.','Maskuri, S.T., M.T.','Fadelis Sukya, S.Kom.M.Cs.','Drs. Awan Setiawan, M.MT., M.M.','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(67,'Wawasan Kampus','Di bawah ini yang bukan merupakan pembantu direktur I,II, III, dan IV Politeknik Negeri Malang adalah?',4,'Supriatna Adhisuwignjo, ST., MT.','Drs. Awan Setiawan, M.MT., M.M.','Drs. Halid Hasan, M.SRAT.HRM.','Dr. Eng. Anggit Murdani, ST., M.Eng.','Dr. Luchis Rubianto, LRSC., M.MT.','Drs. Awan Setiawan, M.MT., M.M.','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(68,'Wawasan Kampus','Siapa nama ketua BEM PSDKU POLINEMA di kota Kediri?',4,'Sindi nur charisma','Dimas pramana prado','Indah wulansari','Septian wijaya aminullah','Ilham bachtiar','Dimas pramana prado','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(69,'Wawasan Kampus','Siapa nama wakil ketua BEM PSDKU POLINEMA di kota Kediri?',4,'Sindi nur charisma','Dimas pramana prado','Indah wulansari','Septian wijaya aminullah','Ilham bachtiar','Indah wulansari','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(70,'Wawasan Kampus','Siapa nama ketua program studi manajemen informatika?',4,'Dandung Novianto S.T M.T','Fadlis Sukya S.Kom M.Cs','Dr Aang Affandi S.E M.M','Maskuri S.T M.T','Dr. Eng. Anggit Murdani, ST., M.Eng.','Fadlis Sukya S.Kom M.Cs','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(71,'Wawasan Kampus','Siapa nama ketua program studi teknik mesin?',4,'Dandung Novianto S.T M.T','Fadlis Sukya S.Kom M.Cs','Dr Aang Affandi S.E M.M','Maskuri S.T M.T','Dr. Eng. Anggit Murdani, ST., M.Eng.','Maskuri S.T M.T','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(72,'Wawasan Kampus','Siapa nama ketua program study akuntansi?',4,'Dandung Novianto S.T M.T','Fadlis Sukya S.Kom M.Cs','Dr Aang Affandi S.E M.M','Maskuri S.T M.T','Ahmad Dony Mutiara Bahtiar, S.T., M.T','Dr Aang Affandi S.E M.M','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(73,'Wawasan Kampus','Berikut ini yang merupakan tupoksi dari kementerian Pengembangan Sumber Daya Manusia (PSDM) adalah …',4,'Bergerak dalam bidang keuangan yang ada di dalam organisasi','Menjalin Kerjasama dengan pihak yang ada di luar kampus','Mengkaji isu-isu dalam kampus','Menghimpun dana beasiswa mahasiswa','Menjalin hubungan dengan ukm dan menjembatani antara ukm dengan manjemen','Menjalin hubungan dengan ukm dan menjembatani antara ukm dengan manjemen','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(74,'Wawasan Kampus','Dimanakah alamat kampus 1 PSDKU Politeknik Negeri Malang di Kota Kediri?',4,'Jl. Lingkar Maskumambang, Sukorame Mojoroto Kota Kediri','Jl. Mayor Bismo 19, Kediri','Jl. Lingkar Maskumambang, Pojok Mojoroto Kota Kediri','Jl. Mayor Bismo 27, Kediri','Jl. Lingkar Maskumambang, Mojoroto Kota Kediri','Jl. Mayor Bismo 27, Kediri','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(75,'Wawasan Kampus','Dimanakah alamat kampuus 2 PSDKU Politeknik Negeri Malang di Kota Kediri?',4,'Jl. Lingkar Maskumambang, Sukorame Mojoroto Kota Kediri','Jl. Mayor Bismo 19. Kediri','Jl. Lingkar Maskumambang, Pojok Mojoroto Kota Kediri','Jl. Mayor Bismo 27. Kediri','Jl. Lingkar Maskumambang, Mojoroto Kota Kediri','Jl. Lingkar Maskumambang, Pojok Mojoroto Kota Kediri','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(76,'Wawasan Kampus','Setiap organisasi intra kampus di PSDKU Polinema Kota Kediri memiliki seorang pembina yang disebut DPK. Manakan kepanjangan DPK yang benar?',4,'Dosen Perwakilan Kemahasiswaan','Dosen Pelaksana Kemahasiswaan','Dewan Pembina Kemahasiswaan','Dosen Pendukung Keputusan','Dosen pembimbing kemahasiswaan','Dosen pembimbing kemahasiswaan','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(77,'Wawasan Kampus','DPK merupakan perwakilan dari program studi yang ditugaskan untuk membimbing dan membina organisasi intra kampus. Setiap program studi memiliki satu dosen yang memiliki kewenangan sebagai DPK. Siapakah DPK yang berasal dari program studi akuntansi?',4,'Ahmad Dony Mutia Bahtiar','Wiwiek Kusumaning Asmoro','Ratna Widyastuti','Agustono Heriadi','Benni Agung Nugroho','Wiwiek Kusumaning Asmoro','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(78,'Wawasan Kampus','DPK merupakan perwakilan dari program studi yang ditugaskan untuk membimbing dan membina organisasi intra kampus. Setiap program studi memiliki satu dosen yang memiliki kewenangan sebagai DPK. Siapakah DPK yang berasal dari program studi teknik mesin?',4,'Ahmad Dony Mutia Bahtiar','Wiwiek Kusumaning Asmoro','Ratna Widyastuti','Agustono Heriadi','Benni Agung Nugroho','Ahmad Dony Mutia Bahtiar','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(79,'Wawasan Kampus','DPK merupakan perwakilan dari program studi yang ditugaskan untuk membimbing dan membina organisasi intra kampus. Setiap program studi memiliki satu dosen yang memiliki kewenangan sebagai DPK. Siapakah DPK yang berasal dari program studi manajemen informatika?',4,'Ahmad Dony Mutia Bahtiar','Wiwiek Kusumaning Asmoro','Ratna Widyastuti','Agustono Heriadi','Benni Agung Nugroho','Ratna Widyastuti','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(80,'Keorganisasian','contoh',4,'jawab a','jawab b','jawab c','jawab d','jawab e','jawab c','2021-07-05 06:09:53','2021-07-05 06:09:53');

/*Table structure for table `m_soalspk` */

DROP TABLE IF EXISTS `m_soalspk`;

CREATE TABLE `m_soalspk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `soal_spk` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `m_soalspk` */

insert  into `m_soalspk`(`id`,`soal_spk`,`created_at`,`updated_at`) values 
(1,'Apakah anda lebih suka bekerja di lapangan daripada kerja di balik layar ?','2021-03-26 06:26:42','2021-06-17 05:55:58'),
(2,'Apakah anda siap bila ditugaskan menjadi delegasi BEM untuk kegiatan undangan Organisasi Lain atau Kampus lain hingga keluar Kota?','2021-03-26 08:14:20','2021-03-26 08:24:11'),
(4,'Apakah anda suka kunjungan pada sebuah Desa untuk melakukan pembinaan disana?','2021-03-26 08:26:12','2021-03-26 08:26:12'),
(5,'Apakah anda suka mengajar Siswa SD?','2021-03-26 08:27:42','2021-03-26 08:27:42'),
(6,'Apakah anda mampu berbicara atau mengemukakan pendapat di depan Umum?','2021-03-26 08:27:55','2021-03-26 08:27:55'),
(7,'Apakah anda suka berada dalam kegiatan forum diskusi?','2021-03-26 08:28:11','2021-03-26 08:28:11'),
(8,'Apakah anda mampu berkomunikasi dengan pihak manajemen kampus untuk kepentingan mahasiswa dan organisasi?','2021-03-26 08:28:28','2021-03-26 08:28:28'),
(9,'Apakah anda siap untuk mendengarkan keluhan dan aspirasi mahasiswa serta memperjuangkan jawaban yang pantas untuk mereka?','2021-03-26 08:28:44','2021-03-26 08:28:44'),
(10,'Apakah anda suka bekerja di bidang keagamaan?','2021-03-26 08:29:02','2021-03-26 08:29:02'),
(11,'Apakah anda suka kegiatan sosial kemasyarakatan seperti baksos sebagai bentuk kepedulian terhadap masyarakat?','2021-03-26 08:29:52','2021-03-26 08:29:52'),
(12,'Apakah anda mampu menggiring mahasiswa baru untuk mengenal kampus dan organisasi kemahasiswaan intra?','2021-03-26 08:30:13','2021-03-26 08:30:13'),
(13,'Apakah anda mampu menjalin kerjasama dengan Organisasi lain dan mengayomi mereka?','2021-03-26 08:30:28','2021-07-08 07:50:29'),
(14,'Apakah anda mampu mengemas sebuah acara untuk memberikan bekal pengetahuan bagi mahasiswa?','2021-03-26 08:30:43','2021-07-08 07:51:07'),
(15,'Apakah anda siap membuat trobosan untuk membuat sebuah acara yang akan melibatkan potensi mahasiswa dalam berbagai bidang?','2021-03-26 08:31:02','2021-07-08 07:51:46'),
(16,'Apakah anda suka bidang fotografi atau videografi atau desain visual?','2021-03-26 08:31:20','2021-03-26 08:31:20');

/*Table structure for table `m_uraian` */

DROP TABLE IF EXISTS `m_uraian`;

CREATE TABLE `m_uraian` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `soalurn` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `m_uraian` */

insert  into `m_uraian`(`id`,`soalurn`,`created_at`,`updated_at`) values 
(1,'Jelaskan apa yang dimaksud dengan organisasi menurut anda pribadi!','2021-03-30 08:21:04','2021-03-30 08:35:33'),
(2,'Jelaskan menurut anda, bagaimanakah menciptakan organisasi yang efektif!','2021-03-30 08:21:39','2021-03-30 08:21:39'),
(3,'Jika kamu dihadapkan dengan dua pilihan, kamu memilih untuk memimpin atau dipimpin? Berikan alasanmu!','2021-03-30 08:22:30','2021-03-30 08:22:30'),
(4,'Apa trobosan anda mengenai sistem pembelajaran online atau daring?','2021-03-30 08:22:41','2021-03-30 08:22:41'),
(5,'Deskripsikan kelebihan dan kekuranganmu!','2021-04-05 05:22:01','2021-04-05 05:22:01');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2021_03_25_075303_create_pilihanganda',2),
(4,'2021_03_25_093426_create_kementrian',3),
(5,'2021_03_26_073948_create_soalspk',4),
(6,'2021_03_30_072100_create_uraian',5),
(7,'2021_04_04_041939_data_latih_line',6),
(10,'2021_04_14_081640_create_jawaban',7),
(11,'2021_04_18_065315_create_datauji',8),
(12,'2021_04_23_081726_create_nilai',9),
(13,'2021_05_04_073527_create_datahasil',10),
(14,'2021_05_19_065853_create_jawabanurn',11),
(15,'2021_05_25_074400_create_formulir',12),
(16,'2021_06_09_073219_create_pengaturan',13);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`level`,`email`,`email_verified_at`,`password`,`remember_token`,`created_at`,`updated_at`) values 
(2,'Pundi','Admin','pundi.8888@gmail.com',NULL,'$2y$10$TOyUAwZd8sFZSs7CB2cJS.86FxyxwnNObZjYbVJmwmIF0dm2X0fVa',NULL,'2021-03-09 09:14:57','2021-06-26 06:22:06'),
(3,'Muhamad Efendi Mulya','Peserta','efendi.mulya911@gmail.com',NULL,'$2y$10$.t7MxnwaGwlSzNro.YRJ7uf.vLhrGIuppSRH7YbLMWKQ7aGOrRIoi',NULL,'2021-06-19 12:35:55','2021-06-23 08:31:04'),
(4,'Suwandi','Asesor','suwandi.123@email.com',NULL,'$2y$10$qHiGsgHdu8.KvdgPgKAPJ.y2GNUFHtYUprRvf2FmJOEBiL.R0Qy2a',NULL,'2021-06-23 09:24:42','2021-06-23 09:24:42'),
(5,'Agus Salim','Asesor','agsal.123@gmail.com',NULL,'$2y$10$m7b9jGT7I3jnnlTOpnjwQ.1c4CC6WMoe70/RdvQrtl3G4kI8zSFNq',NULL,'2021-06-24 00:48:51','2021-06-24 01:45:25'),
(6,'Adi Waluyo','Peserta','adi.waluyo@email.com',NULL,'$2y$10$6ewCPjcq1LmCemSguIMlquIUVnYaFU5NV1s6b/d2JQxjiXVfqlQgm',NULL,'2021-06-29 08:04:47','2021-06-29 08:04:47');

/*Table structure for table `dthasil` */

DROP TABLE IF EXISTS `dthasil`;

/*!50001 DROP VIEW IF EXISTS `dthasil` */;
/*!50001 DROP TABLE IF EXISTS `dthasil` */;

/*!50001 CREATE TABLE  `dthasil`(
 `user_id` int(10) unsigned ,
 `lugri` double ,
 `sosma` double ,
 `dagri` double ,
 `kemhas` double ,
 `psdm` double ,
 `agama` double ,
 `kominfo` double 
)*/;

/*View structure for view dthasil */

/*!50001 DROP TABLE IF EXISTS `dthasil` */;
/*!50001 DROP VIEW IF EXISTS `dthasil` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dthasil` AS select `m_datahasil`.`user_id` AS `user_id`,sqrt(sum(`m_datahasil`.`lugri`)) AS `lugri`,sqrt(sum(`m_datahasil`.`sosma`)) AS `sosma`,sqrt(sum(`m_datahasil`.`dagri`)) AS `dagri`,sqrt(sum(`m_datahasil`.`kemhas`)) AS `kemhas`,sqrt(sum(`m_datahasil`.`psdm`)) AS `psdm`,sqrt(sum(`m_datahasil`.`agama`)) AS `agama`,sqrt(sum(`m_datahasil`.`kominfo`)) AS `kominfo` from `m_datahasil` group by `m_datahasil`.`user_id` */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
