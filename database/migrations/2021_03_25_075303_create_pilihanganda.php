<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePilihanganda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_pilihanganda', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jenispg',255);
            $table->text('soalpg');
            $table->integer('bobotpg');
            $table->string('a',255);
            $table->string('b',255);
            $table->string('c',255);
            $table->string('d',255);
            $table->string('e',255);
            $table->string('jawabanpg',255);
            $table->timestamps();

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_pilihanganda', function (Blueprint $table) {
            //
        });
    }
}
