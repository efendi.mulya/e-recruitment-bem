<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatahasil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_datahasil', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('spk_id')->unsigned();
            $table->integer('lugri');
            $table->integer('sosma');
            $table->integer('dagri');
            $table->integer('kemhas');
            $table->integer('psdm');
            $table->integer('agama');
            $table->integer('kominfo');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('spk_id')->references('id')->on('m_soalspk')->onDelete('restrict');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_datahasil', function (Blueprint $table) {
            //
        });
    }
}
