<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DataLatihLine extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_latih_line', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kementrian')->unsigned();
            $table->integer('soalspk')->unsigned();
            $table->integer('vektor');
            $table->timestamps();

            $table->foreign('kementrian')->references('id')->on('m_kementrian')->onDelete('cascade');
            $table->foreign('soalspk')->references('id')->on('m_soalspk')->onDelete('restrict');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_latih_line', function (Blueprint $table) {
            //
        });
    }
}
