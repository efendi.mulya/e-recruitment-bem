<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_formulir', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('nama',255);
            $table->string('jenkel',255);
            $table->string('prodi',255);
            $table->string('kelas',255);
            $table->string('no_wa',15);
            $table->text('riwayat_org');
            $table->string('kementrian1',255);
            $table->string('kementrian2',255);
            $table->text('motivasi');
            $table->text('kekurangan');
            $table->text('kelebihan');
            $table->string('foto_ktm',255);
            $table->string('foto_diri',255);
            $table->integer('status');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_formulir', function (Blueprint $table) {
            //
        });
    }
}
