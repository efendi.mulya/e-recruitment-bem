<?php

namespace App\Http\Controllers;

use App\Models\M_Soalspk;
use Illuminate\Http\Request;

class Soalspk_controller extends Controller
{
    public function index(){
        $title = 'Master Data Soal SPK';
        $data = M_Soalspk::orderBy('created_at','asc')->get();

        return view('soalspk.index',compact('title','data'));
    }

    public function add(){
        $title = 'Tambah Soal SPK';

        return view('soalspk.add',compact('title'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'soal_spk'=>'required'
        ]);

        $a['soal_spk'] = $request->soal_spk;
        $a['created_at'] = date('Y-m-d H:i:s');
        $a['updated_at'] = date('Y-m-d H:i:s');

        M_Soalspk::insert($a);

        \Session::flash('sukses','Data Berhasil Ditambahkan');

        return redirect('soalspk');
    }

    public function edit($id){
        $title = 'Edit Soal SPK';
        $dt = M_Soalspk::find($id);

        return view('soalspk.edit',compact('title', 'dt'));
    }

    public function update(Request $request,$id){
        $this->validate($request,[
            'soal_spk'=>'required'
        ]);

        $a['soal_spk'] = $request->soal_spk;
        $a['updated_at'] = date('Y-m-d H:i:s');

        M_Soalspk::where('id',$id)->update($a);

        \Session::flash('sukses','Data Berhasil Diperbarui');

        return redirect('soalspk');
    }

    public function delete($id){
        try{
            M_Soalspk::where('id',$id)->delete();

            \Session::flash('sukses','Data Berhasil Dihapus');
        }catch(Exception $e){
            \Session::flash('gagal',$e->getMessage());
        }

        return redirect('soalspk');
    }
}
