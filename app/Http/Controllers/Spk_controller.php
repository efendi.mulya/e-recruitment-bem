<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\M_datauji;
use App\Models\M_datahasil;
use Illuminate\Support\Facades\DB;

class Spk_controller extends Controller
{
    public function index(){
        $breadcrumb = 'Sistem Pendukung Keputusan';
        $title = 'Rekomendasi Kementrian';

        $vk = DB::select('SELECT b.id ,b.soal_spk,
                SUM(IF(kementrian=11, vektor,0)) AS lugri,
                SUM(IF(kementrian=12, vektor,0)) AS dagri,
                SUM(IF(kementrian=13, vektor,0)) AS sosma,
                SUM(IF(kementrian=14, vektor,0)) AS psdm,
                SUM(IF(kementrian=15, vektor,0)) AS kemhas,
                SUM(IF(kementrian=16, vektor,0)) AS agama,
                SUM(IF(kementrian=17, vektor,0)) AS kominfo
                FROM data_latih_line a, m_soalspk b
                WHERE b.id = a.soalspk
                GROUP BY b.id ,b.soal_spk;');

        return view('spk.index',compact('breadcrumb','title','vk'));
    }

    public function store(Request $request){

        $user = Auth()->user()->id;

        foreach ($request->spk_id as $item=>$spk_id) {
            $data = new M_datauji();
            $data->user_id = $request->user_id[$item];
            $data->spk_id = $spk_id;
            $data->jawab = $request->jawab[$item];
            //lugri
            // if ($request->jawab[$item] == 1 && $request->lugri[$item] == 1) {
            //     $data->lugri = '1';
            // } else { $data->lugri = '0'; }
            $data->lugri = $request->lugri[$item] - $request->jawab[$item];
            //sosma
            // if ($request->jawab[$item] == 1 && $request->sosma[$item] == 1) {
            //     $data->sosma = '1';
            // } else { $data->sosma = '0'; }
            $data->sosma = $request->sosma[$item] - $request->jawab[$item];
            //dagri
            // if ($request->jawab[$item] == 1 && $request->dagri[$item] == 1) {
            //     $data->dagri = '1';
            // } else { $data->dagri = '0'; }
            $data->dagri = $request->dagri[$item] - $request->jawab[$item];
            //kemhas
            // if ($request->jawab[$item] == 1 && $request->kemhas[$item] == 1) {
            //     $data->kemhas = '1';
            // } else { $data->kemhas = '0'; }
            $data->kemhas = $request->kemhas[$item] - $request->jawab[$item];
            //psdm
            // if ($request->jawab[$item] == 1 && $request->psdm[$item] == 1) {
            //     $data->psdm = '1';
            // } else { $data->psdm = '0'; }
            $data->psdm = $request->psdm[$item] - $request->jawab[$item];
            //agama
            // if ($request->jawab[$item] == 1 && $request->agama[$item] == 1) {
            //     $data->agama = '1';
            // } else { $data->agama = '0'; }
            $data->agama = $request->agama[$item] - $request->jawab[$item];
            //kominfo
            // if ($request->jawab[$item] == 1 && $request->kominfo[$item] == 1) {
            //     $data->kominfo = '1';
            // } else { $data->kominfo = '0'; }
            $data->kominfo = $request->kominfo[$item] - $request->jawab[$item];
            $data->save();

        }

        // $vk1 = [DB::select('SELECT SUM(IF(kementrian=11, vektor,0)) AS lugri
        //         FROM data_latih_line
        //         GROUP BY soalspk;')];
        // $uji1 = [M_datauji::select('lugri')->where('user_id',$user)->get()];

        // for( $i=0; $i < 15; $i++){
        //     $hasil1 = $vk1[$i] - $uji1[$i];
        // }

        // $hasil = new M_datahasil;
        //     $hasil->user_id = $request->user_id[$item];
        //     $hasil->lugri = $spk_id;
        //     $hasil->save();

        return redirect("spk/finish");
    }

    public function finish(){
        $breadcrumb = 'Sistem Pendukung Keputusan';
        $title = 'Rekomendasi Kementrian';
        $user = Auth()->user()->id;
        $uji = M_datauji::where('user_id',$user)->get();
        $vk = DB::select('SELECT b.id ,b.soal_spk,
                SUM(IF(kementrian=11, vektor,0)) AS lugri,
                SUM(IF(kementrian=12, vektor,0)) AS dagri,
                SUM(IF(kementrian=13, vektor,0)) AS sosma,
                SUM(IF(kementrian=14, vektor,0)) AS psdm,
                SUM(IF(kementrian=15, vektor,0)) AS kemhas,
                SUM(IF(kementrian=16, vektor,0)) AS agama,
                SUM(IF(kementrian=17, vektor,0)) AS kominfo
                FROM data_latih_line a, m_soalspk b
                WHERE b.id = a.soalspk
                GROUP BY b.id ,b.soal_spk;');

        return view('spk.finish',compact('breadcrumb','title','user','uji','vk'));
    }

    public function hitung(Request $request){

        foreach ($request->spk_id as $item=>$spk_id) {
            $data = new M_datahasil();
            $data->user_id = $request->user_id[$item];
            $data->spk_id = $spk_id;
            //lugri
            // $data->lugri = pow(($request->vklugri[$item] - $request->lugri[$item]),2);
            $data->lugri = pow($request->lugri[$item],2);
            //sosma
            // $data->sosma = pow(($request->vksosma[$item] - $request->sosma[$item]),2);
            $data->sosma = pow($request->sosma[$item],2);
            //dagri
            // $data->dagri = pow(($request->vkdagri[$item] - $request->dagri[$item]),2);
            $data->dagri = pow($request->dagri[$item],2);
            //kemhas
            // $data->kemhas = pow(($request->vkkemhas[$item] - $request->kemhas[$item]),2);
            $data->kemhas = pow($request->kemhas[$item],2);
            //psdm
            // $data->psdm = pow(($request->vkpsdm[$item] - $request->psdm[$item]),2);
            $data->psdm = pow($request->psdm[$item],2);
            //agama
            // $data->agama = pow(($request->vkagama[$item] - $request->agama[$item]),2);
            $data->agama = pow($request->agama[$item],2);
            //kominfo
            // $data->kominfo = pow(($request->vkkominfo[$item] - $request->kominfo[$item]),2);
            $data->kominfo = pow($request->kominfo[$item],2);
            $data->save();
        }

        return redirect("spk/hasil");
    }

    public function hasil(){
        $breadcrumb = 'Sistem Pendukung Keputusan';
        $title = 'Rekomendasi Kementrian';
        $user = Auth()->user()->id;
        $hasil = DB::select("SELECT t.user_id, c.kementrian,
                                CASE c.kementrian
                                WHEN 'lugri' THEN lugri
                                WHEN 'sosma' THEN sosma
                                WHEN 'dagri' THEN dagri
                                WHEN 'kemhas' THEN kemhas
                                WHEN 'psdm' THEN psdm
                                WHEN 'agama' THEN agama
                                WHEN 'kominfo' THEN kominfo
                                END AS DATA
                            FROM dthasil t
                            CROSS JOIN
                            (SELECT 'lugri' AS kementrian
                                UNION ALL SELECT 'sosma'
                                UNION ALL SELECT 'dagri'
                                UNION ALL SELECT 'kemhas'
                                UNION ALL SELECT 'psdm'
                                UNION ALL SELECT 'agama'
                                UNION ALL SELECT 'kominfo') c
                            WHERE user_id = '$user'
                            ORDER BY DATA ASC
                            LIMIT 1;");

        return view('spk.hasil',compact('breadcrumb','title','user','hasil'));
    }

    public function delete($id){
        M_datahasil::where('user_id',$id)->delete();

        // M_datahasil::where('user_id',$id)->update([
        //     'lugri' => null,
        //     'sosma' => null,
        //     'dagri' => null,
        //     'kemhas' => null,
        //     'psdm' => null,
        //     'agama' => null,
        //     'kominfo' => null,
        //     'updated_at' => date('Y-m-d H:i:s')
        // ]);

        return redirect('spk');
    }

    public function pilihulang(Request $request){
        $user = Auth()->user()->id;
        foreach ($request->spk_id as $item=>$spk) {
            $data['jawab'] = $request->jawab[$item];
            $data['updated_at'] = date('Y-m-d H:i:s');
            //lugri
            // if ($request->jawab[$item] == 1 && $request->lugri[$item] == 1) {
            //     $data['lugri'] = '1';
            // } else { $data['lugri'] = '0'; }
            $data['lugri'] = $request->lugri[$item] - $request->jawab[$item];
                //sosma
            // if ($request->jawab[$item] == 1 && $request->sosma[$item] == 1) {
            //     $data['sosma'] = '1';
            // } else { $data['sosma'] = '0'; }
            $data['sosma'] = $request->sosma[$item] - $request->jawab[$item];
            //dagri
            // if ($request->jawab[$item] == 1 && $request->dagri[$item] == 1) {
            //     $data['dagri'] = '1';
            // } else { $data['dagri'] = '0'; }
            $data['dagri'] = $request->dagri[$item] - $request->jawab[$item];
            //kemhas
            // if ($request->jawab[$item] == 1 && $request->kemhas[$item] == 1) {
            //     $data['kemhas'] = '1';
            // } else { $data['kemhas'] = '0'; }
            $data['kemhas'] = $request->kemhas[$item] - $request->jawab[$item];
            //psdm
            // if ($request->jawab[$item] == 1 && $request->psdm[$item] == 1) {
            //     $data['psdm'] = '1';
            // } else { $data['psdm'] = '0'; }
            $data['psdm'] = $request->psdm[$item] - $request->jawab[$item];
            //agama
            // if ($request->jawab[$item] == 1 && $request->agama[$item] == 1) {
            //     $data['agama'] = '1';
            // } else { $data['agama'] = '0'; }
            $data['agama'] = $request->agama[$item] - $request->jawab[$item];
            //kominfo
            // if ($request->jawab[$item] == 1 && $request->kominfo[$item] == 1) {
            //     $data['kominfo'] = '1';
            // } else { $data['kominfo'] = '0'; }
            $data['kominfo'] = $request->kominfo[$item] - $request->jawab[$item];

            M_datauji::where('user_id',$user)->where('spk_id',$spk)->update($data);

        }

        return redirect('spk/finish');
    }
}
