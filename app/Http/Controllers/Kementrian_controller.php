<?php

namespace App\Http\Controllers;

use App\Models\Data_latih_line;
use App\Models\M_kementrian;
use App\Models\M_Soalspk;
use Illuminate\Http\Request;

class Kementrian_controller extends Controller
{
    public function index(){
        $title = 'Master Data Kementrian';
        $data = M_kementrian::orderBy('nama_kementrian','asc')->get();
        $count = M_kementrian::count('id');

        return view('kementrian.index',compact('title','data','count'));
    }

    public function detail($id){
        $title = 'Detail Kementrian';
        $dt = M_kementrian::find($id);
        // $soal = M_Soalspk::orderBy('id','asc')->get();
        // $vk = Data_latih_line::find($id);

        return view('kementrian.detail',compact('title','dt'));
    }

    public function add(){
        $title = 'Tambah Kementrian';
        $soal = M_Soalspk::orderBy('id','asc')->get();
        $count = M_kementrian::count('id');

        return view('kementrian.add',compact('title','soal','count'));
    }

    public function store(Request $request){
        try{
            $this->validate($request,[
                'nama_kementrian'=>'required',
                'tupoksi'=>'required'
            ]);

            $soal = $request->soal;
            $vektor = $request->vektor;
            $nama_kementrian = $request->nama_kementrian;
            $tupoksi = $request->tupoksi;

            $id_kementrian = M_kementrian::insertGetId([
                'nama_kementrian' => $nama_kementrian,
                'tupoksi' => $tupoksi,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            foreach($vektor as $e=>$vk){
                // $dt_soal = M_Soalspk::where('id',$soal[$e])->first();

                Data_latih_line::insert([
                    'kementrian' => $id_kementrian,
                    'soalspk' => $soal[$e],
                    'vektor' => $vk,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
            \Session::flash('sukses', 'Data Berhasil Disimpan');
        }
        catch(\Exception $e){
            \Session::flash('gagal',$e->getMessage());
        }

        return redirect('kementrian');
    }

    public function edit($id){
        $title = 'Edit Kementrian';
        $dt = M_kementrian::find($id);
        $soal = M_Soalspk::orderBy('id','asc')->get();

        return view('kementrian.edit',compact('title', 'dt','soal'));
    }

    public function update(Request $request,$id){
        try{
            $this->validate($request,[
                'nama_kementrian'=>'required',
                'tupoksi'=>'required'
            ]);

            $id_line = $request->id_line;
            $vektor = $request->vektor;
            $nama_kementrian = $request->nama_kementrian;
            $tupoksi = $request->tupoksi;

            M_kementrian::where('id',$id)->update([
                'nama_kementrian' => $nama_kementrian,
                'tupoksi' => $tupoksi,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            foreach($vektor as $e=>$vk){
                $data['vektor'] = $vk;
                $data['updated_at'] = date('Y-m-d H:i:s');
                $line = $id_line[$e];

                Data_latih_line::where('id',$line)->update($data);
            }
            \Session::flash('sukses', 'Data Berhasil Diperbarui');
        }
        catch(\Exception $e){
            \Session::flash('gagal',$e->getMessage());
        }

        return redirect('kementrian');
    }

    public function delete($id){
        try{
            Data_latih_line::where('kementrian',$id)->delete();
            M_kementrian::where('id',$id)->delete();

            \Session::flash('sukses','Data Berhasil Dihapus');
        }catch(Exception $e){
            \Session::flash('gagal',$e->getMessage());
        }

        return redirect('kementrian');
    }

    public function view(){
        $title = 'Tabel Data Latih';

        return view('kementrian.coba',compact('title'));
    }
}
