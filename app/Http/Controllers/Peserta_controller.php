<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Exception;
use Illuminate\Support\Facades\Hash;

class Peserta_controller extends Controller
{
    public function index(){
        $title = 'Master Data Peserta';
        $data = User::where(['level'=>'Peserta'])->orderBy('name','asc')->get();

        return view('peserta.index',compact('title','data'));
    }

    public function edit($id){
        $title = 'Edit Peserta';
        $dt = User::find($id);

        return view('peserta.edit',compact('title', 'dt'));
    }

    public function update(Request $request, $id){
        $this->validate($request,[
            'name'=>['required', 'string', 'max:255'],
            'email'=>['required','string','email','max:255'],
            'password'=>['required', 'string', 'min:8', 'confirmed']
        ]);

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['level'] = 'Peserta';
        $data['password'] = Hash::make($request['password']);
        $data['updated_at'] = date('Y-m-d H:i:s');

        User::where('id',$id)->update($data);

        \Session::flash('sukses', 'Data Berhasil Diperbarui');

        return redirect('peserta');

    }

    public function delete($id){
        try{
            User::where('id',$id)->delete();

            \Session::flash('sukses','Data Berhasil Dihapus');
        }catch(Exception $e){
            \Session::flash('gagal',$e->getMessage());
        }

        return redirect('peserta');
    }

    public function profil(){
        $breadcrumb = 'Edit Profil';
        $title = 'Peserta';
        $user = Auth()->user()->id;
        $data = User::where('id',$user)->get();

        return view('peserta.profil',compact('breadcrumb','title', 'data'));
    }

    public function update_profil(Request $request){
        $this->validate($request,[
            'name'=>['required', 'string', 'max:255'],
            'email'=>['required','string','email','max:255'],
            'password'=>['required', 'string', 'min:8', 'confirmed']
        ]);

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['level'] = 'Peserta';
        $data['password'] = Hash::make($request['password']);
        $data['updated_at'] = date('Y-m-d H:i:s');

        $user = Auth()->user()->id;
        User::where('id',$user)->update($data);

        return redirect('home');

    }
}
