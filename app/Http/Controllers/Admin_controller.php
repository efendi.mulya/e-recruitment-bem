<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class Admin_controller extends Controller
{
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'name' => ['required', 'string', 'max:255'],
    //         'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
    //         'password' => ['required', 'string', 'min:8', 'confirmed'],
    //     ]);
    // }

    public function index(){
        $title = 'Master Data Admin';
        $data = User::where(['level'=>'Admin'])->orderBy('name','asc')->get();

        return view('admin.index',compact('title','data'));
    }

    public function add(){
        $title = 'Tambah Admin';

        return view('admin.add',compact('title'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'name'=>['required', 'string', 'max:255'],
            'email'=>['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'=>['required', 'string', 'min:8', 'confirmed']
        ]);

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['level'] = 'Admin';
        $data['password'] = Hash::make($request['password']);
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');

        User::insert($data);

        \Session::flash('sukses', 'Data Berhasil Ditambahkan');

        return redirect('admin');

    }

    public function edit($id){
        $title = 'Edit Admin';
        $dt = User::find($id);

        return view('admin.edit',compact('title', 'dt'));
    }

    public function update(Request $request, $id){
        $this->validate($request,[
            'name'=>['required', 'string', 'max:255'],
            'email'=>['required','string','email','max:255'],
            'password'=>['required', 'string', 'min:8', 'confirmed']
        ]);

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['level'] = 'Admin';
        $data['password'] = Hash::make($request['password']);
        $data['updated_at'] = date('Y-m-d H:i:s');

        User::where('id',$id)->update($data);

        \Session::flash('sukses', 'Data Berhasil Diperbarui');

        return redirect('admin');

    }

    public function delete($id){
        try{
            User::where('id',$id)->delete();

            \Session::flash('sukses','Data Berhasil Dihapus');
        }catch(Exception $e){
            \Session::flash('gagal',$e->getMessage());
        }

        return redirect('admin');
    }

    public function profil(){
        $title = 'Edit Profil';
        $user = Auth()->user()->id;
        $data = User::where('id',$user)->get();

        return view('admin.profil',compact('title', 'data'));
    }

    public function update_profil(Request $request){
        $this->validate($request,[
            'name'=>['required', 'string', 'max:255'],
            'email'=>['required','string','email','max:255'],
            'password'=>['required', 'string', 'min:8', 'confirmed']
        ]);

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['level'] = 'Admin';
        $data['password'] = Hash::make($request['password']);
        $data['updated_at'] = date('Y-m-d H:i:s');

        $user = Auth()->user()->id;
        User::where('id',$user)->update($data);

        \Session::flash('sukses', 'Data Berhasil Diperbarui');

        return redirect('home');

    }
}
