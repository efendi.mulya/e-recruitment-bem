<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\M_kementrian;
use App\Models\M_formulir;
use App\Models\M_pengaturan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class Formulir_controller extends Controller
{
    public function index(){
        $breadcrumb = 'Formulir Pendaftaran';
        $title = 'Anggota Baru BEM PSDKU';
        $user = Auth()->user()->id;
        $kementrian = M_kementrian::orderBy('id','asc')->get();
        $formulir = M_formulir::where('user_id',$user)->get();
        $pengaturan = M_pengaturan::where('menu','formulir')->get();
        $hasil = DB::select("SELECT t.user_id, c.kementrian,
                                CASE c.kementrian
                                WHEN 'lugri' THEN lugri
                                WHEN 'sosma' THEN sosma
                                WHEN 'dagri' THEN dagri
                                WHEN 'kemhas' THEN kemhas
                                WHEN 'psdm' THEN psdm
                                WHEN 'agama' THEN agama
                                WHEN 'kominfo' THEN kominfo
                                END AS DATA
                            FROM dthasil t
                            CROSS JOIN
                            (SELECT 'lugri' AS kementrian
                                UNION ALL SELECT 'sosma'
                                UNION ALL SELECT 'dagri'
                                UNION ALL SELECT 'kemhas'
                                UNION ALL SELECT 'psdm'
                                UNION ALL SELECT 'agama'
                                UNION ALL SELECT 'kominfo') c
                            WHERE user_id = '$user'
                            ORDER BY DATA ASC
                            LIMIT 1;");

        return view('formulir.index',compact('breadcrumb','title','kementrian','hasil','formulir','pengaturan'));
    }

    public function pengaturan(){
        try {
            $pengaturan = M_pengaturan::where('menu','formulir')->get();
            if ($pengaturan[0]->status == 1) {
                M_pengaturan::where('menu','formulir')->update([
                    'status'=>0,
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
                \Session::flash('sukses','Menu Formulir Dimatikan');
            }
            else {
                M_pengaturan::where('menu','formulir')->update([
                    'status'=>1,
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
                \Session::flash('sukses','Menu Formulir Dinyalakan');
            }
        } catch (\Exception $e) {
            \Session::flash('gagal',$e->getMessage());
        }
        return redirect('home');
    }

    public function store(Request $request){
        $user = Auth()->user()->id;

        $this->validate($request,[
            // 'nim'=>'required',
            'nim'=>['required', 'min:10', 'max:12', 'unique:m_formulir'],
            'nama'=>'required',
            'jenkel'=>'required',
            'prodi'=>'required',
            'kelas'=>'required',
            'no_wa'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:11',
            'riwayat_org'=>'required',
            'kementrian1'=>'required',
            'kementrian2'=>'required',
            'motivasi'=>'required',
            'kekurangan'=>'required',
            'kelebihan'=>'required',
            'foto_diri' => 'required',
            'foto_ktm' => 'required'
            // 'foto_diri' => 'nullable|file|image|mimes:jpeg,png,jpg',
            // 'foto_ktm' => 'nullable|file|image|mimes:jpeg,png,jpg'
        ]);

        $foto_diri = $request->file('foto_diri');
        if($foto_diri){
            $nama_file = $request->nim."_PAS_".time().'.'.$foto_diri->extension();
            $foto_diri->move('assets/images/formulir',$nama_file);
            $data['foto_diri'] = 'assets/images/formulir/'.$nama_file;
        }

        $foto_ktm = $request->file('foto_ktm');
        if($foto_ktm){
            $nama_file = $request->nim."_KTM_".time().'.'.$foto_ktm->extension();
            $foto_ktm->move('assets/images/formulir',$nama_file);
            $data['foto_ktm'] = 'assets/images/formulir/'.$nama_file;
        }

        $data['user_id'] = $user;
        $data['nim'] = $request->nim;
        $data['nama'] = $request->nama;
        $data['jenkel'] = $request->jenkel;
        $data['prodi'] = $request->prodi;
        $data['kelas'] = $request->kelas;
        $data['no_wa'] = $request->no_wa;
        $data['riwayat_org'] = $request->riwayat_org;
        $data['kementrian1'] = $request->kementrian1;
        $data['kementrian2'] = $request->kementrian2;
        $data['motivasi'] = $request->motivasi;
        $data['kekurangan'] = $request->kekurangan;
        $data['kelebihan'] = $request->kelebihan;
        $data['status'] = 0;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');

        M_formulir::insert($data);

        return redirect('formulir');
    }

    public function edit(){
        $breadcrumb = 'Edit Formulir Pendaftaran';
        $title = 'Anggota Baru BEM PSDKU';
        $user = Auth()->user()->id;
        $kementrian = M_kementrian::orderBy('id','asc')->get();
        $formulir = M_formulir::where('user_id',$user)->get();
        $hasil = DB::select("SELECT t.user_id, c.kementrian,
                                CASE c.kementrian
                                WHEN 'lugri' THEN lugri
                                WHEN 'sosma' THEN sosma
                                WHEN 'dagri' THEN dagri
                                WHEN 'kemhas' THEN kemhas
                                WHEN 'psdm' THEN psdm
                                WHEN 'agama' THEN agama
                                WHEN 'kominfo' THEN kominfo
                                END AS DATA
                            FROM dthasil t
                            CROSS JOIN
                            (SELECT 'lugri' AS kementrian
                                UNION ALL SELECT 'sosma'
                                UNION ALL SELECT 'dagri'
                                UNION ALL SELECT 'kemhas'
                                UNION ALL SELECT 'psdm'
                                UNION ALL SELECT 'agama'
                                UNION ALL SELECT 'kominfo') c
                            WHERE user_id = '$user'
                            ORDER BY DATA ASC
                            LIMIT 1;");

        return view('formulir.edit',compact('breadcrumb','title','kementrian','hasil','formulir'));
    }

    public function update(Request $request){
        $user = Auth()->user()->id;
        $formulir = M_formulir::where('user_id',$user)->get();
        $foto = $formulir[0]->foto_diri;
        $ktm = $formulir[0]->foto_ktm;

        $this->validate($request,[
            'nim'=>'required',
            'nama'=>'required',
            'jenkel'=>'required',
            'prodi'=>'required',
            'kelas'=>'required',
            'no_wa'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:11',
            'riwayat_org'=>'required',
            'kementrian1'=>'required',
            'kementrian2'=>'required',
            'motivasi'=>'required',
            'kekurangan'=>'required',
            'kelebihan'=>'required'
        ]);

        if ($request->foto_diri != "") {
            File::delete($foto);
            $foto_diri = $request->file('foto_diri');
            if($foto_diri){
                $nama_file = $request->nim."_PAS_".time().'.'.$foto_diri->extension();
                $foto_diri->move('assets/images/formulir',$nama_file);
                $data['foto_diri'] = 'assets/images/formulir/'.$nama_file;
            }
        } else {}

        if ($request->foto_ktm != "") {
            File::delete($ktm);
            $foto_ktm = $request->file('foto_ktm');
            if($foto_ktm){
                $nama_file = $request->nim."_KTM_".time().'.'.$foto_ktm->extension();
                $foto_ktm->move('assets/images/formulir',$nama_file);
                $data['foto_ktm'] = 'assets/images/formulir/'.$nama_file;
            }
        } else {}

        $data['nim'] = $request->nim;
        $data['nama'] = $request->nama;
        $data['jenkel'] = $request->jenkel;
        $data['prodi'] = $request->prodi;
        $data['kelas'] = $request->kelas;
        $data['no_wa'] = $request->no_wa;
        $data['riwayat_org'] = $request->riwayat_org;
        $data['kementrian1'] = $request->kementrian1;
        $data['kementrian2'] = $request->kementrian2;
        $data['motivasi'] = $request->motivasi;
        $data['kekurangan'] = $request->kekurangan;
        $data['kelebihan'] = $request->kelebihan;
        $data['updated_at'] = date('Y-m-d H:i:s');

        // M_formulir::insert($data);
        M_formulir::where('user_id',$user)->update($data);

        return redirect('formulir');
    }

    public function list(){
        $title = 'Daftar Formulir Pendaftaran Peserta';
        $data = M_formulir::orderBy('status','asc')->get();
        $count = M_formulir::count('id');

        return view('penerimaan.index',compact('title','data','count'));
    }

    public function penerimaan($id){
        $title = 'Penerimaan Formulir Pendaftaran Peserta';
        $data = M_formulir::where('user_id',$id)->get();

        return view('penerimaan.penerimaan',compact('title','data'));
    }

    public function approved($id){
        try {
            // $data = M_formulir::find($id);

            M_formulir::where('user_id',$id)->update([
                'status'=>1
            ]);
            \Session::flash('sukses','Formulir berhasil diterima');
        } catch (\Exception $e) {
            \Session::flash('gagal',$e->getMessage());
        }
        return redirect('penerimaan');
    }
}
