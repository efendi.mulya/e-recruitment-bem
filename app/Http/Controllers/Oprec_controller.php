<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Oprec_controller extends Controller
{
    public function index(){
        $title = 'E-Recruitment BEM PSDKU Polinema';

        return view('oprec.index',compact('title'));
    }
}
