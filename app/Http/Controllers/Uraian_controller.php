<?php

namespace App\Http\Controllers;

use App\Models\M_uraian;
use Illuminate\Http\Request;

class Uraian_controller extends Controller
{
    public function index(){
        $title = 'Master Data Soal Uraian';
        $data = M_uraian::orderBy('id','asc')->get();
        $count = M_uraian::count('id');

        return view('uraian.index',compact('title','data','count'));
    }

    public function add(){
        $title = 'Tambah Soal Uraian';
        $count = M_uraian::count('id');

        return view('uraian.add',compact('title','count'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'soalurn'=>'required'
        ]);

        $a['soalurn'] = $request->soalurn;
        $a['created_at'] = date('Y-m-d H:i:s');
        $a['updated_at'] = date('Y-m-d H:i:s');
        // dd($data);

        M_uraian::insert($a);

        \Session::flash('sukses','Data berhasil ditambah');

        return redirect('uraian');
    }

    public function edit($id){
        $title = 'Edit Soal Uraian';
        $dt = M_uraian::find($id);

        return view('uraian.edit',compact('title', 'dt'));
    }

    public function update(Request $request,$id){
        $this->validate($request,[
            'soalurn'=>'required'
        ]);

        $a['soalurn'] = $request->soalurn;
        $a['updated_at'] = date('Y-m-d H:i:s');

        M_uraian::where('id',$id)->update($a);

        \Session::flash('sukses','Data berhasil diperbarui');

        return redirect('uraian');
    }
}
