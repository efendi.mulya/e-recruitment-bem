<?php

namespace App\Http\Controllers;

use App\Models\M_formulir;
use App\Models\M_jawabanpg;
use App\Models\M_jawabanurn;
use App\Models\M_nilai;
use App\Models\M_pengaturan;
use App\Models\M_pilihanganda;
use App\Models\M_uraian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Ujian_controller extends Controller
{
    public function index(){
        $breadcrumb = 'Ujian';
        $title = 'Seleksi Anggota Baru';
        $user = Auth()->user()->id;
        $pengaturan = M_pengaturan::where('menu','ujian')->get();
        $formulir = M_formulir::where('user_id',$user)->get();

        return view('ujian.index',compact('breadcrumb','title','pengaturan','formulir'));
    }

    public function pengaturan(){
        try {
            $pengaturan = M_pengaturan::where('menu','ujian')->get();
            if ($pengaturan[0]->status == 1) {
                M_pengaturan::where('menu','ujian')->update([
                    'status'=>0,
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
                \Session::flash('sukses','Menu Ujian Dimatikan');
            }
            else {
                M_pengaturan::where('menu','ujian')->update([
                    'status'=>1,
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
                \Session::flash('sukses','Menu Ujian Dinyalakan');
            }
        } catch (\Exception $e) {
            \Session::flash('gagal',$e->getMessage());
        }
        return redirect('home');
    }

    public function pilihanganda(){
        $breadcrumb = 'Ujian';
        $title = 'Pilihan Ganda';
        $user = Auth()->user()->id;
        $pengaturan = M_pengaturan::where('menu','ujian')->get();
        $formulir = M_formulir::where('user_id',$user)->get();
        $data1 = M_pilihanganda::where('jenispg','Keorganisasian')->inRandomOrder()->limit(5)->get();
        $data2 = M_pilihanganda::where('jenispg','Matematika')->inRandomOrder()->limit(5)->get();
        $data3 = M_pilihanganda::where('jenispg','Psikotes')->inRandomOrder()->limit(5)->get();
        $data4 = M_pilihanganda::where('jenispg','Kewarganegaraan')->inRandomOrder()->limit(5)->get();
        $data5 = M_pilihanganda::where('jenispg','Wawasan Kampus')->inRandomOrder()->limit(5)->get();

        return view('ujian.pilihanganda',compact('breadcrumb','title','pengaturan','formulir','data1','data2','data3','data4','data5'));
    }

    public function store(Request $request){
        // $data = $request->all();
        // dd($data);
            foreach ($request->soal_id as $item=>$soal_id) {
                $data = new M_jawabanpg();
                $data->user_id = $request->user_id[$item];
                $data->soal_id = $soal_id;
                $data->jawab = $request->jawab[$item];
                $data->kunci = $request->kunci[$item];
                if ($request->jawab[$item] == $request->kunci[$item]) {
                    $data->skor = '4';
                } else { $data->skor = '0'; }
                $data->save();
            }

            $user = Auth()->user()->id;
            // $a = Auth::user()->id;
            $skor = M_jawabanpg::where('user_id',$user)->sum('skor');
            $nilai = new M_nilai();
            $nilai->user_id = $user;
            $nilai->nilaipg = $skor;
            $nilai->nilaiurn = null;
            $nilai->save();

        return redirect('ujian/uraian');
    }

    public function uraian(){
        $breadcrumb = 'Ujian';
        $title = 'Uraian';
        $user = Auth()->user()->id;
        $pengaturan = M_pengaturan::where('menu','ujian')->get();
        $formulir = M_formulir::where('user_id',$user)->get();
        $data = M_uraian::orderBy('id','asc')->get();

        return view('ujian.uraian',compact('breadcrumb','title','pengaturan','formulir','data'));
    }

    public function save(Request $request){
            foreach ($request->soal_id as $item=>$soal_id) {
                $data = new M_jawabanurn();
                $data->user_id = $request->user_id[$item];
                $data->soal_id = $soal_id;
                $data->jawab = $request->jawab[$item];
                $data->skor = null;
                $data->save();
            }

        return redirect('nilai/');
    }
}
