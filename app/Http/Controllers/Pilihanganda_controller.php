<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\M_pilihanganda;

class Pilihanganda_controller extends Controller
{
    public function index(){
        $title = 'Master Data Soal Pilihan Ganda';
        $data = M_pilihanganda::orderBy('jenispg','asc')->get();

        return view('pilihanganda.index',compact('title','data'));
    }

    public function detail($id){
        $title = 'Detail Soal Pilihan Ganda';
        $dt = M_pilihanganda::find($id);

        return view('pilihanganda.detail',compact('title','dt'));
    }

    public function add(){
        $title = 'Tambah Soal Pilihan Ganda';

        return view('pilihanganda.add',compact('title'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'jenispg'=>'required',
            'soalpg'=>'required',
            // 'bobotpg'=>'required',
            'a'=>'required',
            'b'=>'required',
            'c'=>'required',
            'd'=>'required',
            'e'=>'required',
            'jawabanpg'=>'required'
        ]);

        $a['jenispg'] = $request->jenispg;
        $a['soalpg'] = $request->soalpg;
        $a['bobotpg'] = '4';
        $a['a'] = $request->a;
        $a['b'] = $request->b;
        $a['c'] = $request->c;
        $a['d'] = $request->d;
        $a['e'] = $request->e;
        $a['jawabanpg'] = $request->jawabanpg;
        $a['created_at'] = date('Y-m-d H:i:s');
        $a['updated_at'] = date('Y-m-d H:i:s');
        // dd($data);

        M_pilihanganda::insert($a);

        \Session::flash('sukses','Data berhasil ditambah');

        return redirect('pilihanganda');
    }

    public function edit($id){
        $title = 'Edit Soal Pilihan Ganda';
        $dt = M_pilihanganda::find($id);

        return view('pilihanganda.edit',compact('title', 'dt'));
    }

    public function update(Request $request,$id){
        $this->validate($request,[
            'jenispg'=>'required',
            'soalpg'=>'required',
            // 'bobotpg'=>'required',
            'a'=>'required',
            'b'=>'required',
            'c'=>'required',
            'd'=>'required',
            'e'=>'required',
            'jawabanpg'=>'required'
        ]);

        $a['jenispg'] = $request->jenispg;
        $a['soalpg'] = $request->soalpg;
        $a['bobotpg'] = '4';
        $a['a'] = $request->a;
        $a['b'] = $request->b;
        $a['c'] = $request->c;
        $a['d'] = $request->d;
        $a['e'] = $request->e;
        $a['jawabanpg'] = $request->jawabanpg;
        $a['updated_at'] = date('Y-m-d H:i:s');

        M_pilihanganda::where('id',$id)->update($a);

        \Session::flash('sukses','Data berhasil diperbarui');

        return redirect('pilihanganda');
    }

    public function delete($id){
        try{
            M_pilihanganda::where('id',$id)->delete();

            \Session::flash('sukses','Data Berhasil Dihapus');
        }catch(Exception $e){
            \Session::flash('gagal',$e->getMessage());
        }

        return redirect('pilihanganda');
    }
}
