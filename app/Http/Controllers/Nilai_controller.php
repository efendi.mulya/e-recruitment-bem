<?php

namespace App\Http\Controllers;

use App\Models\M_jawabanurn;
use App\Models\M_nilai;
use App\Models\M_users;

use Illuminate\Http\Request;

class Nilai_controller extends Controller
{
    public function index(){
        $breadcrumb = 'Nilai';
        $title = 'Hasil Ujian';
        $user = Auth()->user()->id;
        $data = M_nilai::where('user_id',$user)->get();

        return view('nilai.index',compact('breadcrumb','title','data'));
    }

    public function list(){
        $title = 'Daftar Nilai Ujian Peserta';
        $data = M_nilai::orderBy('nilaiurn','asc')->get();

        return view('penilaian.index',compact('title','data'));
    }

    public function penilaian($id){
        $title = 'Penilaian Jawaban Uraian Peserta';
        $dtid = M_users::find($id);
        $data = M_jawabanurn::where('user_id',$id)->get();

        return view('penilaian.penilaian',compact('title','data','dtid'));
    }

    public function save(Request $request){
        try {
            $this->validate($request,[
                'skor'=>'required|min:0|max:20'
            ]);

            foreach ($request->soal_id as $item=>$soal) {
                $cek = M_jawabanurn::where('user_id',$request->user_id[$item])->
                where('soal_id',$soal)->first();
                if ($cek->n1 == null) {
                    $data['a1'] = Auth()->user()->id;
                    $data['n1'] = $request->skor[$item];
                    $data['updated_at'] = date('Y-m-d H:i:s');
                }
                elseif ($cek->n1 != null) {
                    $data['a2'] = Auth()->user()->id;
                    $data['n2'] = $request->skor[$item];
                    $data['skor'] = ($request->skor[$item] + $request->nilai[$item])/2;
                    $data['updated_at'] = date('Y-m-d H:i:s');
                }

                M_jawabanurn::where('user_id',$request->user_id[$item])->where('soal_id',$soal)->update($data);
            }

            $total1 = M_jawabanurn::where('user_id',$request->user_id)->sum('n1');
            $total2 = M_jawabanurn::where('user_id',$request->user_id)->sum('n2');
            $skor = M_jawabanurn::where('user_id',$request->user_id)->sum('skor');
            if ($skor != 0) {
                $nilai['nilaiurn'] = $skor;
                $nilai['a2'] = Auth()->user()->id;
                $nilai['total2'] = $total2;
                $nilai['updated_at'] = date('Y-m-d H:i:s');
                M_nilai::where('user_id',$request->user_id)->update($nilai);
            } else {
                $nilai['a1'] = Auth()->user()->id;
                $nilai['total1'] = $total1;
                M_nilai::where('user_id',$request->user_id)->update($nilai);
            }

            \Session::flash('sukses', 'Data Berhasil Diperbarui');
        } catch (\Exception $e) {
            \Session::flash('gagal',$e->getMessage());
        }

        return redirect('penilaian');
    }

}
