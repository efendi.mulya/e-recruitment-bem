<?php

namespace App\Http\Controllers;

use App\Models\M_formulir;
use App\Models\M_nilai;
use App\User;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth()->user()->level == "Peserta") {
            $breadcrumb = 'Dashboard';
            $title = 'Peserta';
            return view('home',compact('title','breadcrumb'));
        }
        else {
            $title = 'Dashboard '.Auth()->user()->level;
            $peserta = User::where(['level'=>'Peserta'])->orderBy('name','asc')->get();
            $formulir = M_formulir::orderBy('id','asc')->get();
            $nilai = M_nilai::orderBy('id','asc')->get();
            return view('home',compact('title','peserta','formulir','nilai'));
        }
    }
}
