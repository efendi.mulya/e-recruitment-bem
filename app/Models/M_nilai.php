<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_nilai extends Model
{
    protected $table = 'm_nilai';

    public function namas(){
        return $this->belongsTo('App\Models\M_users','user_id');
    }
}
