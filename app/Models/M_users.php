<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_users extends Model
{
    protected $table = 'users';

    public function jawabanpgs(){
        return $this->hasMany('App\Models\M_jawabanpg','user_id');
    }

    public function jawabanurns(){
        return $this->hasMany('App\Models\M_jawabanurn','user_id');
    }

}
