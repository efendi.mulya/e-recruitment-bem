<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_jawabanurn extends Model
{
    protected $table = 'm_jawabanurn';

    public function soals(){
        return $this->belongsTo('App\Models\M_uraian','soal_id');
    }
}
