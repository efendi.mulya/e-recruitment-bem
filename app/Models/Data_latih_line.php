<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Data_latih_line extends Model
{
    protected $table = 'data_latih_line';

    public function soals(){
        return $this->belongsTo('App\Models\M_Soalspk','soalspk');
    }
}
