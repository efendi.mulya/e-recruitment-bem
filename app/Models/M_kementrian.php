<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_kementrian extends Model
{
    protected $table = 'm_kementrian';

    public function lines(){
        return $this->hasMany('App\Models\Data_latih_line','kementrian');
    }
}
