<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_jawabanpg extends Model
{
    protected $table = 'm_jawabanpg';

    public function soals(){
        return $this->belongsTo('App\Models\M_pilihanganda','soal_id');
    }
}
