<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('/home');
});

Route::get('/keluar', function(){
    \Auth::logout();

    return redirect('/login');
});

Route::group(['middleware' => ['auth','ceklevel:Admin']], function(){

    //Admin
    Route::get('/admin', 'Admin_controller@index');

    Route::get('/admin/add','Admin_controller@add');
    Route::post('/admin/add','Admin_controller@store');

    Route::get('/admin/{id}','Admin_controller@edit');
    Route::put('/admin/{id}','Admin_controller@update');

    Route::delete('/admin/{id}', 'Admin_controller@delete');

    //Peserta
    Route::get('/peserta', 'Peserta_controller@index');

    Route::get('/peserta/{id}','Peserta_controller@edit');
    Route::put('/peserta/{id}','Peserta_controller@update');

    Route::delete('/peserta/{id}', 'Peserta_controller@delete');

    //Pilihan Ganda
    Route::get('/pilihanganda', 'Pilihanganda_controller@index');
    Route::get('/pilihanganda/detail/{id}', 'Pilihanganda_controller@detail');

    Route::get('/pilihanganda/add', 'Pilihanganda_controller@add');
    Route::post('/pilihanganda/add', 'Pilihanganda_controller@store');

    Route::get('/pilihanganda/{id}', 'Pilihanganda_controller@edit');
    Route::put('/pilihanganda/{id}', 'Pilihanganda_controller@update');

    Route::delete('/pilihanganda/{id}', 'Pilihanganda_controller@delete');

    //Uraian
    Route::get('/uraian', 'Uraian_controller@index');

    Route::get('/uraian/add', 'Uraian_controller@add');
    Route::post('/uraian/add', 'Uraian_controller@store');

    Route::get('/uraian/{id}', 'Uraian_controller@edit');
    Route::put('/uraian/{id}', 'Uraian_controller@update');

    //Kementrian
    Route::get('/kementrian', 'Kementrian_controller@index');
    Route::get('/kementrian/detail/{id}', 'Kementrian_controller@detail');

    Route::get('/kementrian/add', 'Kementrian_controller@add');
    Route::post('/kementrian/add', 'Kementrian_controller@store');

    Route::get('/kementrian/{id}', 'Kementrian_controller@edit');
    Route::put('/kementrian/{id}', 'Kementrian_controller@update');

    Route::delete('/kementrian/{id}', 'Kementrian_controller@delete');

    Route::get('/coba', 'Kementrian_controller@view');

    //Soal SPK
    Route::get('/soalspk', 'Soalspk_controller@index');

    Route::get('/soalspk/add', 'Soalspk_controller@add');
    Route::post('/soalspk/add', 'Soalspk_controller@store');

    Route::get('/soalspk/{id}', 'Soalspk_controller@edit');
    Route::put('/soalspk/{id}', 'Soalspk_controller@update');

    Route::delete('/soalspk/{id}', 'Soalspk_controller@delete');

    //Validasi Formulir
    Route::get('/penerimaan', 'Formulir_controller@list');
    Route::get('/penerimaan/{id}', 'Formulir_controller@penerimaan');
    Route::post('/penerimaan/{id}', 'Formulir_controller@approved');

    //Profil
    Route::get('/profil-admin', 'Admin_controller@profil');
    Route::put('/profil-admin', 'Admin_controller@update_profil');

    //Pengaturan Ujian
    Route::put('/pengaturan/ujian', 'Ujian_controller@pengaturan');

    //Pengaturan Formulir
    Route::put('/pengaturan/formulir', 'Formulir_controller@pengaturan');

});

Route::group(['middleware' => ['auth','ceklevel:Asesor']], function(){

    //Penilaian
    Route::get('/penilaian', 'Nilai_controller@list');
    Route::get('/penilaian/{id}', 'Nilai_controller@penilaian');
    Route::put('/penilaian/', 'Nilai_controller@save');

    //Profil
    // Route::get('/profil-asesmen', 'Asesmen_controller@profil');
    // Route::put('/profil-asesmen', 'Asesmen_controller@update_profil');

});

Route::group(['middleware' => ['auth','ceklevel:Peserta']], function(){

    //Ujian
    Route::get('/ujian','Ujian_controller@index');

    //Ujian-PG
    Route::get('/ujian/pg', 'Ujian_controller@pilihanganda');
    Route::post('/ujian/pg', 'Ujian_controller@store');

    //Ujian-Uraian
    Route::get('/ujian/uraian', 'Ujian_controller@uraian');
    Route::post('/ujian/uraian', 'Ujian_controller@save');

    //Nilai
    Route::get('/nilai','Nilai_controller@index');

    //SPK
    Route::get('/spk', 'Spk_controller@index');
    Route::post('/spk', 'Spk_controller@store');

    Route::put('/spk/ulang', 'Spk_controller@pilihulang');

    Route::get('/spk/finish', 'Spk_controller@finish');
    Route::post('/spk/finish', 'Spk_controller@hitung');

    Route::get('/spk/hasil', 'Spk_controller@hasil');

    Route::delete('/spk/ulang/{id}', 'Spk_controller@delete');

    //Formulir
    Route::get('/formulir', 'Formulir_controller@index');
    Route::post('/formulir','Formulir_controller@store');

    Route::get('/formulir/edit', 'Formulir_controller@edit');
    Route::put('/formulir/edit', 'Formulir_controller@update');

    //Profil
    Route::get('/profil-peserta', 'Peserta_controller@profil');
    Route::put('/profil-peserta', 'Peserta_controller@update_profil');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('daftar', 'Oprec_controller@index');

